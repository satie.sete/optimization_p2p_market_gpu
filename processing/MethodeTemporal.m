%% comparaison des différents choix de methode

nMethode = 7;
color = {"b", "r", "m", "k", "g","y","c"};

Methode = ["GPU5","GPU6","GPU8","GPU9","GPU10","GPU11","GPU12" ];
Mat = SimutemporalCISTEMFINAL;


Param = Mat(1,1:16);
nAgent = Param(1);
rhoAgent = Param(2);
epsG = Param(3);
epsL = Param(4);
iterG = Param(5);
iterL = Param(6);
stepG = Param(7);
stepL = Param(8);
dateDebut = Param(9:12);
dateFin = Param(13:16);

DayMonth = [ 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 ];
dayPerMonth = [ 31, 28, 31, 30, 31, 30 , 31, 31, 30, 31, 30, 31 ];

year1 = dateDebut(1);
month1 = dateDebut(2);
day1 = dateDebut(3);
hour1 = dateDebut(4);

year2 = dateFin(1);
month2 = dateFin(2);
day2 = dateFin(3);
hour2 = dateFin(4);

yearref = 2012;
dy = (year1 - yearref);
N1 = (dy*365+ DayMonth(month1) + 1 + day1 - 1) * 24 + hour1;
N1 = N1 + dy / 4 - dy / 100 + dy / 400;
if ((mod(dy,4) == 0 && mod(dy,100) ~= 0) || (mod(dy,400) == 0)) 
    if (month1 < 3) 
        N1 = N1 - 24; % bissextile 
    end
end
	
dy = year2 - yearref;
N2 = (dy * 365 + DayMonth(month2) +1 + day2 - 1) * 24 + hour2;
N2 = N2 + dy / 4 - dy / 100 + dy / 400;

if ((mod(dy,4) == 0 && mod(dy,100) ~= 0) || (mod(dy,400) == 0)) 
    if (month2 < 3) 
        N2 = N2 - 24; % bissextile 
    end
end

nSimu = N2 - N1+1;
temps_tab = zeros(nMethode,nSimu);
iter_tab = zeros(nMethode,nSimu);
fc_tab = zeros(nMethode,nSimu);
conv_tab = zeros(nMethode,nSimu);
ResR_tab = zeros(nMethode,nSimu);
ResS_tab = zeros(nMethode,nSimu);
ResX_tab = zeros(nMethode,nSimu);


for i = (1:nMethode)
    temps_tab(i,:)  = Mat((i-1)*6 + 2, 1:nSimu);
    iter_tab(i,:)   = Mat((i-1)*6 + 3, 1:nSimu);
    fc_tab(i,:)     = Mat((i-1)*6 + 4, 1:nSimu);
    ResR_tab(i,:)   = Mat((i-1)*6 + 5, 1:nSimu);
    ResS_tab(i,:)   = Mat((i-1)*6 + 6, 1:nSimu);
    conv_tab(i,:)   = Mat((i-1)*6 + 7, 1:nSimu);
    
end
ref =1;
speedUp         = (temps_tab(ref,:)- temps_tab)./temps_tab(ref,:);
speedUp_a       = mean(speedUp,2);
speedUp_M       = max(speedUp,[],2);
speedUp_min     = min(speedUp,[],2);
speedUp_median  = median(speedUp,2);
speedUp_std     = std(speedUp,0,2);

tempsMax = max(temps_tab,[],2);
tempsMin = min(temps_tab,[],2);
tempsStd = std(temps_tab,0,2);
tempsMoy = mean(temps_tab,2);



iterMax = max(iter_tab,[],2);
iterMin = min(iter_tab,[],2);
iterStd = std(iter_tab,0,2);
iterMoy = mean(iter_tab,2);

conv = min(conv_tab,[],2);
ResR = max(ResR_tab,[],2);
ResS = max(ResS_tab,[],2);

%%
f = 1;
xSize_r = 18;
ySize_r = 9;
xSize = f*xSize_r;
ySize = f*ySize_r;
Fontsize_v = f*14;
X = linspace(0,nSimu-1,nSimu);


nFigure =1;
figure('Units','centimeters','Position',[1 2 xSize ySize])
name = 'CISTEMFINALtempsAgentMethodeTemporal';
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
for i=(1:nMethode)
    semilogy(X,temps_tab(i,:)', "*"+ color{i});
    hold on
end

legend(Methode,'Location','best');
grid on
title("simulation time (s) according to the simulated hour and the method, 06/01/2013")
axeX = "hour";
axeY = "time (s)";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])


%%


figure()
semilogy(tempsMax, "." + "b",'HandleVisibility','off');
hold on
semilogy(tempsMoy, "*b");
semilogy(tempsMin, "+" + "b",'HandleVisibility','off');
title("Maximum, minimum, average time(s) according to the method, june 2013")
%% etude par jour
taille = 24;
debut=1;
fin = debut+taille-1;

nJour = floor(nSimu/taille);
tempsJour = cell(1,nJour);
 for jour = (1:nJour)
    tempsJour{jour} = temps_tab(:,debut:fin);
    figure('Units','centimeters','Position',[1 2 xSize ySize])
    set(gcf, 'Color', 'w');
    set(gcf, 'PaperUnits','centimeters')
    set(gcf, 'PaperSize',[xSize ySize])
    set(gcf, 'PaperPosition',[0 0 xSize ySize])
    for i=(1:nMethode)
        semilogy((0:taille-1),temps_tab(i,debut:fin)', "*"+ color{i});
        hold on
    end

    legend(Methode,'Location','best');
    grid on
    title("simulation time (s) according to the simulated hour and the method," + "06/" + num2str(jour) + "/2013")
    axeX = "hour";
    axeY = "time (s)";
    xlabel(axeX,'FontSize',Fontsize_v)
    ylabel(axeY,'FontSize',Fontsize_v)
    hold off
    
    debut = fin+1;
    fin = debut+taille-1;  
end


tempsMoyJour = zeros(nMethode,nJour);
tempsMaxJour = zeros(nMethode,nJour);
tempsMinJour = zeros(nMethode,nJour);
tempsStdJour = zeros(nMethode,nJour);

for i=(1:nMethode)
    for j=(1:nJour)
        tempsMoyJour(i,j) = mean(tempsJour{j}(i,:));
        tempsMaxJour(i,j) = max(tempsJour{j}(i,:));
        tempsMinJour(i,j) = min(tempsJour{j}(i,:));
        tempsStdJour(i,j) = std(tempsJour{j}(i,:));
    end
end

figure('Units','centimeters','Position',[1 2 xSize ySize])
name = 'CISTEMFINALtempsAgentMethodeTemporalJourBis';
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
for i=(1:nMethode)
   %semilogy((1:nJour),tempsMaxJour(i,:), "-." + color{i},'HandleVisibility','off');
   hold on
   semilogy((1:nJour),tempsMoyJour(i,:), "-*" + color{i},'DisplayName', Methode{i});
   %semilogy((1:nJour),tempsMinJour(i,:), "-." + color{i},'HandleVisibility','off'); 
end
legend('Location','best');
grid on
title("simulation time (s) according to the method and the day")
axeX = "day";
axeY = "time (s)";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])


%% Etude par heure
nHeure = 24;
nJour = floor(nSimu/nHeure);
tempsHeure = cell(1,nHeure);
tempsMoyHeure = zeros(nMethode,nHeure);
tempsMaxHeure = zeros(nMethode,nHeure);
tempsMinHeure = zeros(nMethode,nHeure);
tempsStdHeure = zeros(nMethode,nHeure);

 for heure=(1:nHeure)
     for jour = (1:nJour)
         indice = (jour-1)*nHeure + heure;
         tempsHeure{heure}(:,jour) = temps_tab(:,indice);
     end
    figure('Units','centimeters','Position',[1 2 xSize ySize])
    set(gcf, 'Color', 'w');
    set(gcf, 'PaperUnits','centimeters')
    set(gcf, 'PaperSize',[xSize ySize])
    set(gcf, 'PaperPosition',[0 0 xSize ySize])
    for i=(1:nMethode)
        semilogy((1:nJour),tempsHeure{heure}(i,:)', "*"+ color{i});
        hold on
    end

    legend(Methode,'Location','best');
    grid on
    title("simulation time (s) according to the simulated day and the method, at " + num2str(heure) + "h")
    axeX = "day";
    axeY = "time (s)";
    xlabel(axeX,'FontSize',Fontsize_v)
    ylabel(axeY,'FontSize',Fontsize_v)
    hold off
    
 end
for i=(1:nMethode)
    for j=(1:nHeure)
        tempsMoyHeure(i,j) = mean(tempsHeure{j}(i,:));
        tempsMaxHeure(i,j) = max(tempsHeure{j}(i,:));
        tempsMinHeure(i,j) = min(tempsHeure{j}(i,:));
        tempsStdHeure(i,j) = std(tempsHeure{j}(i,:));
    end
end

figure('Units','centimeters','Position',[1 2 xSize ySize])
name = 'CISTEMFINALtempsAgentMethodeTemporalHeure';
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
for i=(1:nMethode)
   semilogy((1:nHeure),tempsMaxHeure(i,:), "-." + color{i},'HandleVisibility','off');
   hold on
   semilogy((1:nHeure),tempsMoyHeure(i,:), "-*" + color{i},'DisplayName', Methode{i});
   semilogy((1:nHeure),tempsMinHeure(i,:), "-." + color{i},'HandleVisibility','off'); 
end
legend('Location','best');
grid on
title("Temps de simulation (s) selon la méthode et l'heure, juin 2013")
axeX = "heure";
axeY = "temps(s)";
xlabel(axeX,'FontSize',Fontsize_v)
ylabel(axeY,'FontSize',Fontsize_v)
hold off
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])


