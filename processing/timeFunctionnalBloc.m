

Mat = SimutemporalFBCISTEMFINAL;
nMethod = 7;
Methods = {"GPU5","GPU6","GPU8","GPU9","GPU10","GPU11","GPU12"};
indiceDebut = 1;

nFBlock = 9;
nFBlock_redu = 6;
nOccurence = zeros(nMethod,nFBlock);
time = zeros(nMethod,nFBlock);
time_red = zeros(nMethod,nFBlock_redu);
time_oc = zeros(nMethod,nFBlock);
time_oc_red = zeros(nMethod,nFBlock_redu);
time_sum = zeros(nMethod,1);
time_rel = zeros(nMethod,nFBlock);
time_rel_red = zeros(nMethod,nFBlock_redu);
nIterG = zeros(nMethod,1);
nIterL = zeros(nMethod,1);

FB = {"FB0", "FB1a","FB1b","FB1c","FB2", "FB3a", "FB4","FB5","FB0bis"};
FB_red = {"FB0", "FB1", "FB2", "FB3","FB4","FB5"};
f = 1;

xSize_r = 18;
ySize_r = 9;
xSize = f*xSize_r;
ySize = f*ySize_r;

Fontsize_v = f*14;

%%
Param = Mat(1,1:16);
nAgent = Param(1);
rhoAgent = Param(2);
epsG = Param(3);
epsL = Param(4);
iterG = Param(5);
iterL = Param(6);
stepG = Param(7);
stepL = Param(8);

dateDebut = Param(9:12);
dateFin = Param(13:16);


for i=(1:nMethod)
   nOccurence(i,:) = Mat(2*i,1:nFBlock);
   time(i,:) = Mat(2*i+1,1:nFBlock)/1e9;
   time_sum(i) = sum(time(i,:));
   time_rel(i,:) = time(i,:)/time_sum(i) * 100; % in %
   time_red(i,1) = time(i,1) + time(i,9) ; %FB 0
   time_red(i,2) = time(i,2)+ time(i,3) + time(i,4); %FB1
   time_red(i,3) = time(i,5); %FB2
   time_red(i,4) =  time(i,6); %FB3
   time_red(i,5) = time(i,7) ; %FB4
   time_red(i,6) =  time(i,8); % FB5
   time_rel_red(i,:) = time_red(i,:)/time_sum(i) * 100; % en %
   
   time_oc(i,:) = time(i,:)./nOccurence(i,:);
   time_oc_red(i,1) = time_red(i,1)/(nOccurence(i,1) + nOccurence(i,9));
   if nOccurence(i,2) ~= 0
       time_oc_red(i,2) = time_red(i,2)/nOccurence(i,2);
   else
       time_oc_red(i,2) = 0;
   end
   if nOccurence(i,8) ~= 0
       time_oc_red(i,2) = time_red(i,2)/nOccurence(i,2);
   else
       time_oc_red(i,2) = 0;
   end
   time_oc_red(i,3) = time_red(i,3)/nOccurence(i,5);
   time_oc_red(i,4) = time_red(i,4)/nOccurence(i,6);
   time_oc_red(i,5) = time_red(i,5)/nOccurence(i,7);
   time_oc_red(i,6) = time_red(i,6)/nOccurence(i,8);
   
   nIterG(i) = nOccurence(i,6);
   nIterL(i) = nOccurence(i,2);
   
end



%%
figure(1)
bar(1:length(Methods),time);
set(gca, 'XTick', 1:length(Methods),'XTickLabel',Methods);
set(gca,'YScale','log')
legend(FB)

figure(2)
bar(1:length(Methods),time_rel);
set(gca, 'XTick', 1:length(Methods),'XTickLabel',Methods);
set(gca,'YScale','log')
legend(FB)


%%
figure('Units','centimeters','Position',[1 2 xSize ySize])
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])

name = 'CISTEMPARTIALtempsBarMethodeTemporalFB';
bar(indiceDebut:length(Methods),time(indiceDebut:length(Methods),:), 'stacked');
set(gca,'YScale','log')
set(gca, 'XTick', indiceDebut:length(Methods),'XTickLabel',Methods(indiceDebut:length(Methods)));
legend(FB)
title("Time(s) for each FB according to the method")
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])


figure('Units','centimeters','Position',[1 2 xSize ySize])
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
name = 'CISTEMPARTIALtempsRelBarMethodeTemporalFB';
bar(indiceDebut:length(Methods),time_rel(indiceDebut:length(Methods),:), 'stacked');
title("Relative time(%) for each FB according to the method")
set(gca, 'XTick', indiceDebut:length(Methods),'XTickLabel',Methods(indiceDebut:length(Methods)));
legend(FB)
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])

%%
hold 
figure('Units','centimeters','Position',[1 2 xSize ySize])
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
name = 'CISTEMPARTIALtempsBarMethodeTemporalFBRed';
bar(indiceDebut:length(Methods),time_red(indiceDebut:length(Methods),:), 'stacked');
set(gca, 'XTick', indiceDebut:length(Methods),'XTickLabel',Methods(indiceDebut:length(Methods)));
set(gca,'YScale','log')
legend(FB_red)
title("Time(s) for each FB according to the method")
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])


figure('Units','centimeters','Position',[1 2 xSize ySize])
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])
name = 'CISTEMPARTIALtempsRelBarMethodeTemporalFBRed';
bar(indiceDebut:length(Methods),time_rel_red(indiceDebut:length(Methods),:), 'stacked');
set(gca, 'XTick', indiceDebut:length(Methods),'XTickLabel',Methods(indiceDebut:length(Methods)));
legend(FB_red)
title("Relative time(%) for each FB according to the method")
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])

%%
figure('Units','centimeters','Position',[1 2 xSize ySize])
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])

name = 'CISTEMPARTIALtempsBarMethodeTemporalFBRedOc';
bar(indiceDebut:length(Methods),time_oc_red(indiceDebut:length(Methods),:), 'stacked');
set(gca, 'XTick', indiceDebut:length(Methods),'XTickLabel',Methods(indiceDebut:length(Methods)));
set(gca,'YScale','log')
legend(FB_red)
title("Time(s) by occurence for each FB according to the method")
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])

hold off
figure('Units','centimeters','Position',[1 2 xSize ySize])
set(gcf, 'Color', 'w');
set(gcf, 'PaperUnits','centimeters')
set(gcf, 'PaperSize',[xSize ySize])
set(gcf, 'PaperPosition',[0 0 xSize ySize])

name = 'CISTEMPARTIALtempsBarMethodeTemporalFBOc';
bar(indiceDebut:length(Methods),time_oc(indiceDebut:length(Methods),:), 'stacked');
set(gca, 'XTick', indiceDebut:length(Methods),'XTickLabel',Methods(indiceDebut:length(Methods)));
set(gca,'YScale','log')
legend(FB)
title("Time(s) by occurence for each FB according to the method")
print2eps(name)
eps2pdf([name,'.eps'], [name,'.pdf'])
