#pragma once
#include "ADMMGPU10.cuh"
#include <math.h>
#include <chrono>

int testADMMGPU10();
void testADMMGPU10Time(int test=0);

bool testADMMGPU10Contruct1();
bool testADMMGPU10Contruct2();
bool testADMMGPU10Contruct3();

bool testADMMGPU10Solve1();
bool testADMMGPU10Solve2();
bool testADMMGPU10LAMBDA();
bool testADMMGPU10Bt1();

bool testADMMGPU10UpdateRes();
bool testADMMGPU10CalcRes();

bool testADMMGPU10TradeP();

void testADMMGPU10TimeLAMBDA();
void testADMMGPU10TimeBt1();
void testADMMGPU10TimeTradeP();
void testADMMGPU10TimeUpdateRes();
void testADMMGPU10TimeCalcRes();

