#pragma once

#include <cuda_runtime.h>
#include "MatrixGPU.cuh"


#define NSTEPLOCAL 5
#define NMAXPEERPERTRHREAD 5


__global__ void updateLAMBDAGPU(float* mat1, float* mat2, float rho, int const n);
__global__ void updateBt1GPU(float* Bt1, float* trade, float rho, float* LAMBDA, int const n);
__global__ void updateTrade(float* Bt2, float* Tlocal, float* Tlocal_pre, float* Tmoy, float* P, float* MU, float at1, float at2, float* Bt1, float* Ct, float* matLb, float* matUb, int const n);
__global__ void updatePGPU(float* Tlocal, float* nVoisin, float* MU, float* Tmoy, float* P, float* Ap1, float* Ap2, float* Bp1, float* Cp, float* matLb, float* matUb, int const n);

// sans stockage de la valeur interm�diaire
__global__ void updateTrade2(float* Tlocal, float* Tlocal_pre, float* Tmoy, float* P, float* MU, float at1, float at2, float* Bt1, float* Ct, float* matLb, float* matUb, int const n);
__global__ void updatePGPU2(float* Tlocal, float* nVoisin, float* MU, float* Tmoy, float* P, float* Ap1, float* Ap2, float* Cp, float* matLb, float* matUb, int const n);


// lin�arisation des calculs :
__global__ void updateLAMBDAGPULin(float* LAMBDA, float* trade, float rho, float* CoresLinAgent, float* CoresLinVoisin, float* CoresMatLin, int const n, int const N);
__global__ void updateBt1GPULin(float* Bt1, float* trade, float rho, float* LAMBDA, float* CoresLinAgent, float* CoresLinVoisin, float* CoresMatLin, int const n, int const N);
__global__ void updateTradeLin(float* Tlocal, float* Tlocal_pre, float* Tmoy, float* P, float* MU, float at1, float at2, float* Bt1, float* Ct, float* matLb, float* matUb, float* CoresLinAgent, int const n);
__global__ void updatePGPULin(float* Tlocal, float* nVoisin, float* MU, float* Tmoy, float* P, float* Ap1, float* Ap12, float* Cp, float* matLb, float* matUb, float* CoresAgentLin, int const n);
__global__ void updateDiffGPU(float* tempNN, float* Tlocal, float* CoresLinAgent, float* CoresLinVoisin, float* CoresMatLin, int const n, int const N);
__global__ void updatePnGPU(float* Pn, float* Tmoy, float* nVoisin, const int nAgent);

// ajout de la corespondance indice-> transpos�, et meilleure reduction
__global__ void updateLAMBDAGPULin2(float* LAMBDA, float* trade, float rho, float* CoresLinTrans, int const N);
__global__ void updateBt1GPULin2(float* Bt1, float* trade, float rho, float* LAMBDA, float* CoresLinTrans, int const N);
__global__ void updatePGPULin2(float* MU, float* Tmoy, float* P, float* Ap1, float* Ap2, float* Cp, float* matLb, float* matUb, int const n);
__global__ void updateDiffGPU2(float* tempNN, float* Tlocal, float* CoresLinTrans, int const N);
__global__ void symetriseGPU(float* out, float* tradeLin, float* CoresLinTrans, int const N);


// regroupement des calculs
__global__ void updateLAMBDABt1GPU(float* Bt1, float* LAMBDALin, float* tradeLin, float rho, float* CoresLinTrans, int const N);

template <unsigned int blockSize>
__global__ void updateTradePGPU(float* Tlocal, float* Tlocal_pre, float* Tmoy, float* P, float* MU, float* nVoisin, float at1, float at2, float* Bt1, float* Ct,
	float* matLb, float* matUb, float* Ap1, float* Ap12, float* Cp, float* Pmin, float* Pmax, float* CoresAgentLin, int const n) {

	__shared__ float shArr[blockSize];
	unsigned int thIdx = threadIdx.x;
	const int step = blockSize;
	float sum = 0;
	int i = blockIdx.x;
	int nVoisinLocal = nVoisin[i];
	int beginLocal = CoresAgentLin[i];
	int endLocal = beginLocal + nVoisinLocal; // = CoresAgentLin[blockIdx.x + 1]
	float at1Local = at1;
	float at2Local = at2;
	float at12Local = at1Local + at2Local;

	for (int j = thIdx + beginLocal; j < endLocal; j += step) {
		float m = Tlocal_pre[j] - Tmoy[i] + P[i] - MU[i];
		float r = (Bt1[j] * at1Local + m * at2Local - Ct[j]) / (at12Local);
		float ub = matUb[j];
		float lb = matLb[j];
		float t = (ub - r) * (r > ub) + (lb - r) * (r < lb) + r;
		Tlocal[j] = t;
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduce<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		float moy = shArr[0] / nVoisinLocal;
		Tmoy[blockIdx.x] = moy;
		float muLocal = MU[blockIdx.x];
		float b = moy + muLocal;
		float p = (Ap1[blockIdx.x] * b - Cp[blockIdx.x]) / Ap12[blockIdx.x];
		float Pub = Pmax[blockIdx.x];
		float Plb = Pmin[blockIdx.x];
		p = (Pub - p) * (p > Pub) + (Plb - p) * (p < Plb) + p;
		P[blockIdx.x] = p;
		MU[blockIdx.x] = muLocal + moy - p;
	}

}

template <unsigned int blockSize>
__global__ void updateResGPUMultiBloc(float* res1, float* res2, float* tradelin, float* Tlocal, float* CoresLinTrans, int const N) {
	int thIdx = threadIdx.x;
	int gthIdx = thIdx + blockIdx.x * blockSize;
	const int gridSize = blockSize * gridDim.x;
	float max = 0;
	float max2 = 0;
	__shared__ float shArr[blockSize];
	__shared__ float shArr2[blockSize];

	for (int i = gthIdx; i < N; i += gridSize) {
		float t = Tlocal[i];
		float s = (tradelin[i] - t);
		int k = CoresLinTrans[i];
		float s2 = t + Tlocal[k];
		s2 = s2 * s2;
		s = s * s;
		max = s > max ? s : max;
		max2 = s2 > max2 ? s2 : max2;
	}

	shArr[thIdx] = max;
	shArr2[thIdx] = max2;
	__syncthreads();
	for (int size = blockSize / 2; size > 0; size /= 2) {
		if (thIdx < size) {
			shArr[thIdx] = shArr[thIdx + size] > shArr[thIdx] ? shArr[thIdx + size] : shArr[thIdx]; //shArr[thIdx + size] > shArr[thIdx] ? shArr[thIdx + size] : shArr[thIdx];//shArr[thIdx + size] * (shArr[thIdx + size] > shArr[thIdx]) + shArr[thIdx] * (shArr[thIdx] <= shArr[thIdx + size]);
			shArr2[thIdx] = shArr2[thIdx + size] > shArr2[thIdx] ? shArr2[thIdx + size] : shArr2[thIdx]; //shArr[thIdx + size] > shArr[thIdx] ? shArr[thIdx + size] : shArr[thIdx];//shArr[thIdx + size] * (shArr[thIdx + size] > shArr[thIdx]) + shArr[thIdx] * (shArr[thIdx] <= shArr[thIdx + size]); 
		}
		__syncthreads();
	}
	if (thIdx == 0) {
		res1[blockIdx.x] = shArr[0];
		res2[blockIdx.x] = shArr2[0];
	}
}

template <unsigned int blockSize>
__global__ void updateResLGPUMultiBloc(float* res1, float* res2, float* Tlocal_pre, float* Tlocal, float* P, float* Tmoy, int const N, int const n) {
	int thIdx = threadIdx.x;
	int gthIdx = thIdx + blockIdx.x * blockSize;
	const int gridSize = blockSize * gridDim.x;
	float max = 0;
	float max2 = 0;
	__shared__ float shArr[blockSize];
	__shared__ float shArr2[blockSize];

	for (int i = gthIdx; i < N; i += gridSize) {
		float s = (Tlocal_pre[i] - Tlocal[i]);
		s = s * s;
		max = s > max ? s : max;
		if (i < n) {
			float s2 = (P[i] - Tmoy[i]);
			s2 = s2 * s2;
			max2 = s2 > max2 ? s2 : max2;
		}
	}

	shArr[thIdx] = max;
	shArr2[thIdx] = max2;
	__syncthreads();
	for (int size = blockSize / 2; size > 0; size /= 2) {
		if (thIdx < size) {
			shArr[thIdx] = shArr[thIdx + size] > shArr[thIdx] ? shArr[thIdx + size] : shArr[thIdx]; //shArr[thIdx + size] > shArr[thIdx] ? shArr[thIdx + size] : shArr[thIdx];//shArr[thIdx + size] * (shArr[thIdx + size] > shArr[thIdx]) + shArr[thIdx] * (shArr[thIdx] <= shArr[thIdx + size]);
			shArr2[thIdx] = shArr2[thIdx + size] > shArr2[thIdx] ? shArr2[thIdx + size] : shArr2[thIdx]; //shArr[thIdx + size] > shArr[thIdx] ? shArr[thIdx + size] : shArr[thIdx];//shArr[thIdx + size] * (shArr[thIdx + size] > shArr[thIdx]) + shArr[thIdx] * (shArr[thIdx] <= shArr[thIdx + size]); 
		}
		__syncthreads();
	}
	if (thIdx == 0) {
		res1[blockIdx.x] = shArr[0];
		res2[blockIdx.x] = shArr2[0];
	}
}

// problem global en une fois mais moche (cherche max residus, alors que suffit de comparer au resultat, ecriture/lecture m�moire global � chaque step)

template <unsigned int blockSize>
__device__ void warpReduceMax(volatile float* r, unsigned int idx) {
	if (blockSize >= 64) r[idx] = r[idx + 32] > r[idx] ? r[idx + 32] : r[idx];
	if (blockSize >= 32) r[idx] = r[idx + 16] > r[idx] ? r[idx + 16] : r[idx];
	if (blockSize >= 16) r[idx] = r[idx + 8] > r[idx] ? r[idx + 8] : r[idx];
	if (blockSize >= 8)  r[idx] = r[idx + 4] > r[idx] ? r[idx + 4] : r[idx];
	if (blockSize >= 4)  r[idx] = r[idx + 2] > r[idx] ? r[idx + 2] : r[idx];
	if (blockSize >= 2)  r[idx] = r[idx + 1] > r[idx] ? r[idx + 1] : r[idx];
}


template <unsigned int blockSize>
__global__ void updateTradePGPURes(float* Tlocal, float* Tlocal_pre, float* Tmoy, float* P, float* MU, float* nVoisin, float at1, float at2, float* Bt1, float* Ct,
	float* matLb, float* matUb, float* Ap1, float* Ap12, float* Cp, float* Pmin, float* Pmax, float* CoresAgentLin, float* CoresLinAgent, const int iterL, const float epsL, const int stepL, const int  n) {

	__shared__ float shArrP[blockSize];
	__shared__ float shArr[blockSize];
	unsigned int thIdx = threadIdx.x;
	const int step = blockSize;
	const int i = blockIdx.x;
	float epsL2 = epsL * epsL; // on regarde le residu au carr� !
	__shared__ float resL;
	if (thIdx == 0) {
		resL = 2 * epsL2;
	}
	__syncthreads();
	const int nVoisinLocal = nVoisin[i];
	const int endlocal = CoresAgentLin[i] + nVoisinLocal;
	const int beginlocal = thIdx + CoresAgentLin[i];

	for(int iterLocal = 0; iterLocal<1; iterLocal++)
	{
		// calcul de T
		float sum = 0;
		float max = 0;
		for (int j = beginlocal; j < endlocal; j += step) {
			float t = Tlocal[j];
			Tlocal_pre[j] = t;
			float m = t - Tmoy[i] + P[i] - MU[i];
			float r = (Bt1[j] * at1 + m * at2 - Ct[j]) / (at1 + at2);
			float ub = matUb[j];
			float lb = matLb[j];
			t = (ub - r) * (r > ub) + (lb - r) * (r < lb) + r;
			Tlocal[j] = t;
			float s1 = (Tlocal[j] - Tlocal_pre[j]);
			s1 = s1 * s1;
			max = s1 > max ? s1 : max;//s > max ? s : max; //s * (s > max) + max * (max <= s);
			sum += t;
		}
		// calcul de Tmoy
		shArrP[thIdx] = sum;
		shArr[thIdx] = max;
		__syncthreads();

		if (blockSize >= 512) {
			if (thIdx < 256) { 
				shArr[thIdx] = shArr[thIdx + 256] > shArr[thIdx] ? shArr[thIdx + 256] : shArr[thIdx];
				shArrP[thIdx] += shArrP[thIdx + 256];

			} __syncthreads();
		}
		if (blockSize >= 256) {
			if (thIdx < 128) {
				shArrP[thIdx] += shArrP[thIdx + 128];
				shArr[thIdx] = shArr[thIdx + 128] > shArr[thIdx] ? shArr[thIdx + 128] : shArr[thIdx];
			}
			__syncthreads(); }
		if (blockSize >= 128) {
			if (thIdx < 64) {
				shArrP[thIdx] += shArrP[thIdx + 64];
				shArr[thIdx] = shArr[thIdx + 64] > shArr[thIdx] ? shArr[thIdx + 64] : shArr[thIdx];
			} 
			__syncthreads(); }
		if (thIdx < 32) {
			warpReduce<blockSize>(shArrP, thIdx);
			warpReduceMax<blockSize>(shArr, thIdx);
		}
		//calcul de P
		if (thIdx == 0) {
			float moy = shArrP[0] / nVoisinLocal;
			Tmoy[i] = moy;
			float b = moy + MU[i];
			float p = (Ap1[i] * b - Cp[i]) / Ap12[i];
			float Pub = Pmax[i];
			float Plb = Pmin[i];
			p = (Pub - p) * (p > Pub) + (Plb - p) * (p < Plb) + p;
			P[i] = p;
			MU[i] = MU[i] + moy - p;
			float s2 = (moy - p);
			s2 = s2 * s2;
			resL = s2 > shArr[0] ? s2 : shArr[0];
		}
		__syncthreads(); // pour �tre s�r que le tread 0 ait fini son calcul de P;
		if (resL < epsL2) {
			break;
		}
		iterLocal= iterLocal + 1;
	}
}


// plusieurs it�aration en une seule
template <unsigned int blockSize>
__global__ void updateTradePGPULocal(float* Tlocal, float* Tlocal_pre, float* Tmoy, float* P, float* MU, float* nVoisin, float at1, float at2, float* Bt1, float* Ct,
	float* matLb, float* matUb, float* Ap1, float* Ap12, float* Cp, float* Pmin, float* Pmax, float* CoresAgentLin) {

	//Definition de toutes les variables locales
	int i = blockIdx.x; // c'est aussi l'identifiant de l'agent !
	unsigned int thIdx = threadIdx.x;
	const int step = blockSize;
	// ne change pas
	const float Ap1Local = Ap1[i];
	const float CpLocal = Cp[i];
	const float Ap12Local = Ap12[i];
	const float Pub = Pmax[i];
	const float Plb = Pmin[i];
	const int nVoisinLocal = nVoisin[i];
	const int CoresAgentLinLocal = CoresAgentLin[i];
	const int beginLocal = CoresAgentLinLocal + thIdx;
	const int endLocal = CoresAgentLinLocal + nVoisinLocal;

	const float at1local = at1;
	const float at2local = at2;
	const float at12local = at1local + at2local;


	float Bt1local[NMAXPEERPERTRHREAD];
	float Ctlocal[NMAXPEERPERTRHREAD];
	float matUblocal[NMAXPEERPERTRHREAD];
	float matLblocal[NMAXPEERPERTRHREAD];

	float Tlocallocal[NMAXPEERPERTRHREAD]; // change
	float Tlocalprelocal[NMAXPEERPERTRHREAD]; // change
	float MULOCAL;
	float moy;
	float p;
	float sum;
	float bp;
	float m, r, ub, lb, t;
	// le changement doit �tre partag� par tous les threads du bloc

	__shared__ float MuShared;
	__shared__ float TMoyShared;
	__shared__ float PShared;
	if (thIdx == 0) {
		MuShared = MU[i];
		TMoyShared = Tmoy[i];
		PShared = P[i];
	}
	int k = 0;
	for (int j = beginLocal; j < endLocal; j += step) {
		Bt1local[k] = Bt1[j];
		Ctlocal[k] = Ct[j];
		matUblocal[k] = matUb[j];
		matLblocal[k] = matLb[j];
		//Tlocalprelocal[k] = Tlocal_pre[j];
		Tlocallocal[k] = Tlocal_pre[j];
		k = k + 1;
	}

	__shared__ float shArr[blockSize];


	//Calcul des it�rations
	__syncthreads();
	for (int iter = 0; iter < NSTEPLOCAL; iter++) {

		MULOCAL = MuShared; // tous lisent le m�me : broadcast !
		moy = TMoyShared;
		p = PShared;
		sum = 0;
		k = 0;
		for (int j = beginLocal; j < endLocal; j += step) {
			Tlocalprelocal[k] = Tlocallocal[k];
			m = Tlocallocal[k] - moy + p - MULOCAL;
			r = (Bt1local[k] * at1local + m * at2local - Ctlocal[k]) / (at12local);
			ub = matUblocal[k];
			lb = matLblocal[k];
			t = (ub - r) * (r > ub) + (lb - r) * (r < lb) + r;
			Tlocallocal[k] = t;
			sum += t;
			k = k + 1;
		}

		shArr[thIdx] = sum;
		__syncthreads();
		if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
		if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
		if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
		if (thIdx < 32) {
			warpReduce<blockSize>(shArr, thIdx);
		}
		__syncthreads();

		if (thIdx == 0) {
			moy = shArr[0] / nVoisinLocal;
			TMoyShared = moy;
			bp = moy + MuShared;
			p = (Ap1Local * bp - CpLocal) / Ap12Local;
			p = (Pub - p) * (p > Pub) + (Plb - p) * (p < Plb) + p;
			PShared = p;
			MuShared = MULOCAL + moy - p;
		}
		__syncthreads();
	}
	//Ecriture des it�rations
	__syncthreads();
	k = 0;
	for (int j = beginLocal; j < endLocal; j += step) {
		Tlocal[j] = Tlocallocal[k];
		Tlocal_pre[j] = Tlocalprelocal[k];
		k = k + 1;
	}
	if (thIdx == 0) {
		Tmoy[blockIdx.x] = TMoyShared;// TMoyShared;
		P[blockIdx.x] = PShared;// PShared;
		MU[blockIdx.x] = MuShared;// MuShared;
	}

}


// utilisation m�moire partag�e:
template <unsigned int blockSize>
__global__ void updateTradePGPUShared(float* Tlocal, float* Tlocal_pre, float* Tmoy, float* P, float* MU, float* nVoisin, float at1, float at2, float* Bt1, float* Ct,
	float* matLb, float* matUb, float* Ap1, float* Ap12, float* Cp, float* Pmin, float* Pmax, float* CoresAgentLin) {

	//Definition de toutes les variables locales
	int i = blockIdx.x; // c'est aussi l'identifiant de l'agent !
	unsigned int thIdx = threadIdx.x;
	const int step = blockSize;
	// ne change pas


	float Bt1local[NMAXPEERPERTRHREAD];
	float Ctlocal[NMAXPEERPERTRHREAD];
	float matUblocal[NMAXPEERPERTRHREAD];
	float matLblocal[NMAXPEERPERTRHREAD];

	float Tlocallocal[NMAXPEERPERTRHREAD]; // change
	float Tlocalprelocal[NMAXPEERPERTRHREAD]; // change
	float sum;
	float bp, MULOCAL, moy, p;
	float m, r, ub, lb, t;
	// le changement doit �tre partag� par tous les threads du bloc

	__shared__ float MuShared;
	__shared__ float TMoyShared;
	__shared__ float PShared;


	// constant et commun � tous les thread d'un bloc
	__shared__ float Ap1Shared;
	__shared__ float CpShared;
	__shared__ float Ap12Shared;
	__shared__ float PmaxShared;
	__shared__ float PminShared;
	__shared__ float nVoisinShared;
	__shared__ float at1Shared;
	__shared__ float at2Shared;
	__shared__ float at12Shared;


	if (thIdx == 0) {
		Ap1Shared = Ap1[i];
		CpShared = Cp[i];
		Ap12Shared = Ap12[i];
		PmaxShared = Pmax[i];
		PminShared = Pmin[i];
		nVoisinShared = nVoisin[i];
		at1Shared = at1;
		at2Shared = at2;
		at12Shared = at1 + at2;
		MuShared = MU[i];
		TMoyShared = Tmoy[i];
		PShared = P[i];
	}
	int k = 0;
	__syncthreads();
	const int CoresAgentLinLocal = CoresAgentLin[i];
	const int beginLocal = CoresAgentLinLocal + thIdx;
	const int endLocal = CoresAgentLinLocal + nVoisinShared;
	for (int j = beginLocal; j < endLocal; j += step) {
		Bt1local[k] = Bt1[j];
		Ctlocal[k] = Ct[j];
		matUblocal[k] = matUb[j];
		matLblocal[k] = matLb[j];
		//Tlocalprelocal[k] = Tlocal_pre[j];
		Tlocallocal[k] = Tlocal_pre[j];
		k = k + 1;
	}

	__shared__ float shArr[blockSize];

	//Calcul des it�rations

	for (int iter = 0; iter < NSTEPLOCAL; iter++) {

		MULOCAL = MuShared; // tous lisent le m�me : broadcast !
		moy = TMoyShared;
		p = PShared;
		sum = 0;
		k = 0;
		for (int j = beginLocal; j < endLocal; j += step) {
			Tlocalprelocal[k] = Tlocallocal[k];
			m = Tlocallocal[k] - moy + p - MULOCAL;
			r = (Bt1local[k] * at1Shared + m * at2Shared - Ctlocal[k]) / (at12Shared);
			ub = matUblocal[k];
			lb = matLblocal[k];
			t = (ub - r) * (r > ub) + (lb - r) * (r < lb) + r;
			Tlocallocal[k] = t;
			sum += t;
			k = k + 1;
		}

		shArr[thIdx] = sum;
		__syncthreads();
		if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
		if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
		if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
		if (thIdx < 32) {
			warpReduce<blockSize>(shArr, thIdx);
		}
		__syncthreads();

		if (thIdx == 0) {
			moy = shArr[0] / nVoisinShared;
			TMoyShared = moy;
			bp = moy + MuShared;
			p = (Ap1Shared * bp - CpShared) / Ap12Shared;
			p = (PmaxShared - p) * (p > PmaxShared) + (PminShared - p) * (p < PminShared) + p;
			PShared = p;
			MuShared = MULOCAL + moy - p;
		}
		__syncthreads();
	}
	//Ecriture des it�rations
	__syncthreads();
	k = 0;
	for (int j = beginLocal; j < endLocal; j += step) {
		Tlocal[j] = Tlocallocal[k];
		Tlocal_pre[j] = Tlocalprelocal[k];
		k = k + 1;
	}
	if (thIdx == 0) {
		Tmoy[blockIdx.x] = TMoyShared;// TMoyShared;
		P[blockIdx.x] = PShared;// PShared;
		MU[blockIdx.x] = MuShared;// MuShared;
	}

}


// problem local en une fois mais propre
template <unsigned int blockSize>
__global__ void updateTradePGPUSharedResidual(float* Tlocal, float* Tlocal_pre, float* Tmoy, float* P, float* MU, float* nVoisin, float at1, float at2, float* Bt1, float* Ct,
	float* matLb, float* matUb, float* Ap1, float* Ap12, float* Cp, float* Pmin, float* Pmax, float* CoresAgentLin, float eps, int nStepL) {

	//Definition de toutes les variables locales
	int i = blockIdx.x; // c'est aussi l'identifiant de l'agent !
	unsigned int thIdx = threadIdx.x;
	const int step = blockSize;
	// ne change pas


	float Bt1local[NMAXPEERPERTRHREAD];
	float Ctlocal[NMAXPEERPERTRHREAD];
	float matUblocal[NMAXPEERPERTRHREAD];
	float matLblocal[NMAXPEERPERTRHREAD];

	float Tlocallocal[NMAXPEERPERTRHREAD]; // change
	float Tlocalprelocal[NMAXPEERPERTRHREAD]; // change
	float sum;
	float bp, MULOCAL, moy, p;
	float m, r, ub, lb, t;
	// le changement doit �tre partag� par tous les threads du bloc

	__shared__ float MuShared;
	__shared__ float TMoyShared;
	__shared__ float PShared;


	// constant et commun � tous les thread d'un bloc
	__shared__ float Ap1Shared;
	__shared__ float CpShared;
	__shared__ float Ap12Shared;
	__shared__ float PmaxShared;
	__shared__ float PminShared;
	__shared__ float nVoisinShared;
	__shared__ float at1Shared;
	__shared__ float at2Shared;
	__shared__ float at12Shared;
	__shared__ bool mustContinue;


	if (thIdx == 0) {
		Ap1Shared = Ap1[i];
		CpShared = Cp[i];
		Ap12Shared = Ap12[i];
		PmaxShared = Pmax[i];
		PminShared = Pmin[i];
		nVoisinShared = nVoisin[i];
		at1Shared = at1;
		at2Shared = at2;
		at12Shared = at1 + at2;
		MuShared = MU[i];
		TMoyShared = Tmoy[i];
		PShared = P[i];
		mustContinue = false;
	}
	int k = 0;
	__syncthreads();
	const int CoresAgentLinLocal = CoresAgentLin[i];
	const int beginLocal = CoresAgentLinLocal + thIdx;
	const int endLocal = CoresAgentLinLocal + nVoisinShared;
	float res;
	for (int j = beginLocal; j < endLocal; j += step) {
		Bt1local[k] = Bt1[j];
		Ctlocal[k] = Ct[j];
		matUblocal[k] = matUb[j];
		matLblocal[k] = matLb[j];
		//Tlocalprelocal[k] = Tlocal_pre[j];
		Tlocallocal[k] = Tlocal_pre[j];
		k = k + 1;
	}

	__shared__ float shArr[blockSize];

	//Calcul des it�rations

	for (int iter = 0; iter < nStepL; iter++) {

		MULOCAL = MuShared; // tous lisent le m�me : broadcast !
		moy = TMoyShared;
		p = PShared;
		sum = 0;
		k = 0;
		for (int j = beginLocal; j < endLocal; j += step) {
			Tlocalprelocal[k] = Tlocallocal[k];
			m = Tlocallocal[k] - moy + p - MULOCAL;
			r = (Bt1local[k] * at1Shared + m * at2Shared - Ctlocal[k]) / (at12Shared);
			ub = matUblocal[k];
			lb = matLblocal[k];
			t = (ub - r) * (r > ub) + (lb - r) * (r < lb) + r;
			Tlocallocal[k] = t;
			sum += t;
			res = (t - Tlocalprelocal[k]);
			res = (double)res * res;
			if (res > eps) {
				mustContinue = true; // pas de race condition, car l'ordre n'importe pas,
				//mais est ce que cela ne va pas physiquement bloquer ?
			}
			k = k + 1;
		}

		shArr[thIdx] = sum;
		__syncthreads();
		if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
		if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
		if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
		if (thIdx < 32) {
			warpReduce<blockSize>(shArr, thIdx);
		}
		__syncthreads();

		if (thIdx == 0) {
			moy = shArr[0] / nVoisinShared;
			TMoyShared = moy;
			bp = moy + MuShared;
			p = (Ap1Shared * bp - CpShared) / Ap12Shared;
			p = (PmaxShared - p) * (p > PmaxShared) + (PminShared - p) * (p < PminShared) + p;
			PShared = p;
			res = p - moy;
			res = (double)res * res;
			if (res > eps) {
				mustContinue = true;
			}
			MuShared = MULOCAL + moy - p;
		}
		__syncthreads();
		if (!mustContinue) {
			break;
		}
		else {
			__syncthreads();
			if (thIdx == 0) {
				mustContinue = false;
			}
		}
	}
	//Ecriture des it�rations
	__syncthreads();
	k = 0;
	for (int j = beginLocal; j < endLocal; j += step) {
		Tlocal[j] = Tlocallocal[k];
		Tlocal_pre[j] = Tlocalprelocal[k];
		k = k + 1;
	}
	if (thIdx == 0) {
		Tmoy[blockIdx.x] = TMoyShared;// TMoyShared;
		P[blockIdx.x] = PShared;// PShared;
		MU[blockIdx.x] = MuShared;// MuShared;
	}

}



