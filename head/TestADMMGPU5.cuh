#pragma once
#include "ADMMGPU5.cuh"
#include <math.h>
#include <chrono>

int testADMMGPU5();
void testADMMGPU5Time(int test=0);

bool testADMMGPU5Contruct1();
bool testADMMGPU5Contruct2();
bool testADMMGPU5Contruct3();

bool testADMMGPU5Solve1();
bool testADMMGPU5Solve2();
bool testADMMGPU5LAMBDA();
bool testADMMGPU5Bt1();

bool testADMMGPU5UpdateRes();
bool testADMMGPU5CalcRes();

bool testADMMGPU5Trade();
bool testADMMGPU5P();
bool testADMMGPU5Tmoy();

void testADMMGPU5TimeLAMBDA();
void testADMMGPU5TimeBt1();
void testADMMGPU5TimeTrade();
void testADMMGPU5TimeTmoy();
void testADMMGPU5TimeP();
void testADMMGPU5TimeUpdateRes();
void testADMMGPU5TimeCalcRes();

