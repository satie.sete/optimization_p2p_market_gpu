#pragma once
#include "ADMMGPU9.cuh"
#include <math.h>
#include <chrono>

int testADMMGPU9();
void testADMMGPU9Time(int test=0);

bool testADMMGPU9Contruct1();
bool testADMMGPU9Contruct2();
bool testADMMGPU9Contruct3();

bool testADMMGPU9Solve1();
bool testADMMGPU9Solve2();
bool testADMMGPU9LAMBDA();
bool testADMMGPU9Bt1();

bool testADMMGPU9UpdateRes();
bool testADMMGPU9CalcRes();

bool testADMMGPU9TradeP();

void testADMMGPU9TimeLAMBDA();
void testADMMGPU9TimeBt1();
void testADMMGPU9TimeTradeP();
void testADMMGPU9TimeUpdateRes();
void testADMMGPU9TimeCalcRes();

