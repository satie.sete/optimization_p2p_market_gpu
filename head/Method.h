#pragma once

#include <iostream>
#include <string>

#include <chrono>

#include "StudyCase.h"
#include "MatrixCPU.h"
#include "MatrixGPU.cuh"
#include "Simparam.h"




class Method
{
public:
	Method();
	virtual ~Method();
	virtual void setParam(float param) = 0;
	virtual void setTau(float tau) = 0;

	void updateLAMBDA(MatrixCPU* LAMBDA, MatrixCPU* trade, float rho);
	void updateLAMBDA(MatrixGPU* LAMBDA, MatrixGPU* trade, float rho, MatrixGPU* tempNN);
	virtual float updateRes(MatrixCPU* res, MatrixGPU* Tlocal, MatrixGPU* trade, int iter, MatrixGPU* tempNN);
	float calcFc(MatrixGPU* cost1, MatrixGPU* cost2, MatrixGPU* trade, MatrixGPU* Pn, MatrixGPU* GAMMA, MatrixGPU* tempN1, MatrixGPU* tempNN);
	float updateRes(MatrixCPU* res, MatrixCPU* Tlocal, MatrixCPU* trade, int iter);
	float calcFc(MatrixCPU* cost1, MatrixCPU* cost2, MatrixCPU* trade, MatrixCPU* Pn, MatrixCPU* GAMMA, MatrixCPU* tempN1, MatrixCPU* tempNN);
	void updatePn(MatrixCPU* Pn, MatrixCPU* trade);
	void updatePn(MatrixGPU* Pn, MatrixGPU* trade);
	virtual void solve(Simparam* result, const Simparam& sim, const StudyCase& cas)=0;
	virtual void updateP0(const StudyCase& cas) = 0;
	virtual void init(const Simparam& sim, const StudyCase& cas) = 0;
	virtual void display() = 0;
	std::string _name;

	void resetId();

protected:
	int _id = 0;
	MatrixCPU timePerBlock = MatrixCPU(1, 9, 0); // Fb0, Fb1abc, Fb2, Fb3abc, Fb4, Fb5, Fb0'
	// si les sous ensemble ne sont pas accessible, tout est dans le premier.
	MatrixCPU occurencePerBlock = MatrixCPU(1, 9, 0); //nb de fois utilis� pendant la simu
	
};









