#pragma once


#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include "Agent.h"
#include "MatrixCPU.h"
#include <math.h>
#include <iostream>

#define DELETEB(x) if (x!=nullptr) {delete x; x = nullptr;}
#define DELETEA(x) if (x!=nullptr) {delete[] x; x = nullptr;}

class StudyCase
{
    int _nAgent;
    int _nPro;
    int _nGen;
    int _nCons;
    Agent* _agents = nullptr;
    MatrixCPU _GAMMA;
    MatrixCPU _connect;
    MatrixCPU _a;
    MatrixCPU _b;
    MatrixCPU _Ub;
    MatrixCPU _Lb;
    MatrixCPU _Pmin;
    MatrixCPU _Pmax;
    MatrixCPU _nVoisin;

    void setMatFromFile(const std::string& path, const std::string& date, MatrixCPU* Pgen, MatrixCPU* P0, MatrixCPU* costGen);
    void setGenFromFile(const std::string& path, MatrixCPU* Pgen, MatrixCPU* costGen);
    float rand1() const; 
    
    

public:
    StudyCase();
    
    StudyCase(int nAgent, float P, float dP, float a, float da, float b, float db, float propCons = 0.5, float propPro = 0.125);
    StudyCase(int nAgent, float P0, float dP, float b, float db, float propCons = 0.5);
    StudyCase(const StudyCase& s);
    StudyCase(std::string fileName);
    StudyCase& operator= (const StudyCase& s);
    void UpdateP0(MatrixCPU* P0);
    void genConnec(MatrixCPU* connec, int nCons, int nGen, int nPo);
    void genGAMMA(MatrixCPU* GAMMA, int nCons, int nGen, int nPo);
    void Set29node();
    void Set4node();
    void Set2node();
    void SetEuropeCountry(const std::string& path, const std::string& date);
    void SetEuropeCountryP0(const std::string& path, MatrixCPU* P0);
    void SetEurope(const std::string& path, const std::string& date);
    void SetEuropeP0(const std::string& path, MatrixCPU* P0);
    // getter 
    MatrixCPU getG() const;
    MatrixCPU getC() const;
    MatrixCPU geta() const;
    MatrixCPU getb() const;
    MatrixCPU getUb() const;
    MatrixCPU getLb() const;
    MatrixCPU getPmin() const;
    MatrixCPU getPmax() const;
    MatrixCPU getNvoi() const;
    int getNagent() const;
    MatrixCPU getVoisin(int agent) const;
    Agent getAgent(int agent) const;

    void removeLink(int i, int j);
    Agent removeAgent(int agent);
    void restoreAgent(Agent& agent, bool all = false);
    void addLink(int i, int j);

    void saveCSV(const std::string& fileName, bool all = true);


    void display() const;
    ~StudyCase();

};
