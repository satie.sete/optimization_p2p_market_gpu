#pragma once
#include "ADMMGPU8.cuh"
#include <math.h>
#include <chrono>

int testADMMGPU8();
void testADMMGPU8Time(int test=0);

bool testADMMGPU8Contruct1();
bool testADMMGPU8Contruct2();
bool testADMMGPU8Contruct3();

bool testADMMGPU8Solve1();
bool testADMMGPU8Solve2();
bool testADMMGPU8LAMBDA();
bool testADMMGPU8Bt1();

bool testADMMGPU8UpdateRes();
bool testADMMGPU8CalcRes();

bool testADMMGPU8TradeP();

void testADMMGPU8TimeLAMBDA();
void testADMMGPU8TimeBt1();
void testADMMGPU8TimeTradeP();
void testADMMGPU8TimeUpdateRes();
void testADMMGPU8TimeCalcRes();

