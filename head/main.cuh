#pragma once

#include "MatrixCPU.h"
#include "MatrixGPU.cuh"
#include "StudyCase.h"
#include "Method.h"
#include "Simparam.h"
#include "System.h"


#include <stdio.h>
#include <iostream>
#include <time.h>
#include <cuda_runtime.h>
#include <cudaProfiler.h>

// fichier de test
#include "TestMatrixCPU.h"
#include "TestAgent.h"
#include "TestStudyCase.h"
#include "TestADMM.h"
#include "TestADMMGPU.cuh"
#include "TestADMMGPU5.cuh"
#include "TestADMMGPU6.cuh"
#include "TestADMMGPU8.cuh"
#include "TestADMMGPU9.cuh"
#include "TestADMMGPU10.cuh"
#include "TestSimparam.h"
#include "TestSysteme.h"
#include "TestMatrixGPU.cuh"
#include "TestMatrixCPU.h"






// test on the global and local step impact
void SimulationTempStepOpti();



// Performance test, alea case or europeen case.
void SimuTemporal();



void testADMMGPUtemp();
std::string generateDate(int year, int month, int day, int hour);





/*
On ne peut inclure qu'un seul osqp � la fois, (car m�me nom)
C:\Program Files \OSQP\osqp\include\osqp


*/