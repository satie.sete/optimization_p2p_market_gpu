#pragma once

#include "StudyCase.h"
#include "Simparam.h"

#include "Method.h"
#include "ADMM.h"
#include "ADMM1.h"
#include "ADMMGPU.cuh"
#include "ADMMGPU2.cuh"
#include "ADMMGPU3.cuh"
#include "ADMMGPU4.cuh"
#include "ADMMGPU5.cuh"
#include "ADMMGPU6.cuh"
#include "ADMMGPU7.cuh"
#include "ADMMGPU8.cuh"
#include "ADMMGPU9.cuh"
#include "ADMMGPU10.cuh"
#include "ADMMGPU11.cuh"
#include "ADMMGPU12.cuh"
#include "OSQP.h"

#include <iostream>
#include <string>

#define DELETEB(x) if (x!=nullptr) {delete x; x = nullptr;}
#define DELETEA(x) if (x!=nullptr) {delete[] x; x = nullptr;}

class System
{
public:
	System();
	System(float rho, int iterMaxGlobal, int iterMaxLocal, float epsGlobal, float epsLocal, std::string nameMethode, int nAgent, float P = 0, float dP = 0, float a = 0, float da = 0, float b = 0, float db = 0);
	~System();
	const std::string sADMM = "ADMM";
	const std::string sADMM1 = "ADMM1";
	const std::string sADMMGPU = "ADMMGPU";
	const std::string sADMMGPU6 = "ADMMGPU6";
	const std::string sADMMGPU2 = "ADMMGPU2";
	const std::string sADMMGPU3 = "ADMMGPU3";
	const std::string sADMMGPU4 = "ADMMGPU4";
	const std::string sADMMGPU5 = "ADMMGPU5";
	const std::string sADMMGPU7 = "ADMMGPU7";
	const std::string sADMMGPU8 = "ADMMGPU8";
	const std::string sADMMGPU9 = "ADMMGPU9";
	const std::string sADMMGPU10 = "ADMMGPU10";
	const std::string sADMMGPU11 = "ADMMGPU11";
	const std::string sADMMGPU12 = "ADMMGPU12";
	const std::string sOSQPERSO = "osqperso";
	const std::string sOSQP = "OSQP";
	Simparam solve();
	void solveIntervalle(std::string path, MatrixCPU* interval, int nCons, int nGen); 
	void UpdateP0();
	void resetMethod(); // permet de forcer l'initialisation, m�me si ce n'est pas la premi�re it�ration
	void resetParam();
	void removeLink(int i, int j);
	void addLink(int i, int j);
	Agent removeAgent(int agent);
	void restoreAgent(Agent& agent, bool all = false);
	void setBestRho(float rhoMax = 0, bool rhoVar = 0, float rhoTest = 0);


	// mettre tous les set permettant de modifier les param�tres...
	void setStudyCase(const StudyCase& cas);
	void setSimparam(const Simparam& param);
	void setMethod(std::string nameMethode);
	void setRho(float rho);
	void setRhoL(float rho);
	void setTau(float tau);
	void setIter(int iterG, int iterL);
	void setStep(int stepG, int stepL);
	void setEpsG(float epsG);
	void setEpsL(float epsL);

	MatrixCPU getRes() const;
	MatrixCPU getTrade() const;
	MatrixCPU getTemps() const;
	MatrixCPU getIter() const;
	MatrixCPU getConv() const;
	MatrixCPU getFc() const;
	MatrixCPU getResR() const;
	MatrixCPU getResS() const;
	int getNTrade() const;

	std::string generateDate(int year, int month, int day, int hour);
	void generateP0(MatrixCPU* P0, std::string path, std::string month);
	std::string generateMonth(int year, int month);
	void display(int type=0);
	void displayTime() const;

private:
	StudyCase _case;
	Simparam _simparam;
	Simparam* _result = nullptr;
	Method* _methode = nullptr;
	
	MatrixCPU _temps;
	MatrixCPU _iter;
	MatrixCPU _conv;
	MatrixCPU _fc;
	MatrixCPU _ResR;
	MatrixCPU _ResS;
	
	
	
};




