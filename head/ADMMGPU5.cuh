#pragma once
#include "Method.h"
#include "KernelFunction.cuh"
#include "MatrixGPU.cuh"
#include "MatrixCPU.h"

#include <iostream>
#include <string>
#include <cuda_runtime.h>


class ADMMGPU5 : public Method
{
public:
	ADMMGPU5();
	ADMMGPU5(float rho);
	virtual ~ADMMGPU5();
	void setParam(float rho);
	void setTau(float tau);

	virtual void solve(Simparam* result, const Simparam& sim, const StudyCase& cas);
	void updateGlobalProbGPU();
	void updateLocalProbGPU(MatrixGPU* Tlocal, MatrixGPU* P);
	void init(const Simparam& sim, const StudyCase& cas);
	void updateP0(const StudyCase& cas);
	void updateTmoy(MatrixGPU* Tlocal);
	std::string NAME ="ADMMGPU5";
	
	virtual float updateRes(MatrixCPU* res, MatrixGPU* Tlocal, int iter, MatrixGPU* tempNN);
	float calcRes(MatrixGPU* Tlocal, MatrixGPU* P);
	void display();

private:
	
	float _mu = 40;
	float _tau = 2;
	float _rho = 0;
	int _blockSize = 512;
	int _numBlocks1 = 0;
	int _numBlocks2 = 0;
	int _n = 0;
	int _N = 0;
	float _rhog = 0;
	float _at1 = 0;
	float _at2 = 0;

	MatrixGPU tempNN; 
	MatrixGPU tempN1;

	MatrixGPU Tlocal;
	MatrixGPU P; 
	MatrixGPU Pn; 


	MatrixGPU a;
	MatrixGPU Ap2;
	MatrixGPU Ap1;
	MatrixGPU Ap12;
	MatrixGPU Bt1;
	MatrixGPU Ct;
	MatrixGPU matUb;
	
	MatrixGPU nVoisin;
	MatrixGPU tradeLin;
	MatrixGPU Tmoy;
	MatrixGPU LAMBDALin;
	MatrixGPU Tlocal_pre;
	MatrixGPU MU;

	MatrixGPU CoresMatLin;
	MatrixGPU CoresLinAgent;
	MatrixGPU CoresAgentLin;
	MatrixGPU CoresLinVoisin;
	MatrixGPU CoresLinTrans;
	
	MatrixGPU b;
	MatrixGPU matLb;
	MatrixGPU Cp;
	MatrixGPU Pmin;
	MatrixGPU Pmax;
	

};




template <unsigned int blockSize>
__device__ void warpReduce5(volatile float* sdata, unsigned int tid);


template <unsigned int blockSize>
__global__ void SumTmoy5(float* Tmoy, float* Tlocal, float* nVoisin, float* CoresAgentLin);

