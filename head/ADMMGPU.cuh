#pragma once
#
#include "MatrixGPU.cuh"
#include "MatrixCPU.h"
#include "Method.h"
#include "KernelFunction.cuh"

#include <iostream>
#include <string>
#include <cuda_runtime.h>
#include <chrono>


class ADMMGPU : public Method
{
public:
	ADMMGPU();
	ADMMGPU(float rho);
	virtual ~ADMMGPU();
	void setParam(float rho);
	void setTau(float tau);

	virtual void solve(Simparam* result, const Simparam& sim, const StudyCase& cas);
	virtual void updateP0(const StudyCase& cas);
	virtual void init(const Simparam& sim, const StudyCase& cas);
	std::string NAME ="ADMMGPU";
	float calcRes(MatrixGPU* Tlocal, MatrixGPU* Tlocal_pre, MatrixGPU* Tmoy, MatrixGPU* P, MatrixGPU* tempN1, MatrixGPU* tempNN);
	void updateLocalProbGPU(MatrixGPU* Bt2, MatrixGPU* Tlocal, MatrixGPU* Tmoy, MatrixGPU* Bp1, MatrixGPU* P, MatrixGPU* MU, float at1, float at2, MatrixGPU* Bt1, MatrixGPU* Ct,
		MatrixGPU* matLb, MatrixGPU* matUb, MatrixGPU* nVoisin, MatrixGPU* Ap1, MatrixGPU* Ap2, MatrixGPU* Ap12, MatrixGPU* Cp, MatrixGPU* Pmin, MatrixGPU* Pmax, MatrixGPU* Tlocal_pre);
	void updateGlobalProbGPU(MatrixGPU* LAMBDA, MatrixGPU* Bt1, MatrixGPU* trade, float rho);
	void display();
private:
	float _rho = 0;
	int _blockSize = 512;
	int _numBlocks=0;
	int _numBlocksM = 0;
	int _n = 0;

};







