#pragma once
#include "MatrixCPU.h"
#include <iostream>
#include <string>


class Simparam
{
public:
	static constexpr float RHO = 1.5f;
	static const int ITERMAXGLOBAL = 800;
	static const int ITERMAXLOCAL = 400;
	static constexpr float EPSGLOBAL = 0.0001f; // 1e-4
	static constexpr float EPSLOCAL = 0.00001f; // 1e-5 (pas plus car float !!!!!)
	static const int STEPL = 5;
	static const int STEPG = 5;
	Simparam();
	Simparam(const Simparam& sim);
	Simparam(int nAgent);
	Simparam(float rho, int iterMaxGlobal, int iterMaxLocal, float epsGlobal, float epsLocal, int _nAgent);

	void reset();

	Simparam& operator= (const Simparam& sim);
	~Simparam();
	
	// getter

	float getRho() const;
	int getIterG() const;
	int getIter() const;
	int getIterLTot() const;
	int getIterL() const;
	float getEpsG() const;
	float getEpsL() const;
	int getNAgent() const;

	int getStepL() const;
	int getStepG() const;

	MatrixCPU getTrade() const;
	MatrixCPU getTradeSym() const;
	MatrixCPU getLambda() const;
	MatrixCPU getRes() const;
	MatrixCPU getPn() const;

	float getTime() const;
	MatrixCPU getMU() const;
	float getFc() const;
	float getFcSym() const;
	
	// setter
	void setItG(int iter);
	void setItL(int iter);

	void setLAMBDA(MatrixCPU* m);
	void setTrade(MatrixCPU* m);
	void setTradeSym(MatrixCPU* m);
	void setResF(MatrixCPU* m);
	void setMU(MatrixCPU* m);
	void setPn(MatrixCPU* m);
	void setTimeBloc(MatrixCPU* time, MatrixCPU* occurrence);
	void setIter(int c);
	void setTime(float f);
	void setFc(float f);
	void setFcSym(float f);
	void setNagent(int n);
	void setItLTot(int iterLocalTotal);

	void setRho(float rho);
	void setStep(int stepG, int stepL);
	void setEpsG(float epsG);
	void setEpsL(float epsL);

	// other
	void saveCSV(const std::string& filename, int all=1);
	void display(int type=0) const;
	void displayTime() const;

private:
	float _rho;
	int _iterMaxGlobal;
	int _iterMaxLocal;
	int _stepG;
	int _stepL;
	float _epsGlobal;
	float _epsLocal;
	int _nAgent;
	MatrixCPU _LAMBDA;
	MatrixCPU _trade;
	MatrixCPU _tradeSym;
	MatrixCPU _Pn;
	MatrixCPU _resF;
	int _iter;
	int _iterLocalTotal;
	float _time;
	MatrixCPU _MU;
	float _fc;
	float _fcSym;
	MatrixCPU timePerBlock = MatrixCPU(1, 9, 0); // Fb0, Fb1abc, Fb2, Fb3abc, Fb4, Fb5, Fb0'
	// si les sous ensemble ne sont pas accessible, tout est dans le premier.
	MatrixCPU occurencePerBlock = MatrixCPU(1, 9, 0); //nb de fois utilis� pendant la simu
};




