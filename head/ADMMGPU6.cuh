#pragma once
#include "Method.h"
#include "KernelFunction.cuh"
#include "MatrixGPU.cuh"
#include "MatrixCPU.h"

#include <iostream>
#include <string>
#include <cuda_runtime.h>


class ADMMGPU6 : public Method
{
public:
	ADMMGPU6();
	ADMMGPU6(float rho);
	virtual ~ADMMGPU6();
	void setParam(float rho);
	void setTau(float tau);

	virtual void solve(Simparam* result, const Simparam& sim, const StudyCase& cas);
	void updateGlobalProbGPU();
	void updateLocalProbGPU(MatrixGPU* Tlocal, MatrixGPU* P);
	void init(const Simparam& sim, const StudyCase& cas);
	void updateP0(const StudyCase& cas);
	std::string NAME ="ADMMGPU6";
	
	//virtual float updateRes(MatrixCPU* res, MatrixGPU* Tlocal, int iter, MatrixGPU* tempNN);
	virtual float updateRes(MatrixCPU* res, MatrixGPU* Tlocal, int iter, MatrixGPU* tempN, MatrixGPU* tempN2);
	float calcRes(MatrixGPU* Tlocal, MatrixGPU* P, MatrixGPU* tempN, MatrixGPU* tempN2);
	//float calcRes(MatrixGPU* Tlocal, MatrixGPU* P);
	void display();

private:
	// ne change pas avec P0
	float _mu = 40;
	float _tau = 2;
	float _rho = 0;
	int _blockSize = 128;
	int _numBlocks1 = 0;
	int _numBlocks2 = 0;
	int _n = 0;
	int _N = 0;
	float _rhog = 0;
	float _at1 = 0;
	float _at2 = 0;

	MatrixGPU tempNN; 
	MatrixGPU tempN1; 
	MatrixGPU tempN; 
	MatrixGPU tempN2;

	MatrixGPU Tlocal;
	MatrixGPU P; 
	MatrixGPU Pn;

	MatrixGPU a;
	MatrixGPU Ap2;
	MatrixGPU Ap1;
	MatrixGPU Ap12;
	MatrixGPU Bt1;
	MatrixGPU Ct;
	MatrixGPU matUb;
	
	MatrixGPU nVoisin;
	MatrixGPU tradeLin;
	MatrixGPU Tmoy;
	MatrixGPU LAMBDALin;
	MatrixGPU Tlocal_pre;
	MatrixGPU MU;

	MatrixGPU CoresMatLin;
	MatrixGPU CoresLinAgent;
	MatrixGPU CoresAgentLin;
	MatrixGPU CoresLinVoisin;
	MatrixGPU CoresLinTrans;

	
	MatrixGPU b;
	MatrixGPU matLb;
	MatrixGPU Cp;
	MatrixGPU Pmin;
	MatrixGPU Pmax;
	

};


__device__ float warpReduceMax6(volatile float* r, int const N);

template <unsigned int blockSize>
__global__ void maxMonoBlock6(float* res1, float* resOut1, float* res2, float* resOut2, int const N);