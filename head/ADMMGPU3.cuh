#pragma once
#include "Method.h"
#include "KernelFunction.cuh"
#include "MatrixGPU.cuh"
#include "MatrixCPU.h"

#include <iostream>
#include <string>
#include <cuda_runtime.h>


class ADMMGPU3 : public Method
{
public:
	ADMMGPU3();
	ADMMGPU3(float rho);
	virtual ~ADMMGPU3();
	void setParam(float rho);
	void setTau(float tau);

	virtual void solve(Simparam* result, const Simparam& sim, const StudyCase& cas);
	virtual void updateP0(const StudyCase& cas);
	virtual void init(const Simparam& sim, const StudyCase& cas);
	std::string NAME ="ADMMGPU3";
	float calcRes(MatrixGPU* Tlocal, MatrixGPU* P, MatrixGPU* tempN1, MatrixGPU* tempNN);
	virtual float updateRes(MatrixCPU* res, MatrixGPU* Tlocal, int iter, MatrixGPU* tempNN);
	void updateLocalProbGPU(MatrixGPU* Tlocal, MatrixGPU* P);
	void updateGlobalProbGPU();
	void display();
private:
	float _rho = 0;
	int _blockSize = 256;
	int _numBlocks = 0;
	int _numBlocksM = 0;
	int _n = 0;
	float _mu = 40;
	float _tau = 2;
	
	int _N = 0;
	float _rhog = 0;
	float _at1 = 0;
	float _at2 = 0;

	MatrixGPU tempNN; 
	MatrixGPU tempN1; 

	MatrixGPU Tlocal;
	MatrixGPU P;
	MatrixGPU Pn; 


	MatrixGPU a;
	MatrixGPU Ap2;
	MatrixGPU Ap1;
	MatrixGPU Ap12;
	MatrixGPU Bt1;
	MatrixGPU Ct;
	MatrixGPU Pmax;
	MatrixGPU nVoisin;
	MatrixGPU Tmoy;
	MatrixGPU MU;
	MatrixGPU LAMBDA;
	MatrixGPU Tlocal_pre;
	MatrixGPU trade;
	MatrixGPU GAMMA;
	MatrixGPU connect;

	
	MatrixGPU b;
	MatrixGPU matUb;
	MatrixGPU matLb;
	MatrixGPU Cp;
	MatrixGPU Pmin;

};







