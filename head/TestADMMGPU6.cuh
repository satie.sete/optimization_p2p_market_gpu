#pragma once

#include "ADMMGPU6.cuh"
#include <math.h>
#include <chrono>

int testADMMGPU6();
void testADMMGPU6Time(int test=0);

bool testADMMGPU6Contruct1();
bool testADMMGPU6Contruct2();
bool testADMMGPU6Contruct3();

bool testADMMGPU6Solve1();
bool testADMMGPU6Solve2();
bool testADMMGPU6LAMBDABt1();

bool testADMMGPU6UpdateRes();
bool testADMMGPU6CalcRes();
bool testADMMGPU6TradeP();

void testADMMGPU6TimeLAMBDABt1();
void testADMMGPU6TimeTradeP();
void testADMMGPU6TimeUpdateRes();
void testADMMGPU6TimeCalcRes();

//bool testADMMGPU6Trade();
//bool testADMMGPU6P();
//bool testADMMGPU6Tmoy();*/