# Optimization of a peer-to-peer electricity market resolution on GPU
## Introduction
This repository gathers codes illustrating the algorithms presented in the conference CISTEM 2022 : "Optimization of a peer-to-peer electricity market resolution on GPU". The simulation is made with Cuda/C++ (https://developer.nvidia.com/cuda-downloads) and some results' analysis are made with Matlab. This document is here to give some hint for the code understanding. To have more details on the notations, and the equation, readers may refer to the article.

This code use data (charge and generator capacity) extrated from a European Data set : 
DTU-ELMA/European Dataset (Jensen T, Pinson P. RE-Europe, a large-scale dataset for modeling a highly renewable European electricity system. Sci Data 4, 170175 .2017. https://doi.org/10.1038/sdata.2017.175,
that the complete version is available here https://github.com/DTU-ELMA/European_Dataset. 



## How to read data ?

2 files for generator :

    PowerMaxGen.txt : presents the capacity (MW) of each generator 
        size        : 969 * 2 
        each row    : idGen, Capacity
        Rem         : idGen is not used

    costGen.txt     : presents the lineat cost ($/MW) of each generator 
        size        : 969 * 2
        each row    : idGen, linear cost
        Rem         : idGen is not used

36 files for consumers :

    year-month.txt : presents the consumption (MWh) on 1494 nodes
        size       : 1494 * (1 + nHour)
        each row   : idRow, consuption for each hour
        Rem        : nHour depends on the selected month (for exemple 30*24 for june, 31*24 for january), idRow is the number 
                     of the row and is not used   

2 files for the results :

    SimuTemporal.csv     : presents the results of SimuTemporal()
        size             : 1*16 + (6*nMethod)*nSimu
        first row        : nAgent, epsL, epsG, rho_agent, IterG, IterL, stepG, stepL, yearB, monthB, dayB, hourB,
                            yearE, monthE, dayE, hourE
        others 6 rows    :  time (s) for each simulated hour
                            iter for each simulated hour
                            fc ($) for each simulated hour
                            resS for each simulated hour
                            resR for each simulated hour
                            convergence for each simulated hour
        Rem              : the 6 rows must be repeated for each simulated method, fc is the value of the sum of the function cost, convergence is 
                           a boolean that indicates if the simulation has converged (i.e if the residuals are lowers that the required accuracy)
                            
    SimuTemporalFB.csv          : presents the results of printtime()
        size                    : 1* 16 + 2* nMethod * 9
        first row               : nAgent, epsL, epsG, rho_agent, IterG, IterL, stepG, stepL, yearB, monthB, dayB, hourB,
                            yearE, monthE, dayE, hourE
        others 2 rows    :  occurence of each functionnal bloc
                            time (ns) of each functionnal bloc
        Rem              : the 2 rows must be repeated for each simulated method, 
 
        






