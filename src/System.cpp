#include "../head/System.h"

// To DO MatrixCPU genererP0(path,dateMonth)
// To DO updateCas(P0)

System::System() {
#ifdef DEBUG_CONSTRUCTOR
	std::cout << " system constructor" << std::endl;
#endif // DEBUG_CONSTRUCTOR

	int _nAgent = 29;
	_case.Set29node();
	_simparam = Simparam(_nAgent);
	_methode = new ADMM();
	_result = new Simparam(_simparam);
}


System::System(float rho, int iterMaxGlobal, int iterMaxLocal, float epsGlobal, float epsLocal, std::string nameMethode, int nAgent, float P, float dP, float a, float da, float b, float db)
{
	std::cout << "system constructor 2" << std::endl;
	
	int _nAgent = nAgent;
	if (nAgent == 0) {
		_case = StudyCase();
		_nAgent = 29;
	
	}
	else {
		_case = StudyCase(nAgent,  P,  dP,  a,  da,  b,  db);
	}
	setMethod(nameMethode);
	

	_simparam = Simparam(rho, iterMaxGlobal, iterMaxLocal, epsGlobal, epsLocal, _nAgent);
	_result = new Simparam(_simparam);
	
}
System::~System()
{
	DELETEB(_methode);
	DELETEB(_result);
	
}

Simparam System::solve()
{
	_methode->solve(_result, _simparam, _case);
	return *_result;
}

void System::solveIntervalle(std::string path, MatrixCPU* interval, int nCons, int nGen)
{

	if (interval->getNCol() != 2 || interval->getNLin() != 4) {
		throw std::invalid_argument("interval must be 4*2, year, month, day, hour");
	}
	int year1, year2, month1, month2, day1, day2, hour1, hour2;
	year1 = interval->get(0, 0);
	year2 = interval->get(0, 1);
	month1 = interval->get(1, 0);
	month2 = interval->get(1, 1);
	day1 = interval->get(2, 0);
	day2 = interval->get(2, 1);
	hour1 = interval->get(3, 0);
	hour2 = interval->get(3, 1);
	// verifier que c'est possible
	if (year1 > year2) {
		throw std::invalid_argument("date1 must be before date2 (year)");
	}
	else if (year1 == year2) {
		if (month1 > month2) {
			throw std::invalid_argument("date1 must be before date2 (month)");
		}
		else if (month1 == month2) {
			if (day1 > day2) {
				throw std::invalid_argument("date1 must be before date2 (day)");
			}
			else if (day1 == day2) {
				if (hour1 > hour2) {
					throw std::invalid_argument("date1 must be before date2 (hour)");
				}
			}
		}
	} 
	if ( hour1 > 23 || hour2 > 23) {
		throw std::invalid_argument("hour must be lesser or equal to 23");
	}
	
	int m[12] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 };
	int dayMonth[12] = { 31, 28, 31, 30, 31, 30 , 31, 31, 30, 31, 30, 31 };

	int yearref = 2012;
	int dy = (year1 - yearref);
	int N1 = (dy*365+ m[month1 - 1] + 1 + day1 - 1) * 24 + hour1;
	N1 = N1 + dy / 4 - dy / 100 + dy / 400;
	if ((dy % 4 == 0 && dy % 100 != 0) || (dy % 400 == 0)) {
		if (month1 < 3) {
			N1 = N1 - 24; 
		}
	}
	
	dy = year2 - yearref;
	int N2 = (dy * 365 + m[month2 - 1] +1 + day2 - 1) * 24 + hour2;
	N2 = N2 + dy / 4 - dy / 100 + dy / 400;

	if ((dy % 4 == 0 && dy % 100 != 0) || (dy % 400 == 0)) {
		if (month2 < 3) {
			N2 = N2 - 24; 
		}
	}
	int Nsimu = N2 - N1 + 1;
	

	std::cout << "Simulation count " <<Nsimu << std::endl;
	
	
	bool fin = false;
	bool bissextile = false;
	int year = year1;
	if ((year%4 == 0 && year%100 != 0) || (year % 400 == 0)) {
		bissextile = true;
	}
	int month = month1;
	int day = day1;
	int hour = hour1;
	int dayl = dayMonth[month-1];
	if (month == 2 && bissextile) {
		dayl = 29;
	}

		
	int Nhour = 24 * dayl;
	std::string date = generateMonth(year, month);
	

	MatrixCPU P0Global(nCons, Nhour);
	
	MatrixCPU P0(nCons, 1);
	int indiceP0 = (day-1) * 24 + hour;
	
	StudyCase cas;
	_temps = MatrixCPU(1,Nsimu);
	_iter = MatrixCPU(1, Nsimu);
	_conv= MatrixCPU(1, Nsimu,-1);
	_fc = MatrixCPU(1, Nsimu);
	_ResR = MatrixCPU(1, Nsimu);
	_ResS = MatrixCPU(1, Nsimu);
	int indice = 0;
	int stepG = _simparam.getStepG();
	std::string nameP0;
	if (nCons < 50) {
		nameP0 = path + "/load/Month/Country/Country_" ;
		generateP0(&P0Global, nameP0, date);
		P0Global.getBloc(&P0, 0, nCons, indiceP0, indiceP0 + 1);
		indiceP0 = indiceP0 + 1;
		cas.SetEuropeCountryP0(path, &P0);
	}
	else {
		nameP0 = path + "/load/Month/";
		generateP0(&P0Global, nameP0, date);
		P0Global.getBloc(&P0, 0, nCons, indiceP0, indiceP0 + 1);
		indiceP0 = indiceP0 + 1;
		cas.SetEuropeP0(path, &P0);
	}
	
	//cas.display();
	setStudyCase(cas);
	resetMethod();
	//display(1);
	float epsG = _simparam.getEpsG();
	clock_t t = clock();
	std::cout << "-";
	while (!fin) {
		
		if ((year == year2) && (month == month2) && (day == day2) && (hour == hour2)) {
			fin = true;
		}
		
		/// simulation
		
		
		Simparam res = solve();
		//std::cout << "-";
		t = clock() - t;
		
		_temps.set(0, indice, (float)t / CLOCKS_PER_SEC);

		
	
		_iter.set(0, indice, res.getIter());
		_fc.set(0, indice, res.getFc());
		int iter = res.getIter();
		MatrixCPU Res(res.getRes());
		float resR = Res.get(0, (iter - 1) / stepG);
		float resS = Res.get(1, (iter - 1) / stepG);
		_ResR.set(0, indice, resR);
		_ResS.set(0, indice, resS);
		if (resR < epsG && resS < epsG) {
			_conv.set(0, indice, 1);
		}
		else {
			_conv.set(0, indice, 0);
		}
		if (!fin) {
			t = clock();
			_simparam.setTrade(&(res.getTrade()));
			_simparam.setLAMBDA(&(res.getLambda()));
			_simparam.setMU(&(res.getMU()));
			_simparam.setPn(&res.getPn());
			hour = hour + 1;
			if (hour == 24) {
				//std::cout << "|"; 
				day = day + 1;
				hour = 0;
				if (day > dayl) {
					std::cout << "\n";
					day = 1;
					month = month + 1;
					if (month > 12) {
						year = year + 1;
						month = 1;
						if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
							bissextile = true;
						}
					}
					dayl = dayMonth[month - 1];
					if (month == 2 && bissextile) {
						dayl = 29;
					}
					Nhour = 24 * dayl;
					date = generateMonth(year, month);
					P0Global.setSize(nCons, Nhour);
					generateP0(&P0Global, nameP0, date);
					indiceP0 = 0;
				}
			}
			P0Global.getBloc(&P0, 0, nCons, indiceP0, indiceP0 + 1);
			cas.UpdateP0(&P0);
			setStudyCase(cas);
			UpdateP0(); 
			indiceP0 = indiceP0 + 1;
			indice = indice + 1;
		}
		
	}
	std::cout << std::endl;
	
	std::cout << "times sum : " << _temps.sum() << std::endl;
	std::cout << "simulation count : " << Nsimu << std::endl;
	std::cout << "iter :" << std::endl;
	_iter.display();
	std::cout << "times :" << std::endl;
	_temps.display();
	std::cout << "conv :" << std::endl;
	_conv.display();
	std::cout << "fc : " << std::endl;
	_fc.display();
	
}

void System::UpdateP0()
{
	_methode->updateP0(_case);
}

void System::resetMethod()
{
	setMethod(_methode->_name);
}

void System::resetParam()
{
	_simparam.reset();
}

void System::removeLink(int i, int j)
{
	_case.removeLink(i, j);
}

void System::addLink(int i, int j)
{
	_case.addLink(i, j);
}

Agent System::removeAgent(int agent)
{
	return _case.removeAgent(agent);
}

void System::restoreAgent(Agent& agent, bool all)
{
	_case.restoreAgent(agent, all);
}

void System::setBestRho(float rhoMax, bool rhoVar, float rhoTest)
{
	int nAgent = _case.getNagent();
	float rhoMin = 0.01;
	if (rhoMax == 0) {
		rhoMax = 0.2 * nAgent;
	}
	float epsRho = 0.01;
	float dRhoMin = 0.005;

	if (rhoVar == 0) {
		setTau(1);
	}


	float rho_a = rhoMin;
	float rho_b = rhoMax;
	int multiplieur = 1;
	while ((rho_b - rho_a) > epsRho) {
		float dRho = multiplieur * (rho_a + rho_b) / 100;
		dRho = dRho > dRhoMin ? dRho : dRhoMin;
		float rho_x = 0.5 * (rho_a + rho_b);

		float rho_c = rho_x - (dRho / 2);
		float rho_d = rho_x + (dRho / 2);
		setRho(rho_c);
		*_result = solve();
		int iter_c = _result->getIter();
		int iterL_c = _result->getIterLTot();

		setRho(rho_d);
		*_result = solve();
		int iter_d = _result->getIter();
		int iterL_d = _result->getIterLTot();

		if (iter_d < iter_c) {
			rho_a = rho_d;
			multiplieur = 1;
		}
		else if (iter_d > iter_c) {
			rho_b = rho_c;
			multiplieur = 1;
		}
		else {
			std::cout << iter_d << " " << iter_c << " " << iterL_d << " " << iterL_c << std::endl;
			multiplieur++;
		}

	}

	float rho_x = 0.5 * (rho_a + rho_b);
	setRho(rho_x);
	*_result = solve();
	int iter_x = _result->getIter();
	int iterL_x = _result->getIterLTot();
	float time_x = _result->getTime();

	float rho_the = nAgent * 0.05;
	setRho(rho_the);
	*_result = solve();
	int iter_the = _result->getIter();
	int iterL_the = _result->getIterLTot();
	float time_the = _result->getTime();
	int iter_test = iter_the + iter_x; 
	int iterL_test = 0;
	float time_test = 0;
	if (rhoTest != 0 && rhoTest != rho_the && rhoTest != rho_x) {
		setRho(rhoTest);
		*_result = solve();
		iter_test = _result->getIter();
		iterL_test = _result->getIterLTot();
		time_test = _result->getTime();
	}

	float rhoBest = 0;
	if (iter_x < iter_the) {
		if (iter_x < iter_test) {
			rhoBest = rho_x;
		}
		else if (iter_x > iter_test) {
			rhoBest = rhoTest;
		}
		else {
			if (iterL_x <= iterL_test) {
				rhoBest = rho_x;
			}
			else {
				rhoBest = rhoTest;
			}
		}
	} else if (iter_x > iter_the) {
		if (iter_the < iter_test) {
			rhoBest = rho_the;
		}
		else if (iter_the > iter_test) {
			rhoBest = rhoTest;
		}
		else {
			if (iterL_the <= iterL_test) {
				rhoBest = rho_the;
			}
			else {
				rhoBest = rhoTest;
			}
		}
	}
	else {
		if (iter_x > iter_test) {
			rhoBest = rhoTest;	
		}
		else {
			if (iterL_the <= iterL_x) {
				rhoBest = rho_the;
			}
			else {
				rhoBest = rho_x;
			}
		}
	}
	setRho(rhoBest);
	std::cout << "Best rho find is " << rhoBest << std::endl;
	if (rhoVar == 0) {
		setTau(2);
	}
}

void System::setStudyCase(const StudyCase& cas)
{
	_case = cas;
	
	if (cas.getNagent() != _simparam.getNAgent())
	{
		std::cout << "wrong number of agent, simparam and result update " << std::endl;
		_simparam.setNagent(cas.getNagent());
		_result->setNagent(cas.getNagent());
	}
	
}
void System::setSimparam(const Simparam& param)
{
	_simparam = param;
	DELETEB(_result);
	_result = new Simparam(_simparam);
	if (_case.getNagent() != param.getNAgent())
	{
		std::cout << "wrong number of agent, simparam and result update" << std::endl;
		std::cout << "if it is not wanted change the study case before doing that" << std::endl;
		_simparam.setNagent(_case.getNagent());
		_result->setNagent(_case.getNagent());
	}

}
void System::setMethod(std::string nameMethode) {
	#if DEBUG_DESTRUCTOR
		std::cout << "destructor of " << _methode->_name << std::endl;
	#endif
		DELETEB(_methode);
	#if DEBUG_DESTRUCTOR
		std::cout << "end of destructor " << std::endl;
	#endif
	if (!nameMethode.compare(sADMM)) {
		_methode = new ADMM();
	}
	else if (!nameMethode.compare(sADMM1)) {
		_methode = new ADMM1();
	}
	else if ((!nameMethode.compare(sADMMGPU))) {
		_methode = new ADMMGPU();
	}
	else if ((!nameMethode.compare(sADMMGPU6))) {
		_methode = new ADMMGPU6();
	}
	else if ((!nameMethode.compare(sADMMGPU2))) {
		_methode = new ADMMGPU2();
	}
	else if ((!nameMethode.compare(sADMMGPU3))) {
		_methode = new ADMMGPU3();
	}
	else if ((!nameMethode.compare(sADMMGPU4))) {
		_methode = new ADMMGPU4();
	}
	else if ((!nameMethode.compare(sADMMGPU5))) {
		_methode = new ADMMGPU5();
	}
	else if ((!nameMethode.compare(sADMMGPU7))) {
		_methode = new ADMMGPU7();
	}
	else if ((!nameMethode.compare(sADMMGPU8))) {
		_methode = new ADMMGPU8();
	}
	else if ((!nameMethode.compare(sADMMGPU9))) {
		_methode = new ADMMGPU9();
	}
	else if ((!nameMethode.compare(sADMMGPU10))) {
		_methode = new ADMMGPU10();
	}
	else if ((!nameMethode.compare(sADMMGPU11))) {
		_methode = new ADMMGPU11();
	}
	else if ((!nameMethode.compare(sADMMGPU12))) {
		_methode = new ADMMGPU12();
	}
	else if ((!nameMethode.compare(sOSQP))) {
		_methode = new OSQP();
	}
	else {
		std::cout << "unknonwn method " << nameMethode  << std::endl;
		exit(-1);
	}
}

void System::setRho(float rho) {
	_simparam.setRho(rho);
	_result->setRho(rho);
}
void System::setRhoL(float rho)
{
	_methode->setParam(rho);
}
void System::setTau(float tau)
{
	_methode->setTau(tau);
}
void System::setIter(int iterG, int iterL) {
	_simparam.setItG(iterG);
	_simparam.setItL(iterL);
	_result->setItG(iterG);
	_result->setItL(iterL);
}

void System::setStep(int stepG, int stepL)
{
	_simparam.setStep(stepG, stepL);
	_result->setStep(stepG, stepL);
}

void System::setEpsG(float epsG)
{
	_simparam.setEpsG(epsG);
}

void System::setEpsL(float epsL)
{
	_simparam.setEpsL(epsL);
}


MatrixCPU System::getRes() const {
	return _result->getRes();
}
MatrixCPU System::getTrade() const {
	return _result->getTrade();
}

MatrixCPU System::getTemps() const
{
	return _temps;
}

MatrixCPU System::getIter() const
{
	return _iter;
}

MatrixCPU System::getConv() const
{
	return _conv;
}

MatrixCPU System::getFc() const
{
	return _fc;
}

MatrixCPU System::getResR() const
{
	return _ResR;
}

MatrixCPU System::getResS() const
{
	return _ResS;
}

int System::getNTrade() const
{
	return (_case.getNvoi()).sum();
}


void System::display(int type) // type=0 result, type =1 simparam & methode, type=2 case
{
	if (type==1) {
		std::cout << "Simparam : " << std::endl;
		_simparam.display(1);
		
	}
	else if (type == 2)
	{
		std::cout << "Case : " << std::endl;
		_case.display();
	}
	else {
		std::cout << "Method : ";
		_methode->display();
		_result->display();
	}
}

void System::displayTime() const
{
	_result->displayTime();
}

std::string System::generateDate(int year, int month, int day, int hour)
{
	std::string smonth;
	std::string sday;
	std::string shour;
	if (month < 10) {
		smonth = "0" + std::to_string(month);
	}
	else {
		smonth = std::to_string(month);
	}
	if (day < 10) {
		sday = "0" + std::to_string(day);
	}
	else {
		sday = std::to_string(day);
	}
	if (hour < 10) {
		shour = "0" + std::to_string(hour);
	}
	else {
		shour = std::to_string(hour);
	}



	std::string d = std::to_string(year) + "-" + smonth + "-" + sday + " " + shour + "-00-00";

	return d;
}
std::string System::generateMonth(int year, int month)
{
	std::string smonth;
	std::string sday;
	std::string shour;
	if (month < 10) {
		smonth = "0" + std::to_string(month);
	}
	else {
		smonth = std::to_string(month);
	}
	
	std::string d = std::to_string(year) + "-" + smonth;

	return d;
}

void System::generateP0(MatrixCPU* P0, std::string path, std::string month) {
	
	P0->setFromFile(path + month + ".txt", 1);
}
