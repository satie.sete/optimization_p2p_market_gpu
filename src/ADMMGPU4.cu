#include "../head/ADMMGPU4.cuh"

ADMMGPU4::ADMMGPU4() : Method()
{
#if DEBUG_CONSTRUCTOR
	std::cout << "ADMMGPU4 constructor" << std::endl;
#endif // DEBUG_CONSTRUCTOR
	_name = NAME;
}


ADMMGPU4::ADMMGPU4(float rho) : Method()
{
#if DEBUG_CONSTRUCTOR
	std::cout << "default ADMMGPU4 constructor" << std::endl;
#endif // DEBUG_CONSTRUCTOR
	_name = NAME;
	_rho = rho;
}

ADMMGPU4::~ADMMGPU4()
{
}

void ADMMGPU4::setParam(float rho)
{
	_rho = rho;
}

void ADMMGPU4::setTau(float tau)
{
	if (tau < 1) {
		throw std::invalid_argument("tau must be greater than 1");
	}
	_tau = tau;
}

void ADMMGPU4::init(const Simparam& sim, const StudyCase& cas)
{
	clock_t t = clock();
	_rhog = sim.getRho();
	int nAgent = sim.getNAgent();
	_n = nAgent;
	float rho_p = _rho * _n;
	if (_rho == 0) {
		rho_p = _rhog;
	}
	MatrixCPU GAMMA(cas.getG());
	MatrixGPU connect(cas.getC());
	MatrixGPU Ub(cas.getUb());
	MatrixGPU Lb(cas.getLb());
	MatrixCPU LAMBDA(sim.getLambda());
	MatrixCPU trade(sim.getTrade());

	a = cas.geta();
	b = cas.getb();

	Pmin = MatrixGPU(cas.getPmin());
	Pmax = MatrixGPU(cas.getPmax());
	nVoisin = MatrixGPU(cas.getNvoi());

	
	int nTradeTot = nVoisin.sum();
	_N = nTradeTot;
	_numBlocks1 = ceil((_n + _blockSize - 1) / _blockSize);
	_numBlocks2 = ceil((_N + _blockSize - 1) / _blockSize);
	CoresMatLin = MatrixGPU(nAgent, nAgent, -1);
	CoresLinAgent = MatrixGPU(nTradeTot, 1);
	CoresAgentLin = MatrixGPU(nAgent + 1, 1);
	CoresLinVoisin = MatrixGPU(nTradeTot, 1);


	MU = sim.getMU(); // lambda_l/_rho

	Tlocal_pre = MatrixGPU(nTradeTot, 1);
	tradeLin = MatrixGPU(nTradeTot, 1);
	Tmoy = sim.getPn();
	LAMBDALin = MatrixGPU(nTradeTot, 1);

	_at1 = _rhog; // 2*a
	_at2 = rho_p;

	Ap2 = MatrixGPU(a);
	Ap1 = MatrixGPU(nVoisin);
	Ap12 = MatrixGPU(nAgent, 1);
	

	Bt1 = MatrixGPU(nTradeTot, 1);
	Ct = MatrixGPU(nTradeTot, 1);
	Cp = MatrixGPU(b);
	matUb = MatrixGPU(nTradeTot, 1);
	matLb = MatrixGPU(nTradeTot, 1);


	int indice = 0;

	for (int idAgent = 0;idAgent < nAgent; idAgent++) {
		MatrixCPU omega(cas.getVoisin(idAgent));
		int Nvoisinmax = nVoisin.get(idAgent, 0);
		for (int voisin = 0; voisin < Nvoisinmax; voisin++) {
			int idVoisin = omega.get(voisin, 0);
			matLb.set(indice, 0, Lb.get(idAgent, 0));
			matUb.set(indice, 0, Ub.get(idAgent, 0));
			Ct.set(indice, 0, GAMMA.get(idAgent, idVoisin));
			tradeLin.set(indice, 0, trade.get(idAgent, idVoisin));
			Tlocal_pre.set(indice, 0, trade.get(idAgent, idVoisin));
			LAMBDALin.set(indice, 0, LAMBDA.get(idAgent, idVoisin));
			CoresLinAgent.set(indice, 0, idAgent);
			CoresLinVoisin.set(indice, 0, idVoisin);
			CoresMatLin.set(idAgent, idVoisin, indice);
			indice = indice + 1;
		}
		CoresAgentLin.set(idAgent + 1, 0, indice);
	}

	tempNN = MatrixGPU(_N, 1, 0, 1);
	tempN1 = MatrixGPU(_n, 1, 0, 1); 

	tempNN.preallocateReduction();
	tempN1.preallocateReduction();

	Tlocal = MatrixGPU(_N, 1, 0, 1);
	P = MatrixGPU(_n, 1, 0, 1); 
	Pn = MatrixGPU(_n, 1, 0, 1);



	matUb.transferGPU();
	matLb.transferGPU();
	Pmin.transferGPU();
	Pmax.transferGPU();
	nVoisin.transferGPU();
	a.transferGPU();
	b.transferGPU();
	MU.transferGPU();
	Tlocal_pre.transferGPU();
	tradeLin.transferGPU();
	LAMBDALin.transferGPU();
	Tmoy.transferGPU();

	Ap2.transferGPU();
	Ap1.transferGPU();
	Ap12.transferGPU();
	Bt1.transferGPU();
	Ct.transferGPU();
	Cp.transferGPU();
	CoresAgentLin.transferGPU();
	CoresLinAgent.transferGPU();
	CoresLinVoisin.transferGPU();
	CoresMatLin.transferGPU();

	Pmin.divideT(&nVoisin);
	Pmax.divideT(&nVoisin);
	Ap2.multiplyT(&nVoisin);
	Ap2.multiplyT(&nVoisin);
	Ap1.multiply(rho_p);
	Ap12.add(&Ap1, &Ap2);
	Cp.multiplyT(&nVoisin);
	Tmoy.divideT(&nVoisin);
	updateGlobalProbGPU();
}



void ADMMGPU4::updateP0(const StudyCase& cas)
{
	_id = _id + 1;
	//cudaDeviceSynchronize();
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

	
	matLb.transferCPU();
	Pmin.transferCPU();
	Pmax.transferCPU();
	Cp.transferCPU();
	nVoisin.transferCPU();
	b.transferCPU();

	Pmin = MatrixGPU(cas.getPmin());
	Pmax = MatrixGPU(cas.getPmax());


	MatrixGPU Lb(cas.getLb());
	b = cas.getb();
	Cp = cas.getb();
	int indice = 0;

	for (int idAgent = 0;idAgent < _n; idAgent++) {
		int Nvoisinmax = nVoisin.get(idAgent, 0);
		for (int voisin = 0; voisin < Nvoisinmax; voisin++) {
			matLb.set(indice, 0, Lb.get(idAgent, 0));
			indice = indice + 1;
		}
	}

	nVoisin.transferGPU();
	matLb.transferGPU();
	Pmin.transferGPU();
	Pmax.transferGPU();
	Cp.transferGPU();
	b.transferGPU();

	Pmin.divideT(&nVoisin);
	Pmax.divideT(&nVoisin);
	Cp.multiplyT(&nVoisin);

	//cudaDeviceSynchronize();
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	timePerBlock.increment(0, 8, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
	occurencePerBlock.increment(0, 8, 1);
}


void ADMMGPU4::solve(Simparam* result, const Simparam& sim, const StudyCase& cas)
{
#ifdef DEBUG_SOLVE
	cas.display();
	sim.display(1);
#endif // DEBUG_SOLVE
	
	clock_t tall = clock();
	std::chrono::high_resolution_clock::time_point t1;
	std::chrono::high_resolution_clock::time_point t2;
	// FB 0
	if (_id == 0) {
		//cudaDeviceSynchronize();
		t1 = std::chrono::high_resolution_clock::now();
		init(sim, cas);
		t2 = std::chrono::high_resolution_clock::now();
		timePerBlock.set(0, 0, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
		occurencePerBlock.set(0, 0, 1);
	}
	_rhog = sim.getRho();
	_at1 = _rhog; // 2*a
	
	float epsG = sim.getEpsG();
	float epsL = sim.getEpsL();
	int stepL = sim.getStepL();
	int stepG = sim.getStepG();
	int iterG = sim.getIterG();
	int iterL = sim.getIterL();
	

	float resG = 2 * epsG;
	float resL = 2 * epsL;
	int iterGlobal = 0;
	int iterLocal = 0;


	MatrixCPU nVoisinCPU(cas.getNvoi());
	MatrixCPU LAMBDA(sim.getLambda());
	MatrixCPU trade(sim.getTrade());
	MatrixCPU resF(2, (iterG / stepG) + 1);

	
	while ((iterGlobal < iterG) && (resG>epsG)) {
		resL = 2 * epsL;
		iterLocal = 0;
		while (iterLocal< iterL && resL>epsL) {
			// FB 1
			updateLocalProbGPU(&Tlocal, &P);
			if (!(iterLocal % stepL)) {
				// FB 2
				//cudaDeviceSynchronize();
				t1 = std::chrono::high_resolution_clock::now();
				resL = calcRes(&Tlocal, &P, &tempN1, &tempNN);
				//cudaDeviceSynchronize();
				t2 = std::chrono::high_resolution_clock::now();
				timePerBlock.increment(0, 4, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
			}
			
			Tlocal.swap(&Tlocal_pre); 
			iterLocal++;
		}
		occurencePerBlock.increment(0, 1, iterLocal);
		occurencePerBlock.increment(0, 2, iterLocal);
		occurencePerBlock.increment(0, 4, iterLocal / stepL);
		Tlocal.swap(&Tlocal_pre);
		tradeLin.swap(&Tlocal);
		// FB 3
		//cudaDeviceSynchronize();
		t1 = std::chrono::high_resolution_clock::now();
		updateGlobalProbGPU();
		//cudaDeviceSynchronize();
		t2 = std::chrono::high_resolution_clock::now();
		timePerBlock.set(0, 5, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
		if (!(iterGlobal % stepG)) { // FB 4
			//cudaDeviceSynchronize();
			t1 = std::chrono::high_resolution_clock::now();
			resG = updateRes(&resF, &Tlocal, iterGlobal / stepG, &tempNN);
			//cudaDeviceSynchronize();
			t2 = std::chrono::high_resolution_clock::now();
			timePerBlock.increment(0, 6, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
		}
		iterGlobal++;
	}
	occurencePerBlock.increment(0, 5, iterGlobal);
	occurencePerBlock.increment(0, 6, iterGlobal / stepG);

	// FB 5
	//cudaDeviceSynchronize();
	t1 = std::chrono::high_resolution_clock::now();

	updatePnGPU << <_numBlocks1, _blockSize >> > (Pn._matrixGPU, Tmoy._matrixGPU, nVoisin._matrixGPU, _n);
	float fc = calcFc(&a, &b, &tradeLin, &Pn, &Ct, &tempN1, &tempNN);
	std::cout << iterGlobal << " " << iterLocal << " " << resL << " " << resG << std::endl;
	MatrixCPU tradeLinCPU;
	tradeLin.toMatCPU(tradeLinCPU);
	MatrixCPU LAMBDALinCPU;
	LAMBDALin.toMatCPU(LAMBDALinCPU);
	MatrixCPU PnCPU;
	Pn.toMatCPU(PnCPU);
	MatrixCPU MUCPU;
	MU.toMatCPU(MUCPU);
	int indice = 0;
	for (int idAgent = 0;idAgent < _n; idAgent++) {
		MatrixCPU omega(cas.getVoisin(idAgent));
		int Nvoisinmax = nVoisinCPU.get(idAgent, 0);
		for (int voisin = 0; voisin < Nvoisinmax; voisin++) {
			int idVoisin = omega.get(voisin, 0);
			trade.set(idAgent, idVoisin, tradeLinCPU.get(indice, 0));
			LAMBDA.set(idAgent, idVoisin, LAMBDALinCPU.get(indice, 0));
			indice = indice + 1;
		}
	}

	result->setResF(&resF);
	result->setLAMBDA(&LAMBDA);
	result->setTrade(&trade);
	result->setIter(iterGlobal);
	result->setPn(&PnCPU);
	result->setFc(fc);
	result->setMU(&MUCPU);
	result->setRho(_rhog);
	//cudaDeviceSynchronize();
	t2 = std::chrono::high_resolution_clock::now();
	timePerBlock.increment(0, 7, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
	occurencePerBlock.increment(0, 7, 1);

	result->setTimeBloc(&timePerBlock, &occurencePerBlock);
	tall = clock() - tall;
	result->setTime((float)tall / CLOCKS_PER_SEC);
}

void ADMMGPU4::updateLocalProbGPU( MatrixGPU* Tlocal, MatrixGPU* P) {

	//cudaDeviceSynchronize();
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	updateTradeLin <<<_numBlocks1, _blockSize>>> ( Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU, matLb._matrixGPU, matUb._matrixGPU, CoresLinAgent._matrixGPU, _N);
	//cudaDeviceSynchronize();
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	timePerBlock.increment(0, 1, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());

	// Fb1b
	//cudaDeviceSynchronize();
	t1 = std::chrono::high_resolution_clock::now();
	
	updatePGPULin <<<_numBlocks2, _blockSize >>> (Tlocal->_matrixGPU, nVoisin._matrixGPU, MU._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU, _n);
	//cudaDeviceSynchronize();
	t2 = std::chrono::high_resolution_clock::now();
	timePerBlock.increment(0, 2, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
	
}



void ADMMGPU4::updateGlobalProbGPU()
{
	updateLAMBDAGPULin <<<_numBlocks2, _blockSize >> > (LAMBDALin._matrixGPU, tradeLin._matrixGPU, _rhog, CoresLinAgent._matrixGPU, CoresLinVoisin._matrixGPU, CoresMatLin._matrixGPU, _n,_N);
	updateBt1GPULin <<<_numBlocks2, _blockSize >> > (Bt1._matrixGPU, tradeLin._matrixGPU, _rhog, LAMBDALin._matrixGPU, CoresLinAgent._matrixGPU, CoresLinVoisin._matrixGPU, CoresMatLin._matrixGPU, _n, _N);
}



float ADMMGPU4::updateRes(MatrixCPU* res, MatrixGPU* Tlocal, int iter, MatrixGPU* tempNN)
{


	tempNN->subtract(Tlocal, &tradeLin);
	
	float resS = tempNN->max2();

	updateDiffGPU << <_numBlocks2, _blockSize >> > (tempNN->_matrixGPU, Tlocal->_matrixGPU, CoresLinAgent._matrixGPU, CoresLinVoisin._matrixGPU, CoresMatLin._matrixGPU, _n, _N);
	
	
	float resR = tempNN->max2();

	res->set(0, iter, resR);
	res->set(1, iter, resS);

	if (resR > _mu * resS) {
		_rhog = _tau * _rhog;
		_at1 = _rhog;
		
	}
	else if (resS > _mu * resR) {
		_rhog = _rhog / _tau;
		_at1 = _rhog;
		
	}

	return resR * (resR > resS) + resS * (resR <= resS);
	
}

float ADMMGPU4::calcRes( MatrixGPU* Tlocal, MatrixGPU* P, MatrixGPU* tempN1, MatrixGPU* tempNN)
{
	 tempNN->subtract(Tlocal, &Tlocal_pre);
	 tempN1->subtract(&Tmoy, P);
	 
	 float d1 = tempN1->max2();
	 
	 float d2 = tempNN->max2();
	 
	 return d1* (d1 > d2) + d2 * (d2 >= d1);
}


void ADMMGPU4::display() {

	std::cout << _name << std::endl;
}


