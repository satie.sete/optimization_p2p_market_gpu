#include "../head/MatrixGPU.cuh" 

static const int warpSize = 32;



float MatrixGPU::rand1()
{
    float a = (float)(rand()) / ((float)(RAND_MAX));
    return a;
}


///////////////////////////////////////////////////////////////////////////////
// Constructor
///////////////////////////////////////////////////////////////////////////////
MatrixGPU::MatrixGPU() {
#ifdef DEBUG_CONSTRUCTOR
    std::cout << "constructor" << std::endl;
#endif
    _row = 0;
    _column = 0;
    _N = _row * _column;
    _numBlocks = ceil((_N + _blockSize - 1) / _blockSize);
}

MatrixGPU::MatrixGPU(int l, int c, float value, int pos)
{
#ifdef DEBUG_CONSTRUCTOR
    std::cout << " param constructor " << std::endl;
    std::cout << _matrixCPU << std::endl;
#endif
    _row = l;
    _column = c;
    _N = _row * _column;
    _numBlocks = ceil((_N + _blockSize - 1) / _blockSize);
    if (!pos) {
        _matrixCPU = new float[l * c];
        for (int elem = 0; elem < l * c;elem++) {
            _matrixCPU[elem] = value;
        }
    } if (pos) {
        cudaMalloc((void**)&_matrixGPU, sizeof(float) * _row * _column);
        setGPU << <_numBlocks, _blockSize >> > (_matrixGPU, value, _N);
        _GPU = true;
    }
    

#ifdef DEBUG_CONSTRUCTOR
    std::cout << _matrixGPU << std::endl;
#endif
}

MatrixGPU::MatrixGPU(const MatrixCPU& m)
{
    _row = m.getNLin();
    _column = m.getNCol();
    _N = _row * _column;
    _numBlocks = ceil((_N + _blockSize - 1) / _blockSize);
    _matrixCPU = new float[_row * _column];

    memcpy(_matrixCPU, m._matrixCPU, _row*_column*sizeof(float));
}

MatrixGPU::MatrixGPU(const MatrixGPU & m)
{
#ifdef DEBUG_CONSTRUCTOR
    std::cout << "copy constructor" << std::endl;
#endif
    _row = m.getNLin();
    _column = m.getNCol();
    _N = _row * _column;
    _numBlocks = ceil((_N + _blockSize - 1) / _blockSize);

    if (m.getPos()) {
        cudaMalloc((void**)&_matrixGPU, sizeof(float) * _row * _column);
        setGPU <<<_numBlocks, _blockSize >>> (_matrixGPU, m._matrixGPU, _N);
        _GPU = true;
    }
    else {
        _matrixCPU = new float[_row * _column];
        memcpy(_matrixCPU, m._matrixCPU, _row * _column * sizeof(float));
    }
}

MatrixGPU& MatrixGPU::operator=(const MatrixGPU& m)
{
#ifdef DEBUG_CONSTRUCTOR
    std::cout << "= constructor" << std::endl;
#endif
    _row = m.getNLin();
    _column = m.getNCol();
    _N = _row * _column;
    _numBlocks = ceil((_N + _blockSize - 1) / _blockSize);
    if (getPos()) {
        _GPU = false;
        cudaFree(_matrixGPU);
    }
    else
    {
        DELETEA(_matrixCPU);
    }
    if (m.getPos()) {
        cudaMalloc((void**)&_matrixGPU, sizeof(float) * _row * _column);
        setGPU << <_numBlocks, _blockSize >> > (_matrixGPU, m._matrixGPU, _N);
        _GPU = true;
    }
    else {
        _matrixCPU = new float[_row * _column];
        memcpy(_matrixCPU, m._matrixCPU, _row * _column * sizeof(float));
    }
    
    
    return *this;
}

void MatrixGPU::preallocateReduction()
{
    cudaMalloc((void**)&_preallocation, sizeof(float) * _numBlocks);
    setGPU <<<_numBlocks, _blockSize >>> (_preallocation, 0.0f, _numBlocks);
}

void MatrixGPU::transferGPU()
{
    if (!_GPU) {
        cudaMalloc((void**)&_matrixGPU, sizeof(float) * _row * _column);
        cudaMemcpy(_matrixGPU, _matrixCPU, sizeof(float) * _row * _column, cudaMemcpyHostToDevice);
        DELETEA(_matrixCPU);
        _GPU = true;
    }
    else {
        throw std::domain_error("already in the GPU");
    }
    
}

void MatrixGPU::transferCPU()
{
    if (_GPU) {
        _matrixCPU = new float[_row * _column];
        cudaMemcpy(_matrixCPU, _matrixGPU, sizeof(float) * _row * _column, cudaMemcpyDeviceToHost);
        cudaFree(_matrixGPU);
        _matrixGPU = nullptr;
        _GPU = false;
    }
    else {
        throw std::domain_error("already in the CPU");
    }

}

///////////////////////////////////////////////////////////////////////////////
// Getter
///////////////////////////////////////////////////////////////////////////////
 float MatrixGPU::get(int i, int j) const
{
    
    if ((i >= _row) || ( j >= _column) || (i < 0) || ( j < 0)) {
        throw std::out_of_range("index out of bounds");
    }
    if (_GPU) {
        throw std::invalid_argument("Matrix on GPU");
    }
    else {
        return _matrixCPU[i * _column + j];
    }
}

int MatrixGPU::getNCol() const
{
    return _column;
}

int MatrixGPU::getNLin() const
{
    return _row;
}

bool  MatrixGPU::getPos() const
{
    return _GPU;
}
bool MatrixGPU::dim(MatrixGPU* m) const
{ 
    return ((_row == m->getNLin()) && (_column == m->getNCol()));
}


bool MatrixGPU::isEqual(MatrixGPU* m, float pre) const
{
    if (!dim(m)) {
        throw std::invalid_argument("not the same dimension");
    }
    else {
        if (_GPU || m->getPos()) {
            throw std::invalid_argument("Matrix on GPU");
        }
        else {
            for (int i = 0; i < _row; i++) {
                for (int j = 0; j < _column; j++) {
                    if (fabs(get(i, j) - m->get(i, j)) > pre) {
                        return false;
                    }
                }
            }
        }
    }
    return true;
}

void MatrixGPU::toMatCPU(MatrixCPU& m) const 
{
    m.setSize(_row, _column);
    if (_GPU) {
        cudaMemcpy(m._matrixCPU, _matrixGPU, sizeof(float) * _row * _column, cudaMemcpyDeviceToHost);
    }
    else {
        for (int i = 0; i < _row; i++) {
            for (int j = 0; j < _column; j++) 
            {
                m.set(i, j, get(i, j));
            }
        }
    }
}



///////////////////////////////////////////////////////////////////////////////
// Setter
///////////////////////////////////////////////////////////////////////////////
 void MatrixGPU::set(int i, int j, float value)
{
    if ((i >= _row) || (j >= _column) || (i < 0) || (j < 0)) {
        throw std::out_of_range("index out of bounds");
    }
     if (_GPU) {
        throw std::invalid_argument("Matrix on GPU");
    }
     
    _matrixCPU[i * _column + j] = value;
}

void MatrixGPU::set(MatrixGPU* m)
{
   
    if (!dim(m)) {
        throw std::invalid_argument("not the same dimension");
    }
    if (_GPU && m->getPos()) {
        setGPU<<<_numBlocks, _blockSize >>>(_matrixGPU, m->_matrixGPU, _N);
    }
    else if (!_GPU && !(m->getPos())) 
    {
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            { 
                this->set(i, j, m->get(i, j));
            }
        }
    }
    else {
        throw std::invalid_argument("Matrix not at the same place");
    }

}


void MatrixGPU::setRand(float eps)
{
    
    if (_GPU) {
        throw std::invalid_argument("Matrix on GPU");
    }
    int N = _column * _row;
    for (int elem = 0; elem < N; elem++) {
        _matrixCPU[elem] = 2 * (rand1() - 0.5) * eps;

    }
}

void MatrixGPU::setBloc(int iBegin, int iEnd, int jBegin, int jEnd, MatrixGPU* m)
{
    if ((iBegin < 0) || (jBegin < 0) || iEnd > _row || jEnd > _column) {
        throw std::out_of_range("index out of bounds");
    } if ((iBegin > iEnd) || (jBegin > jEnd)) {
        throw std::invalid_argument("xBegin must be smaller than xEnd");
    } if (m->getNLin() != (iEnd - iBegin) || m->getNCol() != (jEnd - jBegin)) {
        throw std::invalid_argument("not the same dimension");
    }
    if (!_GPU && !(m->getPos())) {
        int row = 0;

        for (int i = iBegin; i < iEnd; i++) {
            int col = 0;
            for (int j = jBegin; j < jEnd;j++) {
                set(i, j, m->get(row, col));
                col = col + 1;
            }
            row = row + 1;
        }
    }
    else {
        throw std::domain_error("Matrix on GPU");
    }
}
void MatrixGPU::setBloc(int iBegin, int iEnd, int jBegin, int jEnd, MatrixCPU* m)
{
    if ((iBegin < 0) || (jBegin < 0) || iEnd > _row || jEnd > _column) {
        throw std::out_of_range("index out of bounds");
    } if ((iBegin > iEnd) || (jBegin > jEnd)) {
        throw std::invalid_argument("xBegin must be smaller than xEnd");
    } if (m->getNLin() != (iEnd - iBegin) || m->getNCol() != (jEnd - jBegin)) {
        throw std::invalid_argument("not the same dimension");
    }
    if (!_GPU) {
        int row = 0;

        for (int i = iBegin; i < iEnd; i++) {
            int col = 0;
            for (int j = jBegin; j < jEnd;j++) {
                set(i, j, m->get(row, col));
                col = col + 1;
            }
            row = row + 1;
        }
    }
    else {
        throw std::domain_error("Matrix on GPU");
    }
}


void MatrixGPU::swap(MatrixGPU* m)
{
    if (!dim(m)) {
        throw std::invalid_argument("not the same dimension");
    }
    if (_GPU && m->getPos()) {
        float* temp = _matrixGPU;
        _matrixGPU = m->_matrixGPU;
        m->_matrixGPU = temp;

    }
    else if (!_GPU && !(m->getPos())) {
        float* temp = _matrixCPU;
        _matrixCPU = m->_matrixCPU;
        m->_matrixCPU = temp;
    }
    else {
        throw std::invalid_argument("Matrix not at the same place");
    }

    
    
}

void MatrixGPU::replace(float previous, float newValue)
{
    if (_GPU) {
        replaceGPU <<<_numBlocks, _blockSize >> > (_matrixGPU, previous, newValue, _N);
    }
    else {
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                if (get(i, j) == previous) {
                    this->set(i, j, newValue);
                }
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
// Addition
///////////////////////////////////////////////////////////////////////////////
void MatrixGPU::add(MatrixGPU* m)
{
    if (!dim(m)) {
        throw std::invalid_argument("not the same dimension");
    }
    if (_GPU && m->getPos()) 
    {
        addGPU<<<_numBlocks, _blockSize>>>(_matrixGPU,m->_matrixGPU,_N);
    }
    else if (!_GPU && !(m->getPos()))
    {
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                float r = get(i, j) + m->get(i, j);
                this->set(i, j, r);
            }
        }
    } else {
        throw std::invalid_argument("Matrix not at the same place");
    } 
}

void MatrixGPU::addVector(MatrixGPU* v)
{
    if (((v->getNCol() != 1) || (v->getNLin() != _row)) && ((v->getNLin() != 1) || (v->getNCol() != _column))) {
        throw std::invalid_argument("wrong dimension of the vector");
    }
    if (v->getNCol() == 1) {
        if (_GPU && v->getPos()) 
        {
            addVectorGPU1<<<_numBlocks, _blockSize >>>(_matrixGPU, v->_matrixGPU, _column, _N);
        }
        else if ((!_GPU) && !(v->getPos())) {
            for (int i = 0;i < _row;++i)
            {
                for (int j = 0;j < _column;++j)
                {
                    float r = get(i, j) + v->get(i, 0);
                    this->set(i, j, r);
                }
            }
        }
        else {
            throw std::invalid_argument("Matrix not at the same place");
        } 
    }
    else {
        if (_GPU && v->getPos())
        {
            addVectorGPU2<<<_numBlocks, _blockSize >>>(_matrixGPU, v->_matrixGPU, _column, _N);
        }
        else if ((!_GPU) && !(v->getPos())) {
            for (int i = 0;i < _row;++i)
            {
                for (int j = 0;j < _column;++j)
                {
                    float r = get(i, j) + v->get(0, j);
                    this->set(i, j, r);
                }
            }
        }
        else {
            throw std::invalid_argument("Matrix not at the same place");
        }
    }
}
void MatrixGPU::add(float c)
{
    if (_GPU) {
        addGPU<<<_numBlocks,_blockSize >>>(_matrixGPU,c, _N);
    }
    else {
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                float r = get(i, j) + c;
                this->set(i, j, r);
            }
        }
    }
}

void MatrixGPU::add(MatrixGPU* m1, MatrixGPU* m2)
{
    if (!m1->dim(m2)) {
        throw std::invalid_argument("not the same dimension");
    }
    if (!dim(m1)) {
        throw std::invalid_argument("not the same dimension");
    }
    if (_GPU && m1->getPos() && m2->getPos()) 
    {
        addGPU<<<_numBlocks, _blockSize >> > (_matrixGPU, m1->_matrixGPU,m2->_matrixGPU, _N);
    }
    else if (!_GPU && !(m1->getPos()) && !(m2->getPos()))
    {
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                float r = m1->get(i, j) + m2->get(i, j);
                this->set(i, j, r);
            }
        }
    }
    else {
        throw std::invalid_argument("Matrix not at the same place");
    }
    
}
void MatrixGPU::add(MatrixGPU* m, float c)
{
    if (_GPU && m->getPos()) 
    {
        addGPU<<<_numBlocks, _blockSize >>> (_matrixGPU,m->_matrixGPU, c, _N);
    }
    else if ((!_GPU) && !(m->getPos())) 
    {
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                float r = m->get(i, j) + c;
                this->set(i, j, r);
            }
        }
    }
    else {
        throw std::invalid_argument("Matrix not at the same place");
    }
    

}
void MatrixGPU::addTrans(MatrixGPU* m)
{
    MatrixGPU temp(*this);
    if (_row != m->getNCol() && _column != m->getNLin())
    {
        throw std::invalid_argument("not the same dimension (transpose)");
    }
    if (_GPU && m->getPos())
    {
        addTransGPU<<<_numBlocks, _blockSize >>>(temp._matrixGPU, _matrixGPU, m->_matrixGPU,_column,_row,_N);
    }
    else if (!_GPU && !(m->getPos()))
    {
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                float r = get(i, j) + m->get(j, i);
                temp.set(i, j, r);
            }
        }
    }
    else {
        throw std::invalid_argument("Matrix not at the same place");
    }
    this->set(&temp);
    
}
///////////////////////////////////////////////////////////////////////////////
// subtraction
///////////////////////////////////////////////////////////////////////////////
void MatrixGPU::subtract(MatrixGPU* m1, MatrixGPU* m2)
{
    if (!m1->dim(m2)) {
        throw std::invalid_argument("not the same dimension");
        
    }
    if (!dim(m1)) {
        throw std::invalid_argument("not the same dimension");
    }
    if (_GPU && m1->getPos() && m2->getPos())
    {
        substractGPU<<<_numBlocks, _blockSize >> > (_matrixGPU, m1->_matrixGPU, m2->_matrixGPU, _N);
    }
    else if (!_GPU && !(m1->getPos()) && !(m2->getPos()))
    {
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                float r = m1->get(i, j) - m2->get(i, j);
                this->set(i, j, r);
            }
        }
    }
    else {
        throw std::invalid_argument("Matrix not at the same place");
    }
    
}
void MatrixGPU::subtract(MatrixGPU* m)
{
    
    if (!dim(m)) {
        throw std::invalid_argument("not the same dimension");
    }
    if (_GPU && m->getPos())
    {
        substractGPU <<<_numBlocks, _blockSize >> > ( _matrixGPU, m->_matrixGPU, _N);
    }
    else if (!_GPU && !(m->getPos()))
    {
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                float r = get(i, j) - m->get(i, j);
                set(i, j, r);
            }
        }
    }
    else {
        throw std::invalid_argument("Matrix not at the same place");
    }
   
    
}
void MatrixGPU::subtractVector(MatrixGPU* v)
{
    if (((v->getNCol() != 1) || (v->getNLin() != _row)) && ((v->getNLin() != 1) || (v->getNCol() != _column))) {
        throw std::invalid_argument("wrong dimension of the vector");
    }
    if (v->getNCol() == 1) {
        if (_GPU && v->getPos())
        {
            substractVectorGPU1 <<<_numBlocks, _blockSize >>>(_matrixGPU, v->_matrixGPU, _column, _N);
        }
        else if ((!_GPU) && !(v->getPos())) {
            for (int i = 0;i < _row;++i)
            {
                for (int j = 0;j < _column;++j)
                {
                    float r = get(i, j) - v->get(i, 0);
                    this->set(i, j, r);
                }
            }
        }
        else {
            throw std::invalid_argument("Matrix not at the same place");
        }
    }
    else {
        if (_GPU && v->getPos())
        {
            substractVectorGPU2 <<<_numBlocks, _blockSize >>>(_matrixGPU, v->_matrixGPU, _column, _N);
        }
        else if ((!_GPU) && !(v->getPos())) {
            for (int i = 0;i < _row;++i)
            {
                for (int j = 0;j < _column;++j)
                {
                    float r = get(i, j) - v->get(0, j);
                    this->set(i, j, r);
                }
            }
        }
        else {
            throw std::invalid_argument("Matrix not at the same place");
        }
    }

}
void MatrixGPU::subtractTrans(MatrixGPU* m)
{
    if (_row != m->getNCol() && _column != m->getNLin())
    {
        throw std::invalid_argument("not the same dimension (transpose)");
    }
    MatrixGPU temp(*this);
    if (_GPU && m->getPos())
    {
        substractTransGPU <<<_numBlocks, _blockSize >>>(temp._matrixGPU, _matrixGPU, m->_matrixGPU, _column, _row, _N);
    }
    else if (!_GPU && !(m->getPos()))
    {
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                float r = get(i, j) - m->get(j, i);
                temp.set(i, j, r);
            }
        }
    }
    else {
        throw std::invalid_argument("Matrix not at the same place");
    }
    this->set(&temp);
}

///////////////////////////////////////////////////////////////////////////////
// Multiplication
///////////////////////////////////////////////////////////////////////////////


void MatrixGPU::multiply(float c)
{
    if (_GPU) {
        multiplyGPU<<<_numBlocks, _blockSize >>> (_matrixGPU, c, _N);
    }
    else {
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                float r = get(i, j) * c;
                this->set(i, j, r);
            }
        }
    }
        
}

///////////////////////////////////////////////////////////////////////////////
// term-to-term multiplication 
///////////////////////////////////////////////////////////////////////////////

void MatrixGPU::multiplyT(MatrixGPU* m)
{
    if (!dim(m)) {
        throw std::invalid_argument("not the same dimension");
    }
    if (_GPU && m->getPos())
    {
        multiplyTGPU <<<_numBlocks, _blockSize >>> (_matrixGPU, m->_matrixGPU, _N);
    }
    else if (!_GPU && !(m->getPos()))
    {
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                float r = get(i, j) * m->get(i, j);
                this->set(i, j, r);
            }
        }
    }
    else {
        throw std::invalid_argument("Matrix not at the same place");
    }
}

void MatrixGPU::multiplyT(MatrixGPU* m1, MatrixGPU* m2)
{
    if (!m1->dim(m2)) {
        throw std::invalid_argument("not the same dimension");
    }
    if (!dim(m1)) {
        throw std::invalid_argument("not the same dimension");
    }
    if (_GPU && m1->getPos() && m2->getPos())
    {
        multiplyTGPU<<<_numBlocks, _blockSize >>>(_matrixGPU, m1->_matrixGPU, m2->_matrixGPU, _N);
    }
    else if (!_GPU && !(m1->getPos()) && !(m2->getPos()))
    {
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                float r = m1->get(i, j) * m2->get(i, j);
                this->set(i, j, r);
            }
        }
    }
    else {
        throw std::invalid_argument("Matrix not at the same place");
    }
}


void MatrixGPU::divide(float c)
{
    if (c == 0) {
        throw std::domain_error("divide by 0");
    }
    if (_GPU) {
        divideGPU <<<_numBlocks, _blockSize >>> (_matrixGPU, c, _N);
    }
    else {
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                float r = get(i, j) / c;
                this->set(i, j, r);
            }
        }
    }
    
}

void MatrixGPU::divideT(MatrixGPU* m)
{
    
    if (!dim(m)) {
        throw std::invalid_argument("not the same dimension");
    }
    if (_GPU && m->getPos())
    {
        divideGPU<<<_numBlocks, _blockSize >>>(_matrixGPU, m->_matrixGPU, _N);
    }
    else if (!_GPU && !(m->getPos()))
    {
        MatrixGPU temp(*this);
        float r = 0;
        float f = 0;
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                f = m->get(i, j);
                if (f == 0) {
                    throw std::domain_error("divide by 0");
                }
                r = get(i, j) / f;
                temp.set(i, j, r);
            }
        }
        set(&temp);
    }
    else {
        throw std::invalid_argument("Matrix not at the same place");
    }
    
}

///////////////////////////////////////////////////////////////////////////////
// Other
///////////////////////////////////////////////////////////////////////////////

float MatrixGPU::max2() const
{
    if (_row == 0 || _column == 0) {
        throw std::out_of_range("Empty Matrix");
    }
    if (!_GPU) {
        float M = fabs(get(0, 0));
        float m = 0;
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                m = fabs(get(i, j));
                if (m > M) {
                    M = m;
                }
            }
        }
        return M;
    } 
    else {
        
        int numBlocks = _numBlocks;
        unsigned int n = _N;
        float odata = 0;
        float* d_odata;
        if (_preallocation != nullptr) {
           
            d_odata = _preallocation;
        }
        else {
          
            cudaMalloc((void**)&d_odata, sizeof(float) * numBlocks);
        }
        
        switch (_blockSize) {
        case 512:
            maxMultiBlock<512> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            maxMonoBlock<512> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 256:
            maxMultiBlock<256> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            maxMonoBlock<256> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 128:
            maxMultiBlock<128> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            maxMonoBlock<128> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 64:
            maxMultiBlock< 64> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            maxMonoBlock< 64> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 32:
            maxMultiBlock< 32> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            maxMonoBlock< 32> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 16:
            maxMultiBlock< 16> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            maxMonoBlock< 16> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  8:
            maxMultiBlock<  8> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            maxMonoBlock< 8> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  4:
            maxMultiBlock<  4> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            maxMonoBlock<  4> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  2:
            maxMultiBlock<  2> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            maxMonoBlock<  2> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  1:
            maxMultiBlock<  1> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            maxMonoBlock<  1> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        }
        cudaMemcpy(&odata, d_odata, sizeof(float), cudaMemcpyDeviceToHost);
        if (_preallocation == nullptr) {
            cudaFree(d_odata);
        }
        return sqrt(odata); 
    }
}


float MatrixGPU::max2(MatrixGPU* m) const
{
    if (_row == 0 || _column == 0) {
        throw std::out_of_range("Empty Matrix");
    }
    if (!_GPU && !(m->getPos())) 
    {
        float M = fabs(get(0, 0));
        float f = 0;
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                f = fabs(get(i, j)- m->get(i,j));
                if (f > M) {
                    M = f;
                }
            }
        }
        return M;
    }
    else if (_GPU && m->getPos()) {
        int numBlocks = _numBlocks;
        unsigned int n = _N;
        float odata = 0;
        float* d_odata;
        if (_preallocation != nullptr) {
            d_odata = _preallocation;
        }
        else {
            cudaMalloc((void**)&d_odata, sizeof(float) * numBlocks);
        }
        

        switch (_blockSize) {
        case 512:
            maxMultiBlock<512> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            maxMonoBlock<512> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 256:
            maxMultiBlock<256> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            maxMonoBlock<256> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 128:
            maxMultiBlock<128> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            maxMonoBlock<128> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 64:
            maxMultiBlock< 64> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            maxMonoBlock< 64> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 32:
            maxMultiBlock< 32> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            maxMonoBlock< 32> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 16:
            maxMultiBlock< 16> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            maxMonoBlock< 16> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  8:
            maxMultiBlock<  8> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            maxMonoBlock< 8> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  4:
            maxMultiBlock<  4> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            maxMonoBlock<  4> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  2:
            maxMultiBlock<  2> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            maxMonoBlock<  2> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  1:
            maxMultiBlock<  1> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            maxMonoBlock<  1> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        }
        
        cudaMemcpy(&odata, d_odata, sizeof(float), cudaMemcpyDeviceToHost);
        if (_preallocation == nullptr) {
            cudaFree(d_odata);
        }
        return sqrt(odata);
    }
    else {
        throw std::invalid_argument("Matrix not at the same place");
    }
}

float MatrixGPU::distance2(MatrixGPU* m)
{
    if (!dim(m)) {
        throw std::invalid_argument("not the same size");
    }
    if (_GPU && m->getPos())
    {
        int numBlocks = _numBlocks;
        unsigned int n = _N;
        float odata = 0;
        float* d_odata;
        if (_preallocation != nullptr) {
            d_odata = _preallocation;
        }
        else {
            cudaMalloc((void**)&d_odata, sizeof(float) * numBlocks);
        }
        

        switch (_blockSize) {
        case 512:
            distanceMultiBlock<512> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            sumMonoBlock<512> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 256:
            distanceMultiBlock<256> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            sumMonoBlock<256> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 128:
            distanceMultiBlock<128> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            sumMonoBlock<128> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 64:
            distanceMultiBlock< 64> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            sumMonoBlock< 64> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 32:
            distanceMultiBlock< 32> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            sumMonoBlock< 32> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 16:
            distanceMultiBlock< 16> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            sumMonoBlock< 16> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  8:
            distanceMultiBlock<  8> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            sumMonoBlock< 8> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  4:
            distanceMultiBlock<  4> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            sumMonoBlock<  4> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  2:
            distanceMultiBlock<  2> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            sumMonoBlock<  2> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  1:
            distanceMultiBlock<  1> << <numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, d_odata, n);
            sumMonoBlock<  1> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        }
        
        cudaMemcpy(&odata, d_odata, sizeof(float), cudaMemcpyDeviceToHost);
        if (_preallocation == nullptr) {
            cudaFree(d_odata);
        }


        return sqrtf(odata);
    }
    else if (!_GPU && !(m->getPos()))
    {
        float d = 0;
        float r = 0;
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                r = get(i, j) - m->get(i, j);
                d = d + r * r;
            }
        }
        return sqrtf(d);
    }
    else {
        throw std::invalid_argument("Matrix not at the same place");
    }
}

void MatrixGPU::Moy(MatrixGPU* m, MatrixGPU* nb, int sens)
{
    float s;
    int n;
    if (sens) { 
        if ((_row != 1) || (_column != m->getNCol()) || (_column != nb->getNCol()) || (nb->getNLin() != 1))
        {
            throw std::invalid_argument("wrong dimension of the vector");
        }
        if (_GPU && nb->getPos() && m->getPos())
        {
            
            moyGPU1 <<<_numBlocks, _blockSize >>> (_matrixGPU, m->_matrixGPU, nb->_matrixGPU, m->getNLin(), _column);
        }
        else if ((!_GPU) && !(nb->getPos()) && !(m->getPos())) {
            for (int j = 0; j < _column;j++)
            {
                n = nb->get(0, j);
                s = 0;
                if (n > 0)
                {
                    for (int i = 0; i < m->getNLin();i++)
                    {
                        s = s + m->get(i, j);
                    }
                    s = s / n;
                }
                set(0, j, s);
            }
        }
        else {
            throw std::invalid_argument("Matrix not at the same place");
        }
        

    }
    else { 
        if ((_column != 1) || (_row != m->getNLin()) || (_row != nb->getNLin()) || (nb->getNCol() != 1)) {
            throw std::invalid_argument("wrong dimension of the vector");
        }
        if (_GPU && nb->getPos())
        {
            
            moyGPU2<<<_numBlocks, _blockSize >> > (_matrixGPU, m->_matrixGPU, nb->_matrixGPU, _row , m->getNCol());
        }
        else if ((!_GPU) && !(nb->getPos())) {
            for (int i = 0; i < _row;i++)
            {
                n = nb->get(i, 0);
                s = 0;
                if (n > 0) {
                    for (int j = 0; j < m->getNCol();j++)
                    {
                        s = s + m->get(i, j);
                    }
                    s = s / n;
                }
                set(i, 0, s);
            }
        }
        else {
            throw std::invalid_argument("Matrix not at the same place");
        }

    }
}

void MatrixGPU::project(MatrixGPU* Lb, MatrixGPU* Ub)
{
    if (!dim(Lb) || !dim(Ub)) {
        throw std::invalid_argument("not the same dimension");
    }
    if (_GPU && Lb->getPos() && Ub->getPos())
    {
        projectGPU<<<_numBlocks, _blockSize >>>(_matrixGPU, Lb->_matrixGPU, Ub->_matrixGPU, _N);
    }
    else if (!_GPU && !(Lb->getPos()) && !(Ub->getPos()))
    {
        float ub = 0;
        float lb = 0;
        float r = 0;
        MatrixGPU temp(*this);
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                r = get(i, j);
                ub = Ub->get(i, j);
                lb = Lb->get(i, j);
                if (ub < lb) {
                    throw std::invalid_argument("impossible to have a value for the projection, ub>lb");
                }
                r = (ub - r) * (r > ub) + (lb - r) * (r < lb) + r; 
                temp.set(i, j, r);
            }
        }
        this->set(&temp);
    }
    else {
        throw std::invalid_argument("Matrix not at the same place");
    }
    
}

float MatrixGPU::sum() const
{
    if (_GPU) 
    {
        int numBlocks = _numBlocks;
        unsigned int n = _N;
        float odata = 0;
        float* d_odata;
        if (_preallocation != nullptr) {
            d_odata = _preallocation;
        }
        else {
            cudaMalloc((void**)&d_odata, sizeof(float) * numBlocks);
        }


        switch (_blockSize) {
        case 512:
            SumMultiBlock<512> <<<numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock<512> <<< 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 256:
            SumMultiBlock<256> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock<256> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 128:
            SumMultiBlock<128> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock<128> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 64:
            SumMultiBlock< 64> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock< 64> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 32:
            SumMultiBlock< 32> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock< 32> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 16:
            SumMultiBlock< 16> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock< 16> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  8:
            SumMultiBlock<  8> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock< 8> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  4:
            SumMultiBlock<  4> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock<  4> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  2:
            SumMultiBlock<  2> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock<  2> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  1:
            SumMultiBlock<  1> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock<  1> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        }
       
        cudaMemcpy(&odata, d_odata, sizeof(float), cudaMemcpyDeviceToHost);
        if (_preallocation == nullptr) {
            cudaFree(d_odata);
        }
        return odata;
    }
    else if (!_GPU)
    {
        float d = 0;
        float r = 0;
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                r = get(i, j);
                d = d + r;
            }
        }
        return d;
    }
}

void MatrixGPU::sum(MatrixGPU* m)
{
    float s = 0;
    
    if ((_column != 1) || (_row != m->getNLin())) {
        throw std::invalid_argument("wrong dimension of the column vector ");
    }
   
    if (_GPU && m->getPos())
    {
        sumGPU <<<_numBlocks, _blockSize >>>(_matrixGPU, m->_matrixGPU, m->getNLin(), m->getNCol());
    }
    else if (!_GPU && !(m->getPos()))
    {
        for (int i = 0; i < _row;i++)
        {
            s = 0;
            for (int j = 0; j < m->getNCol();j++)
            {
                s = s + m->get(i, j);
            }
            set(i, 0, s);
        }
    }
    else {
        throw std::invalid_argument("Matrix not at the same place");
    }
}

float MatrixGPU::distance2() {

    if (_GPU ) 
    {
        int numBlocks = _numBlocks;
        unsigned int n = _N;
        float* d_odata;
        if (_preallocation != nullptr) {
            d_odata = _preallocation;
        }
        else {
            cudaMalloc((void**)&d_odata, sizeof(float) * numBlocks);
        }
        float odata = 0;
        
       
 
        switch (_blockSize) {
        case 512:
            distanceMultiBlock<512> << <numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock<512> << < 1, _blockSize >> > (d_odata, d_odata, numBlocks);
        break;
        case 256:
            distanceMultiBlock<256> <<<numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock<256> <<< 1, _blockSize >> > (d_odata, d_odata, numBlocks);
        break;
        case 128:
            distanceMultiBlock<128> <<<numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock<128> <<< 1, _blockSize >> > (d_odata, d_odata, numBlocks);
        break;
        case 64:
            distanceMultiBlock< 64> <<<numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock< 64> <<< 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 32:
            distanceMultiBlock< 32> <<<numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock< 32> <<< 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case 16:
            distanceMultiBlock< 16> <<<numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock< 16> <<< 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  8:
            distanceMultiBlock<  8> <<<numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock< 8> <<< 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  4:
            distanceMultiBlock<  4> <<<numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock<  4> <<< 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  2:
            distanceMultiBlock<  2> <<<numBlocks, _blockSize >> > (_matrixGPU, d_odata, n);
            sumMonoBlock<  2> <<< 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break;
        case  1:
            distanceMultiBlock<  1> <<<numBlocks, _blockSize >>> (_matrixGPU, d_odata, n);
            sumMonoBlock<  1> <<< 1, _blockSize >> > (d_odata, d_odata, numBlocks);
            break; 
        }
        
        cudaMemcpy(&odata, d_odata, sizeof(float), cudaMemcpyDeviceToHost);
        if (_preallocation == nullptr) {
            cudaFree(d_odata);
        }
        return sqrtf(odata);
    }
    else if (!_GPU)
    {
        float d = 0;
        float r = 0;
        for (int i = 0;i < _row;++i)
        {
            for (int j = 0;j < _column;++j)
            {
                r = get(i, j);
                d = d + r * r;
            }
        }
        return sqrtf(d); 
    }
}



///////////////////////////////////////////////////////////////////////////////
// Display MatrixGPU contents
///////////////////////////////////////////////////////////////////////////////
void MatrixGPU::display() const
{   
    if (this) {
        if (_GPU) {
            std::cout << " Matrix on GPU, use transfertCPU before " << std::endl;
        }
        else {
            if (_row == 0 || _column == 0)
            {
                std::cout << "empty " << std::endl;
            }
            if (_column == 1) {
                std::cout << " transpose  : ";
                for (int i = 0;i < _row;++i)
                {
                    for (int j = 0;j < _column;++j)
                    {
                        float value = get(i, j);
                        std::cout << std::setprecision(7) << value;
                        
                        std::cout << " ";
                    }
                }
                std::cout << std::endl;
            }
            else {
                for (int i = 0;i < _row;++i)
                {
                    for (int j = 0;j < _column;++j)
                    {
                        float value = get(i, j);
                        std::cout << std::setprecision(7) << value;
                        
                        if (j != _column - 1) std::cout << " ";
                    }

                    std::cout << std::endl;
                }
                std::cout << std::endl;
            }
        }
    }
    else 
    {
        std::cout << "not defined " << std::endl;
    }
}




///////////////////////////////////////////////////////////////////////////////
// Destructor
///////////////////////////////////////////////////////////////////////////////
MatrixGPU::~MatrixGPU()
{
    #ifdef DEBUG_DESTRUCTOR
        std::cout << " matrix destructor" << _matrixGPU << std::endl;
    #endif // DEBUG_DESTRUCTOR

    if (_preallocation != nullptr) {
        cudaFree(_preallocation);
    }
    if(_GPU) {
        cudaFree(_matrixGPU);
        _matrixGPU = nullptr;
    } else {
        DELETEA(_matrixCPU);
    }
}



void MatrixGPU::saveCSV(const std::string& filename, std::ios_base::openmode mode, int trans) const
{
    if (_GPU) {
        throw std::domain_error("Matrix on GPU");
    }
    std::ofstream myfile;
    myfile.open(filename, mode);
    myfile.precision(10);
    if (!trans) {
        for (int i = 0; i < _row; i++) {
            for (int j = 0; j < _column;j++) {
                myfile << get(i, j) << ";";
            }
            myfile << "\n";
        }
    }
    else {
        for (int j = 0; j < _column;j++) {
            for (int i = 0; i < _row; i++) {
                myfile << get(i, j) << ";";
            }
            myfile << "\n";
        }
    }

    myfile.close();
}


///////////////////////////////////////////////////////////////////////////////
// GPU function
///////////////////////////////////////////////////////////////////////////////


__global__ void setup_kernel(curandState* state) {

    int idx = threadIdx.x + blockDim.x * blockIdx.x;
    curand_init(1234, idx, 0, &state[idx]);
}


__global__ void generate_kernel(curandState* my_curandstate, float* result, float eps, const unsigned int N) {

    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        result[i] = curand_uniform(my_curandstate + i) * eps;
    }
}





__global__ void setGPU(float* mat1, float* mat2, int N) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        mat1[i] = mat2[i];
    }
}

__global__ void setGPU(float* mat1, const float value, int N) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        mat1[i] = value;
    }
}

__global__ void replaceGPU(float* mat,const float previous, const float newValue,const int N) 
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        mat[i] = (mat[i] == previous) * (newValue-mat[i]) + mat[i];
    }
}




__global__ void addGPU(float* mat, float c, int N) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        mat[i] = mat[i] + c;
    }
}
__global__ void addGPU(float* mat1, float* mat2, float c, int N) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        mat1[i] = mat2[i] + c;
    }
}
__global__ void addGPU(float* mat1, float* mat2, int N) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        mat1[i] = mat1[i] + mat2[i];
    }
}
__global__ void addGPU(float* mat1, float* mat2, float* mat3, int N) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        mat1[i] = mat2[i] + mat3[i];
    }
}

__global__ void addVectorGPU1(float* mat1, float* vect, const int n, int N)
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        int k = i / n; 
        mat1[i] = mat1[i] + vect[k];
    }

}
__global__ void addVectorGPU2(float* mat1, float* vect, const int n, int N) 
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        int k = i % n; 
        mat1[i] = mat1[i] + vect[k];
    }


}

__global__ void addTransGPU(float* out, float* mat1, float* mat2, const int col, const int line, int N) 
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int l = index; l < N; l += step)
    {
        int i = l / col;
        int j = l % col;
        int k = i + j * line;
        out[l] = mat1[l] + mat2[k];
    }
}

__global__ void substractGPU(float* mat1, float* mat2, int N) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        mat1[i] = mat1[i] - mat2[i];
    }
}
__global__ void substractGPU(float* mat1, float* mat2, float* mat3, int N) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        mat1[i] = mat2[i] - mat3[i];
    }
}

__global__ void substractVectorGPU1(float* mat1, float* vect, const int n, int N) 
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        int k = i / n; 
        mat1[i] = mat1[i] - vect[k];
    }

}
__global__ void substractVectorGPU2(float* mat1, float* vect, const int n, int N) 
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        int k = i % n; 
        mat1[i] = mat1[i] - vect[k];
    }

}

__global__ void substractTransGPU(float* out, float* mat1, float* mat2, const int col, const int line, int N)
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int l = index; l < N; l += step)
    {
        int i = l / col;
        int j = l % col;
        int k = i + j * line;
        out[l] = mat1[l] - mat2[k];
    }
}

__global__ void multiplyGPU(float* mat, const float c, int N) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        mat[i] = mat[i] * c;
    }
}

__global__ void multiplyTGPU(float* mat1, float* mat2, int N) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        mat1[i] = mat1[i] * mat2[i];
    }
}
__global__ void multiplyTGPU(float* mat1, float* mat2, float* mat3, int N) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        mat1[i] = mat2[i] * mat3[i];
    }
}

__global__ void divideGPU(float* mat, const float c, int N) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        mat[i] = mat[i] / c;
    }
}
__global__ void divideGPU(float* mat1, float* mat2, int N) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        mat1[i] = mat1[i] / mat2[i];
    }
}

__global__ void moyGPU1(float* res, float* mat1, float* nb, const int line, const int column) 
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
     
    for (int i = index; i < column; i += step)
    {
        float s = 0.0;
        for (int j = 0; j < line; j++)
        {
            s += mat1[i + column *j];
        }
        res[i] = s / nb[i];
    }

}
__global__ void moyGPU2(float* res, float* mat1, float* nb, const int line, const int column) 
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    
    for (int i = index; i < line; i += step)
    {
        float s = 0.0;
        for (int j = 0; j < column; j++)
        {
            s +=  mat1[i*column + j];
        }
        res[i] = s /nb[i];
    }
}

__global__ void projectGPU(float* mat, float* Lb, float* Ub, int N) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;
    for (int i = index; i < N; i += step)
    {
        float r = mat[i];
        float ub = Ub[i];
        float lb = Lb[i];
        r = (ub - r) * (r > ub) + (lb - r) * (r < lb) + r;
        mat[i] = r;
    }
}

__global__ void sumGPU(float* res, float* mat1, const int line, const int column) {
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int step = blockDim.x * gridDim.x;

    for (int i = index; i < line; i += step)
    {
        float s = 0.0;
        for (int j = 0; j < column; j++)
        {
            s += mat1[i*column + j];
        }
        res[i] = s;
    }
}

__global__ void sumGPU2(float* res, float* mat1, const int line) 
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;

    if(index==0)
    {
        float s = 0.0;
        for (int j = 0; j < line; j++)
        {
            s += mat1[j];
        }
        
        *res = s ;
    }
}


template <unsigned int blockSize>
__device__ void warpReduce(volatile float* sdata, unsigned int tid) {
    if (blockSize >= 64) sdata[tid] += sdata[tid + 32];
    if (blockSize >= 32) sdata[tid] += sdata[tid + 16];
    if (blockSize >= 16) sdata[tid] += sdata[tid + 8];
    if (blockSize >= 8) sdata[tid] += sdata[tid + 4];
    if (blockSize >= 4) sdata[tid] += sdata[tid + 2];
    if (blockSize >= 2) sdata[tid] += sdata[tid + 1];
}
__device__ int sumCommSingleWarp(volatile float* shArr) {
    int idx = threadIdx.x % warpSize; 
    if (idx < 16) {
        shArr[idx] += shArr[idx + 16];
        shArr[idx] += shArr[idx + 8];
        shArr[idx] += shArr[idx + 4];
        shArr[idx] += shArr[idx + 2];
        shArr[idx] += shArr[idx + 1];
    }
    return shArr[0];
}

template <unsigned int blockSize>
__global__ void sumMonoBlock(float* g_idata, float* g_odata, unsigned int n) {
    
    int idx = threadIdx.x;
    float sum = 0;
    for (int i = idx; i < n; i += blockSize)
        sum += g_idata[i];
    __shared__ float r[blockSize];
    r[idx] = sum;
    sumCommSingleWarp(&r[idx & ~(warpSize - 1)]);
    __syncthreads();
    if (idx < warpSize) { 
        r[idx] = idx * warpSize < blockSize ? r[idx * warpSize] : 0;
        sumCommSingleWarp(r);
        if (idx == 0)
            *g_odata = r[0];
    }
}


template <unsigned int blockSize>
__global__ void SumMultiBlock(float* g_idata, float* g_odata, unsigned int n) {

    __shared__ float shArr[blockSize];
    int thIdx = threadIdx.x;
    int gthIdx = thIdx + blockIdx.x * blockSize;
    const int gridSize = blockSize * gridDim.x;
    float sum = 0;
    for (int i = gthIdx; i < n; i += gridSize)
       sum += g_idata[i];
    
    shArr[thIdx] = sum;
    __syncthreads();
    for (int size = blockSize / 2; size > 0; size /= 2) { 
        if (thIdx < size)
            shArr[thIdx] += shArr[thIdx + size];
        __syncthreads();
    }
    if (thIdx == 0) {
        g_odata[blockIdx.x] = shArr[0];
    }
}

template <unsigned int blockSize>
__global__ void distanceMultiBlock(float* g_idata, float* g_odata, unsigned int n) {
    int thIdx = threadIdx.x;
    int gthIdx = thIdx + blockIdx.x * blockSize;
    const int gridSize = blockSize * gridDim.x;
    float sum = 0;
    for (int i = gthIdx; i < n; i += gridSize)
        sum += (g_idata[i] * g_idata[i]);
    __shared__ float shArr[blockSize];
    shArr[thIdx] = sum;
    __syncthreads();
    for (int size = blockSize / 2; size > 0; size /= 2) { 
        if (thIdx < size)
            shArr[thIdx] += shArr[thIdx + size];
        __syncthreads();
    }
    if (thIdx == 0)
        g_odata[blockIdx.x] = shArr[0];
   
}



template <unsigned int blockSize>
__global__ void distanceMultiBlock(float* g_idata, float* g_idata2, float* g_odata, unsigned int n) {
    int thIdx = threadIdx.x;
    int gthIdx = thIdx + blockIdx.x * blockSize;
    const int gridSize = blockSize * gridDim.x;
    float sum = 0;
    for (int i = gthIdx; i < n; i += gridSize)
        sum += ((g_idata[i]- g_idata2[i]) * (g_idata[i] - g_idata2[i]));
    __shared__ float shArr[blockSize];
    shArr[thIdx] = sum;
    __syncthreads();
    for (int size = blockSize / 2; size > 0; size /= 2) { 
        if (thIdx < size)
            shArr[thIdx] += shArr[thIdx + size];
        __syncthreads();
    }
    if (thIdx == 0)
        g_odata[blockIdx.x] = shArr[0];

}

__device__ float warpReduceMax(volatile float* r) {
    int idx = threadIdx.x % warpSize; //the lane index in the warp
    if (idx < 16) {
        r[idx] = r[idx + 16] > r[idx] ? r[idx + 16] : r[idx];//r[idx + 16] > r[idx] ? r[idx + 16] : r[idx];//r[idx + 16] * (r[idx + 16] > r[idx]) + r[idx] * (r[idx] <= r[idx + 16]);
        r[idx] = r[idx + 8] > r[idx] ? r[idx + 8] : r[idx];//r[idx +  8] > r[idx] ? r[idx +  8] : r[idx];//r[idx +  8] * (r[idx +  8] > r[idx]) + r[idx] * (r[idx] <= r[idx +  8]);
        r[idx] = r[idx + 4] > r[idx] ? r[idx + 4] : r[idx];//r[idx +  4] > r[idx] ? r[idx +  4] : r[idx];//r[idx +  4] * (r[idx +  4] > r[idx]) + r[idx] * (r[idx] <= r[idx +  4]);
        r[idx] = r[idx + 2] > r[idx] ? r[idx + 2] : r[idx];//r[idx +  2] > r[idx] ? r[idx +  2] : r[idx];//r[idx +  2] * (r[idx +  2] > r[idx]) + r[idx] * (r[idx] <= r[idx +  2]);
        r[idx] = r[idx + 1] > r[idx] ? r[idx + 1] : r[idx];//r[idx +  1] > r[idx] ? r[idx +  1] : r[idx];//r[idx +  1] * (r[idx +  1] > r[idx]) + r[idx] * (r[idx] <= r[idx +  1]);
    }
    return r[0];
}
template <unsigned int blockSize>
__global__ void maxMonoBlock(float* g_idata, float* g_odata, unsigned int n) {
    int idx = threadIdx.x;
    float max = 0;
    for (int i = idx; i < n; i += blockSize) {
        float s = g_idata[i];
        max = s > max ? s : max;// s>max ? s:max;//s * (s > max) + max * (max <= s);
    }
    __shared__ float r[blockSize];
    r[idx] = max;
    warpReduceMax(&r[idx & ~(warpSize - 1)]);
    __syncthreads();
    if (idx < warpSize) { //first warp only
        r[idx] = idx * warpSize < blockSize ? r[idx * warpSize] : 0;
        warpReduceMax(r);
        if (idx == 0)
            *g_odata = r[0];
    }
}
template <unsigned int blockSize>
__global__ void maxMultiBlock(float* g_idata, float* g_odata, unsigned int n) {
    int thIdx = threadIdx.x;
    int gthIdx = thIdx + blockIdx.x * blockSize;
    const int gridSize = blockSize * gridDim.x;
    float max = 0;
    for (int i = gthIdx; i < n; i += gridSize) {
        float s = (g_idata[i] * g_idata[i]);
        max = s > max ? s : max;//s > max ? s : max; //s * (s > max) + max * (max <= s);
    }
    __shared__ float shArr[blockSize];
    shArr[thIdx] = max;
    __syncthreads();
    for (int size = blockSize / 2; size > 0; size /= 2) { //uniform
        if (thIdx < size)
            shArr[thIdx] = shArr[thIdx + size] > shArr[thIdx] ? shArr[thIdx + size] : shArr[thIdx]; //shArr[thIdx + size] > shArr[thIdx] ? shArr[thIdx + size] : shArr[thIdx];//shArr[thIdx + size] * (shArr[thIdx + size] > shArr[thIdx]) + shArr[thIdx] * (shArr[thIdx] <= shArr[thIdx + size]); 
        __syncthreads();
    }
    if (thIdx == 0)
        g_odata[blockIdx.x] = shArr[0];
}

template <unsigned int blockSize>
__global__ void maxMultiBlock(float* g_idata, float* g_idata2, float* g_odata, unsigned int n) {
    int thIdx = threadIdx.x;
    int gthIdx = thIdx + blockIdx.x * blockSize;
    const int gridSize = blockSize * gridDim.x;
    float max = 0;
    for (int i = gthIdx; i < n; i += gridSize) {
        float s = (g_idata[i] - g_idata2[i]);
        s = s*s;
        max = s > max ? s : max;//s > max ? s : max; //s * (s > max) + max * (max <= s);
    }
    __shared__ float shArr[blockSize];
    shArr[thIdx] = max;
    __syncthreads();
    for (int size = blockSize / 2; size > 0; size /= 2) { //uniform
        if (thIdx < size) {
            shArr[thIdx] = shArr[thIdx + size] > shArr[thIdx] ? shArr[thIdx + size] : shArr[thIdx]; //shArr[thIdx + size] > shArr[thIdx] ? shArr[thIdx + size] : shArr[thIdx];//shArr[thIdx + size] * (shArr[thIdx + size] > shArr[thIdx]) + shArr[thIdx] * (shArr[thIdx] <= shArr[thIdx + size]); 
        }
            
        __syncthreads();
    }
    if (thIdx == 0) {
        g_odata[blockIdx.x] = shArr[0];
    }
}
