#include "../head/ADMMGPU10.cuh"

#define NSTEPLOCAL 5
#define NMAXPEERPERTRHREAD 5


ADMMGPU10::ADMMGPU10() : Method()
{
#if DEBUG_CONSTRUCTOR
	std::cout << "Constructeur ADMMGPU10" << std::endl;
#endif // DEBUG_CONSTRUCTOR
	_name = NAME;
}


ADMMGPU10::ADMMGPU10(float rho) : Method()
{
#if DEBUG_CONSTRUCTOR
	std::cout << "Constructeur ADMMGPU10 defaut" << std::endl;
#endif // DEBUG_CONSTRUCTOR
	_name = NAME;
	_rho = rho;
}

ADMMGPU10::~ADMMGPU10()
{
}

void ADMMGPU10::setParam(float rho)
{
	_rho = rho;
}

void ADMMGPU10::setTau(float tau)
{
	if (tau < 1) {
		throw std::invalid_argument("tau must be greater than 1");
	}
	_tau = tau;
}

void ADMMGPU10::init(const Simparam& sim, const StudyCase& cas)
{
	// intitilisation des matrixs et variables 
	clock_t t = clock();
	_rhog = sim.getRho();
	//std::cout << "rho initial " << _rhog << std::endl;
	int nAgent = sim.getNAgent();
	_n = nAgent;
	float rho_p = _rho * _n;
	if (_rho == 0) {
		rho_p = _rhog;
	}
	MatrixCPU GAMMA(cas.getG());
	MatrixGPU connect(cas.getC());
	MatrixGPU Ub(cas.getUb());
	MatrixGPU Lb(cas.getLb());
	MatrixCPU LAMBDA(sim.getLambda());
	MatrixCPU trade(sim.getTrade());

	a = cas.geta();
	b = cas.getb();

	Pmin = MatrixGPU(cas.getPmin());
	Pmax = MatrixGPU(cas.getPmax());
	nVoisin = MatrixGPU(cas.getNvoi());

	int nVoisinMax = nVoisin.max2();
	if (_blockSize * NMAXPEERPERTRHREAD < nVoisinMax) {
		std::cout << _blockSize << " " << NMAXPEERPERTRHREAD << " " << nVoisinMax << std::endl;
		throw std::invalid_argument("For this Method, an agent must not have more than 5120 peers");
	}



	// mise sous forme lin�aire
	int nTradeTot = nVoisin.sum();
	_N = nTradeTot;
	_numBlocks1 = ceil((_n + _blockSize - 1) / _blockSize);
	_numBlocks2 = ceil((_N + _blockSize - 1) / _blockSize);
	CoresMatLin = MatrixGPU(nAgent, nAgent, -1);
	CoresLinAgent = MatrixGPU(nTradeTot, 1);
	CoresAgentLin = MatrixGPU(nAgent + 1, 1);
	CoresLinVoisin = MatrixGPU(nTradeTot, 1);
	CoresLinTrans = MatrixGPU(nTradeTot, 1);

	MU = sim.getMU(); // facteur reduit i.e lambda_l/_rho

	Tlocal_pre = MatrixGPU(nTradeTot, 1);
	tradeLin = MatrixGPU(nTradeTot, 1);
	Tmoy = sim.getPn();
	LAMBDALin = MatrixGPU(nTradeTot, 1);

	_at1 = _rhog; // represente en fait 2*a
	_at2 = rho_p;

	Ap2 = MatrixGPU(a);
	Ap1 = MatrixGPU(nVoisin);
	Ap12 = MatrixGPU(nAgent, 1);
	// Bt2 et Bp1 variable intermediaire, pas besoin de les stocker

	Bt1 = MatrixGPU(nTradeTot, 1);
	Ct = MatrixGPU(nTradeTot, 1);
	Cp = MatrixGPU(b);
	matUb = MatrixGPU(nTradeTot, 1);
	matLb = MatrixGPU(nTradeTot, 1);


	int indice = 0;

	for (int idAgent = 0;idAgent < nAgent; idAgent++) {
		MatrixCPU omega(cas.getVoisin(idAgent));
		int Nvoisinmax = nVoisin.get(idAgent, 0);
		for (int voisin = 0; voisin < Nvoisinmax; voisin++) {
			int idVoisin = omega.get(voisin, 0);
			matLb.set(indice, 0, Lb.get(idAgent, 0));
			matUb.set(indice, 0, Ub.get(idAgent, 0));
			Ct.set(indice, 0, GAMMA.get(idAgent, idVoisin));
			tradeLin.set(indice, 0, trade.get(idAgent, idVoisin));
			Tlocal_pre.set(indice, 0, trade.get(idAgent, idVoisin));
			LAMBDALin.set(indice, 0, LAMBDA.get(idAgent, idVoisin));
			CoresLinAgent.set(indice, 0, idAgent);
			CoresLinVoisin.set(indice, 0, idVoisin);
			CoresMatLin.set(idAgent, idVoisin, indice);
			indice = indice + 1;
		}
		CoresAgentLin.set(idAgent + 1, 0, indice);
	}
	for (int lin = 0;lin < nTradeTot;lin++) {
		int i = CoresLinAgent.get(lin,0);
		int j = CoresLinVoisin.get(lin, 0);
		int k = CoresMatLin.get(j,i); 
		CoresLinTrans.set(lin, 0, k);
	}

	tempNN = MatrixGPU(_N, 1, 0, 1);
	tempN1 = MatrixGPU(_n, 1, 0, 1); // plut�t que de re-allouer de la m�moire � chaque utilisation

	Tlocal = MatrixGPU(_N, 1, 0, 1);
	P = MatrixGPU(_n, 1, 0, 1); // moyenne des trades
	Pn = MatrixGPU(_n, 1, 0, 1); // somme des trades

	tempNN.preallocateReduction();
	Tlocal.preallocateReduction();
	P.preallocateReduction();

	matUb.transferGPU();
	matLb.transferGPU();
	Pmin.transferGPU();
	Pmax.transferGPU();
	nVoisin.transferGPU();
	a.transferGPU();
	b.transferGPU();
	MU.transferGPU();
	Tlocal_pre.transferGPU();
	tradeLin.transferGPU();
	LAMBDALin.transferGPU();
	Tmoy.transferGPU();

	Ap2.transferGPU();
	Ap1.transferGPU();
	Ap12.transferGPU();
	Bt1.transferGPU();
	Ct.transferGPU();
	Cp.transferGPU();
	CoresAgentLin.transferGPU();
	CoresLinAgent.transferGPU();
	CoresLinVoisin.transferGPU();
	CoresMatLin.transferGPU();
	CoresLinTrans.transferGPU();

	Pmin.divideT(&nVoisin);
	Pmax.divideT(&nVoisin);
	Ap2.multiplyT(&nVoisin);
	Ap2.multiplyT(&nVoisin);
	Ap1.multiply(rho_p);
	Ap12.add(&Ap1, &Ap2);
	Cp.multiplyT(&nVoisin);
	Tmoy.divideT(&nVoisin);
	updateGlobalProbGPU();
}



void ADMMGPU10::updateP0(const StudyCase& cas)
{
	_id = _id + 1;
#ifdef INSTRUMENTATION
	cudaDeviceSynchronize();
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
#endif // INSTRUMENTATION

	matLb.transferCPU();
	Pmin.transferCPU();
	Pmax.transferCPU();
	Cp.transferCPU();
	nVoisin.transferCPU();
	b.transferCPU();

	Pmin = MatrixGPU(cas.getPmin());
	Pmax = MatrixGPU(cas.getPmax());


	MatrixGPU Lb(cas.getLb());

	b = cas.getb();
	Cp = cas.getb();
	int indice = 0;

	for (int idAgent = 0;idAgent < _n; idAgent++) {
		int Nvoisinmax = nVoisin.get(idAgent, 0);
		for (int voisin = 0; voisin < Nvoisinmax; voisin++) {
			matLb.set(indice, 0, Lb.get(idAgent, 0));
			indice = indice + 1;
		}
	}

	nVoisin.transferGPU();
	matLb.transferGPU();
	Pmin.transferGPU();
	Pmax.transferGPU();
	Cp.transferGPU();
	b.transferGPU();

	Pmin.divideT(&nVoisin);
	Pmax.divideT(&nVoisin);
	Cp.multiplyT(&nVoisin);
#ifdef INSTRUMENTATION
	cudaDeviceSynchronize();
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	timePerBlock.increment(0, 8, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
	occurencePerBlock.increment(0, 8, 1);
#endif // INSTRUMENTATION
}


void ADMMGPU10::solve(Simparam* result, const Simparam& sim, const StudyCase& cas)
{
#ifdef DEBUG_SOLVE
	cas.display();
	sim.display(1);
#endif // DEBUG_SOLVE
	
	
	clock_t tall = clock();
#ifdef INSTRUMENTATION
	std::chrono::high_resolution_clock::time_point t1;
	std::chrono::high_resolution_clock::time_point t2;
#endif // INSTRUMENTATION
	// FB 0
	if (_id == 0) {
#ifdef INSTRUMENTATION
		cudaDeviceSynchronize();
		t1 = std::chrono::high_resolution_clock::now();
#endif // INSTRUMENTATION
		init(sim, cas);
#ifdef INSTRUMENTATION
		t2 = std::chrono::high_resolution_clock::now();
		timePerBlock.set(0, 0, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
		occurencePerBlock.set(0, 0, 1);
#endif // INSTRUMENTATION
	}
	_rhog = sim.getRho();
	_at1 = _rhog; // represente en fait 2*a
	
	float epsG = sim.getEpsG();
	float epsL = sim.getEpsL();
	const int stepL = sim.getStepL();
	const int stepG = sim.getStepG();
	const int iterG = sim.getIterG();
	const int iterL = sim.getIterL();
	

	float resG = 2 * epsG;
	float resL = 2 * epsL;
	int iterGlobal = 0;
	int iterLocal = 0;

	MatrixCPU nVoisinCPU(cas.getNvoi());
	MatrixCPU LAMBDA(sim.getLambda());
	MatrixCPU trade(sim.getTrade());
	MatrixCPU resF(2, (iterG / stepG) + 1);

	while ((iterGlobal < iterG) && (resG > epsG)) {
		resL = 2 * epsL;
		iterLocal = 0;
		while (iterLocal< iterL && resL>epsL) {
#ifdef INSTRUMENTATION
			cudaDeviceSynchronize();
			t1 = std::chrono::high_resolution_clock::now();
#endif // INSTRUMENTATION
			updateLocalProbGPU(&Tlocal, &P);
#ifdef INSTRUMENTATION
			cudaDeviceSynchronize();
			t2 = std::chrono::high_resolution_clock::now();
			timePerBlock.increment(0, 1, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
#endif // INSTRUMENTATION
			if (!(iterLocal % stepL)) {
#ifdef INSTRUMENTATION
				cudaDeviceSynchronize();
				t1 = std::chrono::high_resolution_clock::now();
#endif // INSTRUMENTATION
				resL = calcRes(&Tlocal, &P); // blocant il faut que tout soit calcul�
#ifdef INSTRUMENTATION
				cudaDeviceSynchronize();
				t2 = std::chrono::high_resolution_clock::now();
				timePerBlock.increment(0, 4, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
#endif // INSTRUMENTATION
			}
			//std::cout << iterGlobal << " " << iterLocal << " " << resL << " " << resG << std::endl;
			Tlocal.swap(&Tlocal_pre);
			iterLocal++;
		}
#ifdef INSTRUMENTATION
		occurencePerBlock.increment(0, 1, iterLocal);
		occurencePerBlock.increment(0, 4, iterLocal / stepL);
#endif // INSTRUMENTATION
		Tlocal.swap(&Tlocal_pre);
		tradeLin.swap(&Tlocal);
		// FB 3
#ifdef INSTRUMENTATION
		cudaDeviceSynchronize();
		t1 = std::chrono::high_resolution_clock::now();
#endif // INSTRUMENTATION
		updateGlobalProbGPU();
#ifdef INSTRUMENTATION
		cudaDeviceSynchronize();
		t2 = std::chrono::high_resolution_clock::now();
		timePerBlock.increment(0, 5, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
#endif // INSTRUMENTATION
		if (!(iterGlobal % stepG)) {
#ifdef INSTRUMENTATION
			cudaDeviceSynchronize();
			t1 = std::chrono::high_resolution_clock::now();
#endif // INSTRUMENTATION
			resG = updateRes(&resF, &Tlocal, iterGlobal / stepG, &tempNN);
#ifdef INSTRUMENTATION
			cudaDeviceSynchronize();
			t2 = std::chrono::high_resolution_clock::now();
			timePerBlock.increment(0, 6, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
#endif // INSTRUMENTATION
		}
		//std::cout << iterGlobal << " " << iterLocal << " " << resL << " " << resF.get(0, iterGlobal / stepG) << " " << resF.get(1, iterGlobal / stepG) << std::endl;
		iterGlobal++;
	}
#ifdef INSTRUMENTATION
	occurencePerBlock.increment(0, 5, iterGlobal);
	occurencePerBlock.increment(0, 6, iterGlobal / stepG);
	// FB 5
	cudaDeviceSynchronize();
	t1 = std::chrono::high_resolution_clock::now();
#endif // INSTRUMENTATION


	updatePnGPU << <_numBlocks1, _blockSize >> > (Pn._matrixGPU, Tmoy._matrixGPU, nVoisin._matrixGPU, _n);
	float fc = calcFc(&a, &b, &tradeLin, &Pn, &Ct, &tempN1, &tempNN);
	//std::cout << iterGlobal << " " << iterLocal << " " << resL << " " << resG << std::endl;
	MatrixCPU tradeLinCPU;
	tradeLin.toMatCPU(tradeLinCPU);
	MatrixCPU LAMBDALinCPU;
	LAMBDALin.toMatCPU(LAMBDALinCPU);
	MatrixCPU PnCPU;
	Pn.toMatCPU(PnCPU);
	MatrixCPU MUCPU;
	MU.toMatCPU(MUCPU);
	int indice = 0;
	for (int idAgent = 0;idAgent < _n; idAgent++) {
		MatrixCPU omega(cas.getVoisin(idAgent));
		int Nvoisinmax = nVoisinCPU.get(idAgent, 0);
		for (int voisin = 0; voisin < Nvoisinmax; voisin++) {
			int idVoisin = omega.get(voisin, 0);
			trade.set(idAgent, idVoisin, tradeLinCPU.get(indice, 0));
			LAMBDA.set(idAgent, idVoisin, LAMBDALinCPU.get(indice, 0));
			indice = indice + 1;
		}
	}

	result->setResF(&resF);
	result->setLAMBDA(&LAMBDA);
	result->setTrade(&trade);
	result->setIter(iterGlobal);
	result->setPn(&PnCPU);
	result->setFc(fc);
	result->setMU(&MUCPU);
	result->setRho(_rhog);
#ifdef INSTRUMENTATION
	cudaDeviceSynchronize();
	t2 = std::chrono::high_resolution_clock::now();
	timePerBlock.increment(0, 7, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
	occurencePerBlock.increment(0, 7, 1);

	result->setTimeBloc(&timePerBlock, &occurencePerBlock);
#endif // INSTRUMENTATION
	tall = clock() - tall;
	result->setTime((float)tall / CLOCKS_PER_SEC);
}

void ADMMGPU10::updateLocalProbGPU( MatrixGPU* Tlocal, MatrixGPU* P) {
	int numBlocks = _n;
	switch (_blockSize) {
	case 512:
		updateTradePGPUShared<512> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU);
		break;
	case 256:
		updateTradePGPUShared<256> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU);
		break;
	case 128:
		updateTradePGPUShared<128> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU);
		break;
	case 64:
		updateTradePGPUShared< 64> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU);
		break;
	case 32:
		updateTradePGPUShared< 32> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU);
		break;
	case 16:
		updateTradePGPUShared< 16> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU);
		break;
	case  8:
		updateTradePGPUShared<  8> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU);
		break;
	case  4:
		updateTradePGPUShared<  4> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU);
		break;
	case  2:
		updateTradePGPUShared<  2> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU);
		break;
	case  1:
		updateTradePGPUShared<  1> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU);
		break;
	}
}



void ADMMGPU10::updateGlobalProbGPU()
{
	updateLAMBDAGPULin2 << <_numBlocks2, _blockSize >> > (LAMBDALin._matrixGPU, tradeLin._matrixGPU, _rhog, CoresLinTrans._matrixGPU, _N);
	updateBt1GPULin2 << <_numBlocks2, _blockSize >> > (Bt1._matrixGPU, tradeLin._matrixGPU, _rhog, LAMBDALin._matrixGPU, CoresLinTrans._matrixGPU, _N);
}


float ADMMGPU10::updateRes(MatrixCPU* res, MatrixGPU* Tlocal, int iter, MatrixGPU* tempNN)
{
	float resS = Tlocal->max2(&tradeLin);

	updateDiffGPU2 <<<_numBlocks2, _blockSize >>> (tempNN->_matrixGPU, Tlocal->_matrixGPU, CoresLinTrans._matrixGPU, _N);
	float resR = tempNN->max2();

	res->set(0, iter, resR);
	res->set(1, iter, resS);

	if (resR > _mu * resS) {
		_rhog = _tau * _rhog;
		_at1 = _rhog;
		//std::cout << "rho augmente :" << _rhog << std::endl;
	}
	else if (resS > _mu * resR) {// rho = rho / tau_inc;
		_rhog = _rhog / _tau;
		_at1 = _rhog;
		//std::cout << "rho diminue :" << _rhog << std::endl;
	}
	
	return resR * (resR > resS) + resS * (resR <= resS);
}


float ADMMGPU10::calcRes(MatrixGPU* Tlocal, MatrixGPU* P)
{
	float d1 = Tlocal->max2(&Tlocal_pre);
	float d2 = P->max2(&Tmoy);

	return d1* (d1 > d2) + d2 * (d2 >= d1);
}


void ADMMGPU10::display() {

	std::cout << _name << std::endl;
}


