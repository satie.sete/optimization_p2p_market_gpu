
#include "../head/main.cuh"


// mingw32 - make.exe

// Simulation

int main(int argc, char* argv[]){
#ifdef DEBUG_TEST
	std::cout << "test Matrix err =" << testMatrix() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test Agent err =" << testAgent() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test StudyCase err =" << testStudyCase() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test Simparam err =" << testSimparam() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test ADMM err =" << testADMM() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test ADMMGPU err =" << testADMMGPU() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test ADMMGPU5 err =" << testADMMGPU5() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test ADMMGPU6 err =" << testADMMGPU6() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test ADMMGPU9 err =" << testADMMGPU9() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test ADMMGPU10 err =" << testADMMGPU10() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test systeme err =" << testSysteme() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test systeme err =" << result << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test Matrix err =" << testMatrix() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test MatrixGPU err =" << testMGPU() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test OSQP err =" << testOSQP() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	testADMMGPUtemp();
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
#endif // DEBUG_TEST
	srand(time(nullptr));
	std::cout.precision(12);
	//std::cout << "test Matrix err =" << testMatrix() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	//std::cout << "-------------------------------------------------------- " << std::endl;
	
	
	
	SimuTemporal();
	//testADMMGPUtemp();
	


	return 0;
}





void SimulationTempStepOpti()
{
	// date 0->4 1 janvier 2012
	// date 0->4 1 juin 2013
	// date 10-15 20 octobre 2014

	std::string path = "data/";
	std::string fileName = "SimutemporalStep8.csv";
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;

	std::string methode = "ADMMGPU8";

	float epsG = 0.001f;
	float epsL = 0.0005f;



	MatrixCPU interval(4, 2);
	interval.set(0, 0, 2013);
	interval.set(0, 1, 2013);
	interval.set(1, 0, 6);
	interval.set(1, 1, 6);
	interval.set(2, 0, 10);
	interval.set(2, 1, 10);
	interval.set(3, 0, 0);
	interval.set(3, 1, 4);

	int iterG = 80000;
	int iterL = 1500;

	//int nCons = 25;
	//int nGen = 25;
	int nGen = 969;
	int nCons = 1494;
	int nAgent = nGen + nCons;



	float rhoAgent = 0.05f;
	float rho = rhoAgent * nAgent; //0.056 * nAgent;
	float rhoL = rho;


	const int nStep = 8;
	int Steps[nStep] = { 1, 2, 5, 10, 25, 50, 100, 200 };
	MatrixCPU StepLocals(1, nStep);
	MatrixCPU StepGlobal(1, nStep);
	for (int i = 0; i < nStep; i++) {
		StepLocals.set(0, i, Steps[i]);
		StepGlobal.set(0, i, Steps[i]);
	}

	System sys;
	sys.setIter(iterG, iterL);
	sys.setEpsG(epsG);
	sys.setEpsL(epsL);
	
	sys.setMethod(methode);
	sys.setRho(rho);
	//sys.setRhoL(rhoL);
	


	MatrixCPU Param(1, 15);
	Param.set(0, 0, nAgent);
	Param.set(0, 1, epsG);
	Param.set(0, 2, epsL);
	Param.set(0, 3, iterG);
	Param.set(0, 4, iterL);
	Param.set(0, 5, rho);
	Param.set(0, 6, rhoL);
	Param.set(0, 7, interval.get(0, 0));
	Param.set(0, 8, interval.get(1, 0));
	Param.set(0, 9, interval.get(2, 0));
	Param.set(0, 10, interval.get(3, 0));
	Param.set(0, 11, interval.get(0, 1));
	Param.set(0, 12, interval.get(1, 1));
	Param.set(0, 13, interval.get(2, 1));
	Param.set(0, 14, interval.get(3, 1));
	Param.saveCSV(fileName, mode);

	StepLocals.saveCSV(fileName, mode);
	StepGlobal.saveCSV(fileName, mode);


	for (int local = 0; local < nStep; local++) {
		for (int global = 0; global < nStep; global++) {
			sys.setStep(StepGlobal.get(0, global), StepLocals.get(0, local));


			clock_t t = clock();
			sys.solveIntervalle(path, &interval, nCons, nGen);
			t = clock() - t;

			std::cout << "temps simulation : " << t / CLOCKS_PER_SEC << std::endl;


			MatrixCPU temps(sys.getTemps());
			MatrixCPU iter(sys.getIter());
			MatrixCPU conv(sys.getConv());
			MatrixCPU fc(sys.getFc());
			//MatrixCPU ResR(sys.getResR());
			//MatrixCPU ResS(sys.getResS());

			temps.display();
			iter.display();

			temps.saveCSV(fileName, mode);
			iter.saveCSV(fileName, mode);
			fc.saveCSV(fileName, mode);
			//ResR.saveCSV(fileName, mode);
			//ResS.saveCSV(fileName, mode);
			conv.saveCSV(fileName, mode);

			std::cout << "-------------------------------------------------------- " << std::endl;
			sys.resetParam();
		}
	}



}



void SimuTemporal()
{
	std::string path = "data/";
	std::string fileName = "SimutemporalCISTEMFINAL.csv";
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	const int nMethode = 1;
	//std::string methodes[nMethode] = { "OSQP", "ADMM","ADMMGPU", "ADMMGPU2","ADMMGPU3","ADMMGPU4", "ADMMGPU5", "ADMMGPU6","ADMMGPU7"};
	//std::string methodes[nMethode] = { "ADMM","ADMMGPU","ADMMGPU5"};
	//std::string methodes[nMethode] = { "ADMMGPU5","ADMMGPU8","ADMMGPU9","ADMMGPU10", "ADMMGPU11" };
	
	std::string methodes[nMethode] = { "ADMMGPU12" };
	float epsG = 0.01f;
	float epsL = 0.005f;



	MatrixCPU interval(4, 2);
	interval.set(0, 0, 2013);
	interval.set(0, 1, 2013);
	interval.set(1, 0, 6);
	interval.set(1, 1, 6);
	interval.set(2, 0, 1);
	interval.set(2, 1, 10);
	interval.set(3, 0, 0);
	interval.set(3, 1, 23);

	int iterG = 2000;
	int iterL = 700;

	//int nCons = 25;
	//int nGen = 25;
	int nGen = 969;
	int nCons = 1494;
	int nAgent = nGen + nCons;
	float rhoAgent = 0.05f;
	float rho = rhoAgent * nAgent; //0.056 * nAgent;
	//float rhoMax = 0.005 * nAgent;
	//float rho = 0.005 * nAgent;
	
	float stepG = 2;
	float stepL = 2;
	
	System sys; 
	StudyCase cas;
	sys.setIter(iterG, iterL);
	sys.setStep(stepG, stepL);
	sys.setEpsG(epsG);
	sys.setEpsL(epsL);


	MatrixCPU Param(1, 16);
	Param.set(0, 0, nAgent);
	Param.set(0, 1, rhoAgent);
	Param.set(0, 2, epsG);
	Param.set(0, 3, epsL);
	Param.set(0, 4, iterG);
	Param.set(0, 5, iterL);
	Param.set(0, 6, stepG);
	Param.set(0, 7, stepL);
	Param.set(0, 8, interval.get(0, 0));
	Param.set(0, 9, interval.get(1, 0));
	Param.set(0, 10, interval.get(2, 0));
	Param.set(0, 11, interval.get(3, 0));
	Param.set(0, 12, interval.get(0, 1));
	Param.set(0, 13, interval.get(1, 1));
	Param.set(0, 14, interval.get(2, 1));
	Param.set(0, 15, interval.get(3, 1));
	
	
	Param.saveCSV(fileName, mode);
	
	for (int i = 0; i < nMethode;i++) {
		std::string methode = methodes[i];
		sys.setMethod(methode);
		sys.setRho(rho);
		std::cout << "----------------Simu---------------------------- " << std::endl;
		// Debut simu
		
		clock_t t = clock();
		
		sys.solveIntervalle(path, &interval, nCons, nGen);
		t = clock() - t;
		
		std::cout << "temps simulation : " << t / CLOCKS_PER_SEC << std::endl;
		
		MatrixCPU temps(sys.getTemps());
		MatrixCPU iter(sys.getIter());
		MatrixCPU conv(sys.getConv());
		MatrixCPU fc(sys.getFc());
		MatrixCPU ResR(sys.getResR());
		MatrixCPU ResS(sys.getResS());

		//temps.display();
		/*temps.saveCSV(fileName, mode);
		iter.saveCSV(fileName, mode);
		fc.saveCSV(fileName, mode);
		ResR.saveCSV(fileName, mode);
		ResS.saveCSV(fileName, mode);
		conv.saveCSV(fileName, mode);*/

		std::cout << "-------------------------------------------------------- " << std::endl;
		sys.displayTime();
		//sys.resetParam();
	}

}



void testADMMGPUtemp()
{
	System sys;
	float epsG = 0.0001f;
	float epsL = 0.00001f;
	int iterL = 2000;
	int iterG = 400;
	sys.setEpsG(epsG);
	sys.setEpsL(epsL);
	sys.setStep(1, 1);
	sys.setIter(iterG, iterL);
	const int nMethode = 13;
	std::string methodes[nMethode] = {"ADMM","ADMM1","ADMMGPU", "ADMMGPU2","ADMMGPU3","ADMMGPU4", "ADMMGPU5", "ADMMGPU6", "ADMMGPU8", "ADMMGPU9", "ADMMGPU10", "ADMMGPU11", "ADMMGPU12" };
	

	float fc = -69677.4;
	float Pn[29] = { -81.5435 ,-435.3918, -265.1791, -65.3475, -78.8221, -9.8000,  -12.8000, -394.0950, -289.2683, -201.2195,  -48.7805, -319.7409,-25.6740, -261.7490, -282.0785, -166.1365,  -40.2723, -212.2855,  -95.6848,  432.9953, 355.2106, 384.5925, 449.2794,  243.9024,  339.7212,  266.7089,  363.4624,  223.4674, 226.5286 };					//-81.56948  -435.429   -265.2162  -65.38065  -78.8434   -9.390951  -12.47829 -394.1254  -289.3031  -201.2529 -48.80591  -319.768   -25.71047  -261.7702  -282.1037  -166.1616 -40.2928   -212.3176  -95.70714 432.9755  355.1785   384.571   449.2593  243.8796  339.7002  266.6864  363.4278   223.4439  226.5048
					/*-81.56938  -435.4289  -265.2162  -65.38068  -78.84332  -9.390917  -12.4783  -394.1253  -289.303   -201.2528 -48.80609  -319.7679  -25.71185  -261.7701  -282.1036  -166.1615 -40.29279  -212.3175  -95.70706 432.9755  355.1785   384.571   449.2593  243.8797  339.7002  266.6865  363.4278   223.444   226.5049*/
	MatrixCPU P(29, 1);
	for (int i = 0;i < 29;i++) {
		P.set(i, 0, Pn[i]);
	}

	MatrixCPU result(nMethode, 1, -1);
	MatrixCPU temps(nMethode, 1, -1);

	for (int i = 0; i < nMethode; i++) {
		std::string methode = methodes[i];
		sys.setMethod(methode);
		Simparam res = sys.solve();
		res.display();
		MatrixCPU P2 = res.getPn();
		P2.display();
		int test = P2.isEqual(&P, 0.1) + 2 * (fabs(fc - res.getFc())<= 0.1);
		result.set(i, 0, test);
		temps.set(i, 0, res.getTime());
		std::cout << "-------------------------------------------------------- " << std::endl;
	}

	result.display();
	temps.display();
}

std::string generateDate(int year, int month, int day, int hour)
{
	std::string smonth;
	std::string sday;
	std::string shour;
	if (month < 10) {
		smonth = "0" + std::to_string(month);
	}
	else {
		smonth = std::to_string(month);
	}
	if (day < 10) {
		sday = "0" + std::to_string(day);
	}
	else {
		sday = std::to_string(day);
	}
	if (hour < 10) {
		shour = "0" + std::to_string(hour);
	}
	else {
		shour = std::to_string(hour);
	}
		
		

	std::string d = std::to_string(year) + "-" + smonth + "-" + sday + " " + shour +"-00-00";
	
	return d;
}



