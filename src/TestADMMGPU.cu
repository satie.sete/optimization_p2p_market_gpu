
#include "../head/TestADMMGPU.cuh"

int testADMMGPU()
{
	int n = 1;
	if (!testADMMGPUContruct1()) return n;
	n++;
	if (!testADMMGPUContruct2()) return n;
	n++;
	if (!testADMMGPUContruct3()) return n;
	n++;
	if (!testADMMGPURes()) return n;
	n++;
	if (!testADMMGPUFc()) return n;
	n++;
	if (!testADMMGPUBt1()) return n;
	n++;
	if (!testADMMGPUTrade()) return n;
	n++;
	if (!testADMMGPULAMBDA()) return n;
	n++;
	if (!testADMMGPULAMBDA2()) return n;
	n++; // 10
	if (!testADMMGPUSolve1()) return n;
	n++;
	if (!testADMMGPUSolve2()) return n;
	n++;


	return 0;
}

bool testADMMGPUContruct1()
{
	std::cout << "default constructor" << std::endl;
	ADMMGPU a;
	return true;
}

bool testADMMGPUContruct2()
{
	float rho = 2;

	std::cout << "param constructor" << std::endl;
	ADMMGPU a(rho);
	return true;
}
bool testADMMGPUContruct3()
{
	float rho = 2;

	std::cout << "2 times constructor" << std::endl;
	ADMMGPU a;
	a = ADMMGPU(rho);
	return true;
}

bool testADMMGPUSolve1()
{
	//solve(Simparam* result, Simparam sim, StudyCase cas);
	StudyCase cas;
	cas.Set2node();
	
	int nAgent = cas.getNagent();
	Simparam param(nAgent);
	param.setRho(1);
	Simparam res(param);

	ADMMGPU a(0.5);


	a.solve(&res, param, cas);
	res.display();
	MatrixCPU Trade(nAgent, nAgent);
	Trade.set(0, 1, -1);
	Trade.set(1, 0, 1);
	MatrixCPU Res(res.getRes());
	Res.display();
	MatrixCPU trade = res.getTrade();
	trade.display();
	return trade.isEqual(&Trade, 0.001f);

}
bool testADMMGPUSolve2()
{
	//solve(Simparam* result, Simparam sim, StudyCase cas);
	StudyCase cas;
	cas.Set29node();
	int nAgent = cas.getNagent();

	Simparam param(nAgent);
	float epsG = 0.00001f;
	param.setEpsG(epsG);
	
	Simparam res(param);
	ADMMGPU a;
	a.solve(&res, param, cas);
	res.display();


	float Pn[29] = { -81.5435 ,-435.3918, -265.1791, -65.3475, -78.8221, -9.8000,  -12.8000, -394.0950, -289.2683, -201.2195,  -48.7805, -319.7409,-25.6740, -261.7490, -282.0785, -166.1365,  -40.2723, -212.2855,  -95.6848,  432.9953, 355.2106, 384.5925, 449.2794,  243.9024,  339.7212,  266.7089,  363.4624,  223.4674, 226.5286 };
					
	MatrixCPU P(29, 1);
	for (int i = 0;i < 29;i++) {
		P.set(i, 0, Pn[i]);
	}
	MatrixCPU P2 = res.getPn();

	return P2.isEqual(&P, 0.1);

}




bool testADMMGPURes()
{
	/* float ADMMGPU::calcRes( MatrixGPU* Tlocal, MatrixGPU* Tlocal_pre, MatrixGPU* Tmoy, MatrixGPU* P, MatrixGPU* tempN1, MatrixGPU* tempNN)
{
	 tempNN->subtract(Tlocal, Tlocal_pre);
	 tempN1->subtract(Tmoy, P);
	 float d1 = (tempN1->toMatCPU()).max2();
	 float d2 = (tempNN->toMatCPU()).max2();


	 return d1* (d1 > d2) + d2 * (d2 >= d1);
}*/
	int nAgent = 4;
	float value1 = 2;
	float value2 = 4;
	float value3 = -2;
	float value4 = 3;
	float d = 0;
	MatrixGPU Tlocal(nAgent, nAgent, value1);
	MatrixGPU Tlocal_pre(nAgent, nAgent, value2);
	MatrixGPU Tmoy(nAgent, 1, value3);
	MatrixGPU P(nAgent, 1, value4);
	MatrixGPU tempNN(nAgent, nAgent, value2);
	MatrixGPU tempN1(nAgent, 1, value3);


	Tlocal_pre.transferGPU();
	Tlocal.transferGPU();
	Tmoy.transferGPU();
	P.transferGPU();
	tempNN.transferGPU();
	tempN1.transferGPU();

	ADMMGPU a;
	d = a.calcRes(&Tlocal, &Tlocal_pre, &Tmoy, &P, &tempN1, &tempNN);



	float d1 = fabs(value1 - value2);
	float d2 = fabs(value3 - value4);


	return (d == (d1 * (d1 > d2) + d2 * (d2 >= d1)));
}

bool testADMMGPUFc()
{
	/*
	float Method::calcFc(MatrixGPU* cost1, MatrixGPU* cost2, MatrixGPU* trade, MatrixGPU* Pn, MatrixGPU* GAMMA, MatrixGPU* tempN1, MatrixGPU* tempNN)
{
	float fc = 0;
	tempN1->set(cost1);
	tempN1->multiply(0.5);
	tempN1->multiplyT(Pn);
	tempN1->add(cost2);
	tempN1->multiplyT(Pn);
	////cudaDeviceSynchronize();
	fc = fc + tempN1->sum();
	tempNN->set(trade);
	tempNN->multiplyT(GAMMA);
	////cudaDeviceSynchronize();
	fc = fc + tempNN->sum();
	return fc;
}
	*/
	int nAgent = 3;
	float value1 = 1.5;
	float value2 = 2;
	float value3 = 3;
	float value4 = 8;
	float value5 = 10;
	float value6 = nAgent * nAgent * value4 * value5 + nAgent * (value1 * 0.5f * value3 + value2) * value3;


	MatrixGPU cost1(nAgent, 1, value1);
	MatrixGPU cost2(nAgent, 1, value2);
	MatrixGPU Pn(nAgent, 1, value3);
	MatrixGPU tempN1(nAgent, 1);
	MatrixGPU ttempN1(nAgent, 1);
	MatrixGPU trade(nAgent, nAgent, value4);
	MatrixGPU GAMMA(nAgent, nAgent, value5);
	MatrixGPU tempNN(nAgent, nAgent);
	MatrixGPU ttempNN(nAgent, nAgent);
	cost1.transferGPU();
	cost2.transferGPU();
	Pn.transferGPU();
	tempN1.transferGPU();
	ttempN1.transferGPU();
	GAMMA.transferGPU();
	tempNN.transferGPU();
	ttempNN.transferGPU();
	trade.transferGPU();


	ADMMGPU a;
	float fc = a.calcFc(&cost1, &cost2, &trade, &Pn, &GAMMA, &tempN1, &tempNN);
	float fc2 = 0;

	ttempN1.set(&cost1);
	ttempN1.multiply(0.5);
	ttempN1.multiplyT(&Pn);
	ttempN1.add(&cost2);
	ttempN1.multiplyT(&Pn);
	
	fc2 = fc2 + ttempN1.sum();
	ttempNN.set(&trade);
	ttempNN.multiplyT(&GAMMA);
	fc2 = fc2 + ttempNN.sum();
	


	
	return ((fc == value6) && (fc2 == fc));
}

bool testADMMGPUBt1()
{
	int nAgent = 10;
	int _blockSize = 256;
	int numBlocks = ceil((nAgent + _blockSize - 1) / _blockSize);
	float value1 = 2;
	float value2 = 4;
	float value3 = -2;
	float rho = 1.5;
	MatrixGPU Bt1(nAgent, nAgent, value1);
	MatrixGPU Bt11(nAgent, nAgent, value1);
	MatrixGPU trade(nAgent, nAgent, value2);
	MatrixGPU LAMBDA(nAgent, nAgent, value3);

	Bt1.transferGPU();
	Bt11.transferGPU();
	trade.transferGPU();
	LAMBDA.transferGPU();

	ADMMGPU a;

	Bt1.set(&trade);
	Bt1.subtractTrans(&trade);
	Bt1.multiply(0.5 * rho);
	Bt1.subtract(&LAMBDA);
	Bt1.divide(rho);

	updateBt1GPU << <numBlocks, _blockSize >> > (Bt11._matrixGPU, trade._matrixGPU, rho, LAMBDA._matrixGPU, nAgent);

	Bt1.transferCPU();
	Bt11.transferCPU();


	return Bt1.isEqual(&Bt11);
}
bool testADMMGPULAMBDA()
{
	int nAgent = 10;
	float value1 = 2;
	float value2 = -8;
	float value3 = 1;
	MatrixGPU LAMBDA(nAgent, nAgent, value1);
	MatrixGPU LAMBDA2(nAgent, nAgent, value1);
	MatrixGPU trade(nAgent, nAgent, value2);
	float rho = value3;
	MatrixGPU tempNN(nAgent, nAgent);


	ADMMGPU a;
	a.updateLAMBDA(&LAMBDA, &trade, rho, &tempNN);

	tempNN.set(&trade);
	tempNN.addTrans(&trade);
	tempNN.multiply(rho);
	tempNN.multiply(0.5);
	LAMBDA2.add(&LAMBDA2, &tempNN);


	return LAMBDA.isEqual(&LAMBDA2);
}
bool testADMMGPULAMBDA2()
{
	int nAgent = 10;
	int blockSize = 256;
	int numBlocks = ceil((nAgent + blockSize - 1) / blockSize);
	float value1 = 2;
	float value2 = -8;
	float value3 = 1;
	MatrixGPU LAMBDA(nAgent, nAgent, value1);
	MatrixGPU LAMBDA2(nAgent, nAgent, value1);
	MatrixGPU trade(nAgent, nAgent, value2);
	float rho = value3;
	MatrixGPU tempNN(nAgent, nAgent);

	LAMBDA.transferGPU();
	LAMBDA2.transferGPU();
	trade.transferGPU();
	tempNN.transferGPU();



	ADMMGPU a;
	a.updateLAMBDA(&LAMBDA, &trade, rho, &tempNN);
	updateLAMBDAGPU << <numBlocks, blockSize >> > (LAMBDA2._matrixGPU, trade._matrixGPU, rho, nAgent);

	LAMBDA.transferCPU();
	LAMBDA2.transferCPU();

	return LAMBDA.isEqual(&LAMBDA2);

}
bool testADMMGPUTrade()
{
	int nAgent = 10;
	int blockSize = 256;
	int numBlocks = ceil((nAgent + blockSize - 1) / blockSize);
	float value1 = 0;
	float value2 = -8;
	float value3 = 1;
	float value4 = 1;
	float value5 = -1;
	float value6 = -30;
	float value7 = 0;
	float value8 = 5;
	MatrixGPU Bt1(nAgent, nAgent, value1);
	float at1 = value3;
	MatrixGPU Bt2(nAgent, nAgent, value2);
	float at2 = value4;
	MatrixGPU Ct(nAgent, nAgent, value5);
	MatrixGPU Lb(nAgent, nAgent, value6);
	MatrixGPU Ub(nAgent, nAgent, value7);
	MatrixGPU Tlocal(nAgent, nAgent);
	MatrixGPU Tlocal2(nAgent, nAgent);
	MatrixGPU Tmoy(nAgent, 1, value3);
	MatrixGPU P(nAgent, 1, value4);
	MatrixGPU MU(nAgent, 1, value5);
	MatrixGPU Tlocal_pre(nAgent, nAgent, value8);

	Bt2.transferGPU();
	Bt1.transferGPU();
	Tlocal.transferGPU();
	Tlocal2.transferGPU();
	Ct.transferGPU();
	Lb.transferGPU();
	Ub.transferGPU();
	Tlocal_pre.transferGPU();
	Tmoy.transferGPU();
	P.transferGPU();
	MU.transferGPU();


	ADMMGPU a;
	Bt2.set(&Tlocal);
	Bt2.subtractVector(&Tmoy);
	Bt2.addVector(&P);
	Bt2.subtractVector(&MU);
	float ada = at1 / at2;
	float apa = at1 + at2;

	Tlocal.set(&Bt1);
	Tlocal.multiply(ada);
	Tlocal.add(&Bt2);
	Tlocal.multiply(at2);

	Tlocal.subtract(&Ct);
	Tlocal.divide(apa);
	Tlocal.project(&Lb, &Ub);

	updateTrade << <numBlocks, blockSize >> > (Bt2._matrixGPU, Tlocal2._matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P._matrixGPU, MU._matrixGPU, at1, at2, Bt1._matrixGPU, Ct._matrixGPU, Lb._matrixGPU, Ub._matrixGPU, nAgent);



	Tlocal.transferCPU();
	Tlocal2.transferCPU();

	return Tlocal.isEqual(&Tlocal2);
}
bool testADMMGPUP()
{
	int nAgent = 10;
	int blockSize = 256;
	int numBlocks = ceil((nAgent + blockSize - 1) / blockSize);
	float value1 = 3;
	float value2 = -8;
	float value3 = 1;
	float value5 = -1;
	float value6 = -30;
	float value7 = 0;
	float value8 = 5;
	float value9 = 2;

	MatrixGPU Bp1(nAgent, 1);
	MatrixGPU Ap1(nAgent, 1, value2);
	MatrixGPU Ap2(nAgent, 1, value3);
	MatrixGPU Ap12(nAgent, 1);
	MatrixGPU Cp(nAgent, 1, value5);
	MatrixGPU Lb(nAgent, 1, value6);
	MatrixGPU Ub(nAgent, 1, value7);
	MatrixGPU Tlocal(nAgent, nAgent, value8);
	MatrixGPU nVoisin(nAgent, 1, value9);
	MatrixGPU Tmoy(nAgent, 1);
	MatrixGPU MU(nAgent, 1, value1);
	MatrixGPU MU2(nAgent, 1, value1);
	MatrixGPU P(nAgent, 1);
	MatrixGPU P2(nAgent, 1);
	Bp1.transferGPU();
	Ap1.transferGPU();
	Ap2.transferGPU();
	Ap12.transferGPU();
	Ap12.add(&Ap1, &Ap2);
	Cp.transferGPU();
	P.transferGPU();
	Lb.transferGPU();
	Ub.transferGPU();
	Tlocal.transferGPU();
	nVoisin.transferGPU();
	Tmoy.transferGPU();
	MU.transferGPU();
	MU2.transferGPU();
	P2.transferGPU();

	ADMMGPU a;

	Tmoy.Moy(&Tlocal, &nVoisin); 
	Bp1.add(&MU, &Tmoy);
	P.multiplyT(&Ap1, &Bp1);
	P.subtract(&Cp);

	P.divideT(&Ap12);
	P.project(&Lb, &Ub);
	MU.add(&Tmoy);
	MU.subtract(&P);


	updatePGPU << <numBlocks, blockSize >> > (Tlocal._matrixGPU, nVoisin._matrixGPU, MU2._matrixGPU, Tmoy._matrixGPU, P2._matrixGPU, Ap1._matrixGPU, Ap2._matrixGPU, Bp1._matrixGPU, Cp._matrixGPU, Lb._matrixGPU, Ub._matrixGPU, nAgent);
	P.transferCPU();
	P2.transferCPU();

	return P.isEqual(&P2);
}
