


#include "../head/Simparam.h"
Simparam::Simparam()
{
#if DEBUG_CONSTRUCTOR
	std::cout << "simparam constructor" << std::endl;
#endif // DEBUG_CONSTRUCTOR
	_rho = RHO;
	_iterMaxGlobal = ITERMAXGLOBAL;
	_iterMaxLocal = ITERMAXLOCAL;
	_epsGlobal = EPSGLOBAL;
	_epsLocal = EPSLOCAL;
	_stepL = STEPL;
	_stepG = STEPG;
	_nAgent = 0;
	_iter = 0;
	_time = 0;
	_fc = 0;
	_fcSym = 0;
	_iterLocalTotal = 0;

}

Simparam::Simparam(const Simparam& sim)
{
	_rho = sim._rho;
	_iterMaxGlobal = sim._iterMaxGlobal;
	_iterMaxLocal = sim._iterMaxLocal;
	_epsGlobal = sim._epsGlobal;
	_epsLocal = sim._epsLocal;
	_nAgent = sim._nAgent;
	_stepL = sim._stepL;
	_stepG = sim._stepG;
	_iter = sim._iter;
	_time = sim._time;
	_fc = sim._fc;
	_fcSym = sim._fcSym;
	_iterLocalTotal = sim._iterLocalTotal;

	_LAMBDA = MatrixCPU(sim._LAMBDA);
	_trade = MatrixCPU(sim._trade);
	_tradeSym = MatrixCPU(sim._tradeSym);
	_Pn = MatrixCPU(sim._Pn);
	_resF = MatrixCPU(sim._resF);
	_MU = MatrixCPU(sim._MU);

}



Simparam::Simparam(int nAgent) {
#ifdef DEBUG_CONSTRUCTOR
	std::cout << "simparam constructor 1" << std::endl;
#endif // DEBUG_CONSTRUCTOR
	
	_nAgent = nAgent;
	_rho = RHO;
	_iterMaxGlobal = ITERMAXGLOBAL;
	_iterMaxLocal = ITERMAXLOCAL;
	_epsGlobal = EPSGLOBAL;
	_epsLocal =  EPSLOCAL;
	_stepL = STEPL;
	_stepG = STEPG;

	_LAMBDA = MatrixCPU(nAgent, nAgent);
	_trade = MatrixCPU(nAgent, nAgent);
	_tradeSym = MatrixCPU(nAgent, nAgent);
	_Pn = MatrixCPU(nAgent, 1);
	_resF = MatrixCPU(2, (_iterMaxGlobal/STEPG)+1);
	_MU = MatrixCPU(nAgent, 1);
	_iter = 0;
	_time = 0;
	_fc = 0;
	_fcSym = 0;
	_iterLocalTotal = 0;
}


Simparam::Simparam(float rho, int iterMaxGlobal, int iterMaxLocal, float epsGlobal, float epsLocal, int nAgent)
{
	#if DEBUG_CONSTRUCTOR
		std::cout << "constructeur simparam 2" << std::endl;
	#endif // DEBUG_CONSTRUCTOR
	_rho = rho;
	_iterMaxGlobal = iterMaxGlobal;
	_iterMaxLocal = iterMaxLocal;
	_nAgent = nAgent;
	_epsGlobal = epsGlobal;
	_epsLocal =  epsLocal;
	
	_stepL = STEPL;
	_stepG = STEPG;
	_LAMBDA = MatrixCPU(nAgent, nAgent);
	_trade = MatrixCPU(nAgent, nAgent);
	_tradeSym = MatrixCPU(nAgent, nAgent);
	_Pn = MatrixCPU(nAgent, 1);
	_resF = MatrixCPU(2, (_iterMaxGlobal / STEPG)+1);
	_MU = MatrixCPU(nAgent, 1);
	_iter = 0;
	_time = 0;
	_fc = 0;
	_fcSym = 0;
	_iterLocalTotal = 0;
}

void Simparam::reset()
{
	_LAMBDA = MatrixCPU(_nAgent, _nAgent);
	_trade = MatrixCPU(_nAgent, _nAgent);
	_tradeSym = MatrixCPU(_nAgent, _nAgent);
	_Pn = MatrixCPU(_nAgent, 1);
	_MU = MatrixCPU(_nAgent, 1);
}

Simparam& Simparam::operator=(const Simparam& sim)
{
#if DEBUG_CONSTRUCTOR
	std::cout << "operateur =" << std::endl;
#endif // DEBUG_CONSTRUCTOR
	_rho = sim._rho;
	_iterMaxGlobal = sim._iterMaxGlobal;
	_iterMaxLocal = sim._iterMaxLocal;
	_epsGlobal = sim._epsGlobal;
	_epsLocal = sim._epsLocal;
	_nAgent = sim._nAgent;

    _iter = sim._iter;
	_iterLocalTotal = sim._iterLocalTotal;
	_time = sim._time;
	_fc = sim._fc;
	_fcSym = sim._fcSym;


	_LAMBDA = MatrixCPU(sim._LAMBDA);
	_trade = MatrixCPU(sim._trade);
	_tradeSym = MatrixCPU(sim._tradeSym);
	_Pn = MatrixCPU(sim._Pn);
	_resF = MatrixCPU(sim._resF);
	_MU = MatrixCPU(sim._MU);
	


	return *this;
}

Simparam::~Simparam()
{
}

float Simparam::getRho() const
{
	return _rho;
}

int Simparam::getIterG() const
{
	return _iterMaxGlobal;
}

int Simparam::getIter() const
{
	return _iter;
}

int Simparam::getIterLTot() const
{
	return _iterLocalTotal;
}

int Simparam::getIterL() const
{
	return _iterMaxLocal;
}

float Simparam::getEpsG() const
{
	return _epsGlobal;
}

float Simparam::getEpsL() const
{
	return _epsLocal;
}

int Simparam::getNAgent() const
{
	return _nAgent;
}

int Simparam::getStepL() const
{
	return _stepL;
}
int Simparam::getStepG() const
{
	return _stepG;
}


MatrixCPU Simparam::getTrade() const {
	return _trade;
}
MatrixCPU Simparam::getTradeSym() const
{
	return _tradeSym;
}
MatrixCPU Simparam::getLambda() const
{
	return _LAMBDA;
}
MatrixCPU Simparam::getRes() const {
	return _resF;
}

MatrixCPU Simparam::getPn() const
{
	return _Pn;
}

float Simparam::getTime() const
{
	return _time;
}

MatrixCPU Simparam::getMU() const
{
	return _MU;
}

float Simparam::getFc() const
{
	return _fc;
}

float Simparam::getFcSym() const
{
	return _fcSym;
}

void Simparam::setItG(int iter) {
	_iterMaxGlobal = iter;
	_resF = MatrixCPU(2, (_iterMaxGlobal/_stepG)+1);
}

void Simparam::setItL(int iter) {
	_iterMaxLocal = iter;
}


void Simparam::setLAMBDA(MatrixCPU* m)
{
	_LAMBDA.set(m);
}

void Simparam::setTrade(MatrixCPU* m)
{
	_trade.set(m);
}

void Simparam::setTradeSym(MatrixCPU* m)
{
	_tradeSym.set(m);
}


void Simparam::setResF(MatrixCPU* m)
{
	_resF.set(m);
}

void Simparam::setMU(MatrixCPU* m)
{
	_MU.set(m);
}
void Simparam::setPn(MatrixCPU* m)
{
	_Pn.set(m);
}

void Simparam::setTimeBloc(MatrixCPU* time, MatrixCPU* occurrence)
{
	timePerBlock.set(time);
	occurencePerBlock.set(occurrence);
}

void Simparam::setIter(int c)
{
	_iter = c;
}

void Simparam::setTime(float f)
{
	_time = f;
}

void Simparam::setFc(float f)
{
	_fc = f;
}

void Simparam::setFcSym(float f)
{
	_fcSym = f;
}

void Simparam::setNagent(int n)
{
	_nAgent = n;
	reset();
}

void Simparam::setItLTot(int iterLocalTotal)
{
	_iterLocalTotal = iterLocalTotal;
}

void Simparam::setRho(float rho) {
	_rho = rho;
}

void Simparam::setStep(int stepG, int stepL)
{
	_stepG = stepG;
	_stepL = stepL;
	_resF = MatrixCPU(2, (_iterMaxGlobal / _stepG) + 1);
}

void Simparam::setEpsG(float epsG)
{
	_epsGlobal = epsG;
}

void Simparam::setEpsL(float epsL)
{
	_epsLocal = epsL;
}



void Simparam::saveCSV(const std::string& fileName, int all)
{
	/*
	float _rho;
	int _iterMaxGlobal;
	int _iterMaxLocal;
	float _epsGlobal;
	float _epsLocal;
	int _nAgent;
	MatrixCPU _LAMBDA;
	MatrixCPU _trade;
	MatrixCPU _Pn;
	MatrixCPU _resF;
	MatrixCPU MU;
	int _iter;
	float _time;
	float _fc;*/
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	

	int Nligne = 1 + _nAgent + _nAgent + 1 + 2 + 1;
	if (_MU.getNLin() > 0) {
		Nligne = Nligne + _nAgent;
	}

	if (all) {
		MatrixCPU param(1, 7);
		param.set(0, 0, _rho);
		param.set(0, 1, _iterMaxGlobal);
		param.set(0, 2, _iterMaxLocal);
		param.set(0, 3, _epsGlobal);
		param.set(0, 4, _epsLocal);
		param.set(0, 5, _nAgent);
		param.set(0, 6, Nligne);
		param.saveCSV(fileName, mode);
	}
	
	_LAMBDA.saveCSV(fileName, mode);
	_trade.saveCSV(fileName, mode);
	if (all) {

		MatrixCPU temp(1, _nAgent);
		temp.addTrans(&_Pn);
		temp.saveCSV(fileName, mode);
		_resF.saveCSV(fileName, mode);
		_MU.saveCSV(fileName, mode);

		MatrixCPU result(1, 3);
		result.set(0, 0, _iter);
		result.set(0, 1, _time);
		result.set(0, 2, _fc);
		result.saveCSV(fileName, mode);
	}
}

void Simparam::display(int type) const
{  // type =1 parameters, type = 2 all result, other just some results
	if (type==1) {
		std::cout << "Simulation's parameters : " << std::endl;
		std::cout << "Agents' count : " << _nAgent << std::endl;
		std::cout << "rho : " << _rho << std::endl;
		std::cout << "k_max : " << _iterMaxGlobal << std::endl;
		std::cout << "j_max : " << _iterMaxLocal << std::endl;
		std::cout << "eps_g : " << _epsGlobal << std::endl;
		std::cout << "eps_l : " << _epsLocal << std::endl;
		std::cout << "StepG/StepL if on GPU : " << _stepG << " / " << _stepL << std::endl;

	}
	else if (type == 2) {
		std::cout << " Simulation result : " << std::endl;
		std::cout << " Agents' count : " << _nAgent << std::endl;
		std::cout << "f_c : " << _fc << std::endl;
		std::cout << "iter : " << _iter << std::endl;
		std::cout << "Pn : " << std::endl;
		_Pn.display();
		std::cout << "Residuals : " << _resF.get(0, (_iter - 1)/_stepG) << " " << _resF.get(1, (_iter - 1) / _stepG) << std::endl;
		std::cout << "computation time" << _time << std::endl;
		
		std::cout << "Trades : " << std::endl;
		_trade.display();
		std::cout << "LAMBDA : " << std::endl;
		_LAMBDA.display();
	}
	else {
		std::cout << " Simulation result : " << std::endl;
		std::cout << " Agents' count : " << _nAgent << std::endl;
		std::cout << "f_c : " << _fc << std::endl;
		std::cout << "iter : " << _iter << std::endl;
		std::cout << "Residuals : " << _resF.get(0, (_iter - 1) / _stepG) << " " << _resF.get(1, (_iter - 1) / _stepG) << std::endl;
		std::cout << "computation time : " << _time << std::endl;
	}
}

void Simparam::displayTime() const
{
	std::string fileName = "SimutemporalFBCISTEMFINAL.csv";
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	float factor = 1000000; // go from ns to ms fot the time
	occurencePerBlock.saveCSV(fileName, mode);
	timePerBlock.saveCSV(fileName, mode);
	if (occurencePerBlock.get(0, 0) != 0) {
		std::cout << "total resolution time :" << timePerBlock.sum() / (1000 * factor) << "s" << std::endl;
		std::cout << " Fb0 : " << timePerBlock.get(0, 0) / factor << "ms and occurence :" << occurencePerBlock.get(0, 0) << std::endl;
		if (occurencePerBlock.get(0, 2) != 0) {
			std::cout << " Fb1a : " << timePerBlock.get(0, 1) / factor << "ms and occurence :" << occurencePerBlock.get(0, 1) << std::endl;
			std::cout << " Fb1b : " << timePerBlock.get(0, 2) / factor << "ms and occurence :" << occurencePerBlock.get(0, 2) << std::endl;
			std::cout << " Fb1c : " << timePerBlock.get(0, 3) / factor << "ms and occurence :" << occurencePerBlock.get(0, 3) << std::endl;
		}
		else {
			std::cout << " Fb1 : " << timePerBlock.get(0, 1) / factor << "ms and occurence :" << occurencePerBlock.get(0, 1) << std::endl;
		}
		std::cout << " Fb2 : " << timePerBlock.get(0, 4) / factor << "ms and occurence :" << occurencePerBlock.get(0, 4) << std::endl;
		std::cout << " Fb3 : " << timePerBlock.get(0, 5) / factor << "ms and occurence :" << occurencePerBlock.get(0, 5) << std::endl;
		std::cout << " Fb4 : " << timePerBlock.get(0, 6) / factor << "ms and occurence :" << occurencePerBlock.get(0, 6) << std::endl;
		std::cout << " Fb5 : " << timePerBlock.get(0, 7) / factor << "ms and occurence :" << occurencePerBlock.get(0, 7) << std::endl;
		if (occurencePerBlock.get(0, 8) != 0) {
			std::cout << " Fb0 update : " << timePerBlock.get(0, 8) / factor << "ms and occurence :" << occurencePerBlock.get(0, 8) << std::endl;
		}
	}
	else {
		std::cout << "pas de temps � afficher, ou alors il n'y a pas eut d'initialisation" << std::endl;
	}

	
}
