#include "../head/ADMMGPU6.cuh"

ADMMGPU6::ADMMGPU6() : Method()
{
#if DEBUG_CONSTRUCTOR
	std::cout << " ADMMGPU6 constructor" << std::endl;
#endif // DEBUG_CONSTRUCTOR
	_name = NAME;
}


ADMMGPU6::ADMMGPU6(float rho) : Method()
{
#if DEBUG_CONSTRUCTOR
	std::cout << "default ADMMGPU6 constructor" << std::endl;
#endif // DEBUG_CONSTRUCTOR
	_name = NAME;
	_rho = rho;
}

ADMMGPU6::~ADMMGPU6()
{
}

void ADMMGPU6::setParam(float rho)
{
	_rho = rho;
}

void ADMMGPU6::setTau(float tau)
{
	if (tau < 1) {
		throw std::invalid_argument("tau must be greater than 1");
	}
	_tau = tau;
}

void ADMMGPU6::init(const Simparam& sim, const StudyCase& cas)
{
	
	clock_t t = clock();
	_rhog = sim.getRho();
	
	int nAgent = sim.getNAgent();
	_n = nAgent;
	float rho_p = _rho * _n;
	if (_rho == 0) {
		rho_p = _rhog;
	}
	MatrixCPU GAMMA(cas.getG());
	MatrixGPU connect(cas.getC());
	MatrixGPU Ub(cas.getUb());
	MatrixGPU Lb(cas.getLb());
	MatrixCPU LAMBDA(sim.getLambda());
	MatrixCPU trade(sim.getTrade());

	a = cas.geta();
	b = cas.getb();

	Pmin = MatrixGPU(cas.getPmin());
	Pmax = MatrixGPU(cas.getPmax());
	nVoisin = MatrixGPU(cas.getNvoi());

	
	int nTradeTot = nVoisin.sum();
	_N = nTradeTot;
	_numBlocks1 = ceil((_n + _blockSize - 1) / _blockSize);
	_numBlocks2 = ceil((_N + _blockSize - 1) / _blockSize);
	CoresMatLin = MatrixGPU(nAgent, nAgent, -1);
	CoresLinAgent = MatrixGPU(nTradeTot, 1);
	CoresAgentLin = MatrixGPU(nAgent + 1, 1);
	CoresLinVoisin = MatrixGPU(nTradeTot, 1);
	CoresLinTrans = MatrixGPU(nTradeTot, 1);

	MU = sim.getMU(); //  lambda_l/_rho

	Tlocal_pre = MatrixGPU(nTradeTot, 1);
	tradeLin = MatrixGPU(nTradeTot, 1);
	Tmoy = sim.getPn();
	LAMBDALin = MatrixGPU(nTradeTot, 1);

	_at1 = _rhog; //  2*a
	_at2 = rho_p;

	Ap2 = MatrixGPU(a);
	Ap1 = MatrixGPU(nVoisin);
	Ap12 = MatrixGPU(nAgent, 1);
	

	Bt1 = MatrixGPU(nTradeTot, 1);
	Ct = MatrixGPU(nTradeTot, 1);
	Cp = MatrixGPU(b);
	matUb = MatrixGPU(nTradeTot, 1);
	matLb = MatrixGPU(nTradeTot, 1);


	int indice = 0;

	for (int idAgent = 0;idAgent < nAgent; idAgent++) {
		MatrixCPU omega(cas.getVoisin(idAgent));
		int Nvoisinmax = nVoisin.get(idAgent, 0);
		for (int voisin = 0; voisin < Nvoisinmax; voisin++) {
			int idVoisin = omega.get(voisin, 0);
			matLb.set(indice, 0, Lb.get(idAgent, 0));
			matUb.set(indice, 0, Ub.get(idAgent, 0));
			Ct.set(indice, 0, GAMMA.get(idAgent, idVoisin));
			tradeLin.set(indice, 0, trade.get(idAgent, idVoisin));
			Tlocal_pre.set(indice, 0, trade.get(idAgent, idVoisin));
			LAMBDALin.set(indice, 0, LAMBDA.get(idAgent, idVoisin));
			CoresLinAgent.set(indice, 0, idAgent);
			CoresLinVoisin.set(indice, 0, idVoisin);
			CoresMatLin.set(idAgent, idVoisin, indice);
			indice = indice + 1;
		}
		CoresAgentLin.set(idAgent + 1, 0, indice);
	}
	for (int lin = 0;lin < nTradeTot;lin++) {
		int i = CoresLinAgent.get(lin,0);
		int j = CoresLinVoisin.get(lin, 0);
		int k = CoresMatLin.get(j,i); 
		CoresLinTrans.set(lin, 0, k);
	}

	tempN = MatrixGPU(_numBlocks2, 1, 0, 1); 
	tempN2 = MatrixGPU(_numBlocks2, 1, 0, 1);
	tempNN = MatrixGPU(_N, 1, 0, 1);
	tempN1 = MatrixGPU(_n, 1, 0, 1); 

	Tlocal = MatrixGPU(_N, 1, 0, 1);
	P = MatrixGPU(_n, 1, 0, 1); 
	Pn = MatrixGPU(_n, 1, 0, 1); 

	matUb.transferGPU();
	matLb.transferGPU();
	Pmin.transferGPU();
	Pmax.transferGPU();
	nVoisin.transferGPU();
	a.transferGPU();
	b.transferGPU();
	MU.transferGPU();
	Tlocal_pre.transferGPU();
	tradeLin.transferGPU();
	LAMBDALin.transferGPU();
	Tmoy.transferGPU();

	Ap2.transferGPU();
	Ap1.transferGPU();
	Ap12.transferGPU();
	Bt1.transferGPU();
	Ct.transferGPU();
	Cp.transferGPU();
	CoresAgentLin.transferGPU();
	CoresLinAgent.transferGPU();
	CoresLinVoisin.transferGPU();
	CoresMatLin.transferGPU();
	CoresLinTrans.transferGPU();

	Pmin.divideT(&nVoisin);
	Pmax.divideT(&nVoisin);
	Ap2.multiplyT(&nVoisin);
	Ap2.multiplyT(&nVoisin);
	Ap1.multiply(rho_p);
	Ap12.add(&Ap1, &Ap2);
	Cp.multiplyT(&nVoisin);
	Tmoy.divideT(&nVoisin);
	updateGlobalProbGPU();
	
}



void ADMMGPU6::updateP0(const StudyCase& cas)
{
	_id = _id + 1;
#ifdef INSTRUMENTATION
	cudaDeviceSynchronize();
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
#endif // INSTRUMENTATION

	
	matLb.transferCPU();
	Pmin.transferCPU();
	Pmax.transferCPU();
	Cp.transferCPU();
	nVoisin.transferCPU();
	b.transferCPU();

	Pmin = MatrixGPU(cas.getPmin());
	Pmax = MatrixGPU(cas.getPmax());


	MatrixGPU Lb(cas.getLb());

	b = cas.getb();
	Cp = cas.getb();
	int indice = 0;

	for (int idAgent = 0;idAgent < _n; idAgent++) {
		int Nvoisinmax = nVoisin.get(idAgent, 0);
		for (int voisin = 0; voisin < Nvoisinmax; voisin++) {
			matLb.set(indice, 0, Lb.get(idAgent, 0));
			indice = indice + 1;
		}
	}

	nVoisin.transferGPU();
	matLb.transferGPU();
	Pmin.transferGPU();
	Pmax.transferGPU();
	Cp.transferGPU();
	b.transferGPU();

	Pmin.divideT(&nVoisin);
	Pmax.divideT(&nVoisin);
	Cp.multiplyT(&nVoisin);
#ifdef INSTRUMENTATION
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	timePerBlock.increment(0, 8, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
	occurencePerBlock.increment(0, 8, 1);
#endif // INSTRUMENTATION
}


void ADMMGPU6::solve(Simparam* result, const Simparam& sim, const StudyCase& cas)
{
#ifdef DEBUG_SOLVE
	cas.display();
	sim.display(1);
#endif // DEBUG_SOLVE
	
	// FB 0
	clock_t tall = clock();
#ifdef INSTRUMENTATION
	std::chrono::high_resolution_clock::time_point t1;
	std::chrono::high_resolution_clock::time_point t2;
#endif // INSTRUMENTATION
	// FB 0
	if (_id == 0) {
#ifdef INSTRUMENTATION
		cudaDeviceSynchronize();
		t1 = std::chrono::high_resolution_clock::now();
#endif // INSTRUMENTATION
		init(sim, cas);
#ifdef INSTRUMENTATION
		cudaDeviceSynchronize();
		t2 = std::chrono::high_resolution_clock::now();
		timePerBlock.set(0, 0, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
		occurencePerBlock.set(0, 0, 1);
#endif // INSTRUMENTATION
	}
	_rhog = sim.getRho();
	_at1 = _rhog; // 2*a
	
	float epsG = sim.getEpsG();
	float epsL = sim.getEpsL();
	const int stepL = sim.getStepL();
	const int stepG = sim.getStepG();
	const int iterG = sim.getIterG();
	const int iterL = sim.getIterL();
	

	float resG = 2 * epsG;
	float resL = 2 * epsL;
	int iterGlobal = 0;
	int iterLocal = 0;

	MatrixCPU nVoisinCPU(cas.getNvoi());
	MatrixCPU LAMBDA(sim.getLambda());
	MatrixCPU trade(sim.getTrade());
	MatrixCPU resF(2, (iterG / stepG) + 1);

	
	while ((iterGlobal < iterG) && (resG>epsG)) {
		resL = 2 * epsL;
		iterLocal = 0;
		while (iterLocal< iterL && resL>epsL) {
			// FB 1
#ifdef INSTRUMENTATION
			cudaDeviceSynchronize();
			t1 = std::chrono::high_resolution_clock::now();
#endif // INSTRUMENTATION
			updateLocalProbGPU(&Tlocal, &P);
#ifdef INSTRUMENTATION
			cudaDeviceSynchronize();
			t2 = std::chrono::high_resolution_clock::now();
			timePerBlock.increment(0, 1, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
#endif // INSTRUMENTATION
			if (!(iterLocal % stepL)) {
				// FB 2
#ifdef INSTRUMENTATION
				cudaDeviceSynchronize();
				t1 = std::chrono::high_resolution_clock::now();
#endif // INSTRUMENTATION
				resL = calcRes(&Tlocal,&P, &tempN, &tempN2); 
#ifdef INSTRUMENTATION
				cudaDeviceSynchronize();
				t2 = std::chrono::high_resolution_clock::now();
				timePerBlock.increment(0, 4, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
#endif // INSTRUMENTATION
			}
			Tlocal.swap(&Tlocal_pre); 
			iterLocal++;
		}
#ifdef INSTRUMENTATION
		occurencePerBlock.increment(0, 1, iterLocal);
		occurencePerBlock.increment(0, 4, iterLocal / stepL);
#endif // INSTRUMENTATION

		Tlocal.swap(&Tlocal_pre);
		tradeLin.swap(&Tlocal);
		// FB 3
#ifdef INSTRUMENTATION
		cudaDeviceSynchronize();
		t1 = std::chrono::high_resolution_clock::now();
#endif // INSTRUMENTATION
		updateGlobalProbGPU();
#ifdef INSTRUMENTATION
		cudaDeviceSynchronize();
		t2 = std::chrono::high_resolution_clock::now();
		timePerBlock.increment(0, 5, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
#endif // INSTRUMENTATION
		if (!(iterGlobal % stepG)) {// FB 4
#ifdef INSTRUMENTATION
			cudaDeviceSynchronize();
			t1 = std::chrono::high_resolution_clock::now();
#endif // INSTRUMENTATION
			resG = updateRes(&resF, &Tlocal, iterGlobal / stepG, &tempN, &tempN2);
#ifdef INSTRUMENTATION
			cudaDeviceSynchronize();
			t2 = std::chrono::high_resolution_clock::now();
			timePerBlock.increment(0, 6, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
#endif // INSTRUMENTATION
		}
		iterGlobal++;
	}
#ifdef INSTRUMENTATION
	occurencePerBlock.increment(0, 5, iterGlobal);
	occurencePerBlock.increment(0, 6, iterGlobal / stepG);
	// FB 5
	cudaDeviceSynchronize();
	t1 = std::chrono::high_resolution_clock::now();
#endif // INSTRUMENTATION

	updatePnGPU << <_numBlocks1, _blockSize >> > (Pn._matrixGPU, Tmoy._matrixGPU, nVoisin._matrixGPU, _n);
	float fc = calcFc(&a, &b, &tradeLin, &Pn, &Ct, &tempN1, &tempNN);
	
	MatrixCPU tradeLinCPU;
	tradeLin.toMatCPU(tradeLinCPU);
	MatrixCPU LAMBDALinCPU;
	LAMBDALin.toMatCPU(LAMBDALinCPU);
	MatrixCPU PnCPU;
	Pn.toMatCPU(PnCPU);
	MatrixCPU MUCPU;
	MU.toMatCPU(MUCPU);
	int indice = 0;
	for (int idAgent = 0;idAgent < _n; idAgent++) {
		MatrixCPU omega(cas.getVoisin(idAgent));
		int Nvoisinmax = nVoisinCPU.get(idAgent, 0);
		for (int voisin = 0; voisin < Nvoisinmax; voisin++) {
			int idVoisin = omega.get(voisin, 0);
			trade.set(idAgent, idVoisin, tradeLinCPU.get(indice, 0));
			LAMBDA.set(idAgent, idVoisin, LAMBDALinCPU.get(indice, 0));
			indice = indice + 1;
		}
	}

	result->setResF(&resF);
	result->setLAMBDA(&LAMBDA);
	result->setTrade(&trade);
	result->setIter(iterGlobal);
	result->setPn(&PnCPU);
	result->setFc(fc);
	result->setMU(&MUCPU);
	result->setRho(_rhog);
	
#ifdef INSTRUMENTATION
	cudaDeviceSynchronize();
	t2 = std::chrono::high_resolution_clock::now();
	timePerBlock.increment(0, 7, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
	occurencePerBlock.increment(0, 7, 1);

	result->setTimeBloc(&timePerBlock, &occurencePerBlock);
#endif // INSTRUMENTATION

	result->setTimeBloc(&timePerBlock, &occurencePerBlock);
	tall = clock() - tall;
	result->setTime((float)tall / CLOCKS_PER_SEC);
}

void ADMMGPU6::updateLocalProbGPU( MatrixGPU* Tlocal, MatrixGPU* P) 
{
	int numBlocks = _n;
	switch (_blockSize) {
	case 512:
		
		updateTradePGPU<512> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU, _n);
		break;
	case 256:
		updateTradePGPU<256> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU, _n);
		break;
	case 128:
		updateTradePGPU<128> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU, _n);
		break;
	case 64:
		updateTradePGPU< 64> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU, _n);
		break;
	case 32:
		updateTradePGPU< 32> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU, _n);
		break;
	case 16:
		updateTradePGPU< 16> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU, _n);
		break;
	case  8:
		updateTradePGPU<  8> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU, _n);
		break;
	case  4:
		updateTradePGPU<  4> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU, _n);
		break;
	case  2:
		updateTradePGPU<  2> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU, _n);
		break;
	case  1:
		updateTradePGPU<  1> << <numBlocks, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, nVoisin._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU,
			matLb._matrixGPU, matUb._matrixGPU, Ap1._matrixGPU, Ap12._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, CoresAgentLin._matrixGPU, _n);
		break;
	}
}



void ADMMGPU6::updateGlobalProbGPU()
{
	updateLAMBDABt1GPU <<<_numBlocks2, _blockSize >>> (Bt1._matrixGPU, LAMBDALin._matrixGPU, tradeLin._matrixGPU, _rhog, CoresLinTrans._matrixGPU, _N);
}




float ADMMGPU6::updateRes(MatrixCPU* res, MatrixGPU* Tlocal, int iter, MatrixGPU* tempN, MatrixGPU* tempN2) {
	int numBlocks = _numBlocks2;
	
	float resS = 0; // res(1)
	float resR = 0; // res(0)

	switch (_blockSize) {
	case 512:
		updateResGPUMultiBloc<512><<<numBlocks,_blockSize>>>(tempN->_matrixGPU, tempN2->_matrixGPU, tradeLin._matrixGPU, Tlocal->_matrixGPU, CoresLinTrans._matrixGPU, _N);
		maxMonoBlock6<512> << < 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	case 256:
		updateResGPUMultiBloc<256> << <numBlocks, _blockSize >> > (tempN->_matrixGPU, tempN2->_matrixGPU, tradeLin._matrixGPU, Tlocal->_matrixGPU, CoresLinTrans._matrixGPU, _N);
		maxMonoBlock6<256> << < 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	case 128:
		updateResGPUMultiBloc<128> <<<numBlocks, _blockSize >>> (tempN->_matrixGPU, tempN2->_matrixGPU, tradeLin._matrixGPU, Tlocal->_matrixGPU, CoresLinTrans._matrixGPU, _N);
		maxMonoBlock6<128> <<< 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	case 64:
		updateResGPUMultiBloc<64> << <numBlocks, _blockSize >> > (tempN->_matrixGPU, tempN2->_matrixGPU, tradeLin._matrixGPU, Tlocal->_matrixGPU, CoresLinTrans._matrixGPU, _N);
		maxMonoBlock6<64> << < 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	case 32:
		updateResGPUMultiBloc<32> << <numBlocks, _blockSize >> > (tempN->_matrixGPU, tempN2->_matrixGPU, tradeLin._matrixGPU, Tlocal->_matrixGPU, CoresLinTrans._matrixGPU, _N);
		maxMonoBlock6<32> << < 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	case 16:
		updateResGPUMultiBloc<16> << <numBlocks, _blockSize >> > (tempN->_matrixGPU, tempN2->_matrixGPU, tradeLin._matrixGPU, Tlocal->_matrixGPU, CoresLinTrans._matrixGPU, _N);
		maxMonoBlock6<16> << < 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	case  8:
		updateResGPUMultiBloc<8> << <numBlocks, _blockSize >> > (tempN->_matrixGPU, tempN2->_matrixGPU, tradeLin._matrixGPU, Tlocal->_matrixGPU, CoresLinTrans._matrixGPU, _N);
		maxMonoBlock6<8> << < 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	case  4:
		updateResGPUMultiBloc<4> << <numBlocks, _blockSize >> > (tempN->_matrixGPU, tempN2->_matrixGPU, tradeLin._matrixGPU, Tlocal->_matrixGPU, CoresLinTrans._matrixGPU, _N);
		maxMonoBlock6<4> << < 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	case  2:
		updateResGPUMultiBloc<2> << <numBlocks, _blockSize >> > (tempN->_matrixGPU, tempN2->_matrixGPU, tradeLin._matrixGPU, Tlocal->_matrixGPU, CoresLinTrans._matrixGPU, _N);
		maxMonoBlock6<2> << < 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	case  1:
		updateResGPUMultiBloc<1> <<<numBlocks, _blockSize >> > (tempN->_matrixGPU, tempN2->_matrixGPU, tradeLin._matrixGPU, Tlocal->_matrixGPU, CoresLinTrans._matrixGPU, _N);
		maxMonoBlock6<1> <<< 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	}
	cudaMemcpy(&resS, tempN->_matrixGPU, sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(&resR, tempN2->_matrixGPU, sizeof(float), cudaMemcpyDeviceToHost);
	resS = sqrt(resS);
	resR = sqrt(resR);
	res->set(0, iter, resR);
	res->set(1, iter, resS);

	
	if (resR > _mu * resS) {
		_rhog = _tau * _rhog;
		_at1 = _rhog;
	}
	else if (resS > _mu * resR) {// rho = rho / tau_inc;
		_rhog = _rhog / _tau;
		_at1 = _rhog;
	}
	
	return resR * (resR > resS) + resS * (resR <= resS);
}
	

float ADMMGPU6::calcRes( MatrixGPU* Tlocal, MatrixGPU* P, MatrixGPU* tempN, MatrixGPU* tempN2) {
	int numBlocks = _numBlocks2;

	float d1 = 0; // res(1)
	float d2 = 0; // res(0)

	switch (_blockSize) {
	case 512:
		updateResLGPUMultiBloc<512> << <numBlocks, _blockSize >> > (tempN->_matrixGPU, tempN2->_matrixGPU, Tlocal_pre._matrixGPU, Tlocal->_matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, _N, _n);
		maxMonoBlock6<512> << < 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	case 256:
		updateResLGPUMultiBloc<256> << <numBlocks, _blockSize >> > (tempN->_matrixGPU, tempN2->_matrixGPU, Tlocal_pre._matrixGPU, Tlocal->_matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, _N, _n);
		maxMonoBlock6<256> << < 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	case 128:
		updateResLGPUMultiBloc<128> << <numBlocks, _blockSize >> > (tempN->_matrixGPU, tempN2->_matrixGPU, Tlocal_pre._matrixGPU, Tlocal->_matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, _N, _n);
		maxMonoBlock6<128> << < 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	case 64:
		updateResLGPUMultiBloc<64> << <numBlocks, _blockSize >> > (tempN->_matrixGPU, tempN2->_matrixGPU, Tlocal_pre._matrixGPU, Tlocal->_matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, _N, _n);
		maxMonoBlock6<64> << < 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	case 32:
		updateResLGPUMultiBloc<32> << <numBlocks, _blockSize >> > (tempN->_matrixGPU, tempN2->_matrixGPU, Tlocal_pre._matrixGPU, Tlocal->_matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, _N, _n);
		maxMonoBlock6<32> << < 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	case 16:
		updateResLGPUMultiBloc<16> << <numBlocks, _blockSize >> > (tempN->_matrixGPU, tempN2->_matrixGPU, Tlocal_pre._matrixGPU, Tlocal->_matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, _N, _n);
		maxMonoBlock6<16> << < 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	case  8:
		updateResLGPUMultiBloc<8> << <numBlocks, _blockSize >> > (tempN->_matrixGPU, tempN2->_matrixGPU, Tlocal_pre._matrixGPU, Tlocal->_matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, _N, _n);
		maxMonoBlock6<8> << < 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	case  4:
		updateResLGPUMultiBloc<4> << <numBlocks, _blockSize >> > (tempN->_matrixGPU, tempN2->_matrixGPU, Tlocal_pre._matrixGPU, Tlocal->_matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, _N, _n);
		maxMonoBlock6<4> << < 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	case  2:
		updateResLGPUMultiBloc<2> << <numBlocks, _blockSize >> > (tempN->_matrixGPU, tempN2->_matrixGPU, Tlocal_pre._matrixGPU, Tlocal->_matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, _N, _n);
		maxMonoBlock6<2> << < 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	case  1:
		updateResLGPUMultiBloc<1> << <numBlocks, _blockSize >> > (tempN->_matrixGPU, tempN2->_matrixGPU, Tlocal_pre._matrixGPU, Tlocal->_matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, _N, _n);
		maxMonoBlock6<1> << < 1, _blockSize >> > (tempN->_matrixGPU, tempN->_matrixGPU, tempN2->_matrixGPU, tempN2->_matrixGPU, numBlocks);
		break;
	}
	cudaMemcpy(&d1, tempN->_matrixGPU, sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(&d2, tempN2->_matrixGPU, sizeof(float), cudaMemcpyDeviceToHost);

	d1 = sqrt(d1);
	d2 = sqrt(d2);

	return d1 * (d1 > d2) + d2 * (d2 >= d1);

}





__device__ float warpReduceMax6(volatile float* r, int const N) {
	int idx = threadIdx.x % warpSize; //the lane index in the warp
	if (idx < 16) {
		if (idx + 16 < N) r[idx] = r[idx + 16] > r[idx] ? r[idx + 16] : r[idx];//r[idx + 16] > r[idx] ? r[idx + 16] : r[idx];//r[idx + 16] * (r[idx + 16] > r[idx]) + r[idx] * (r[idx] <= r[idx + 16]);
		if (idx + 8 < N) r[idx] = r[idx + 8] > r[idx] ? r[idx + 8] : r[idx];//r[idx +  8] > r[idx] ? r[idx +  8] : r[idx];//r[idx +  8] * (r[idx +  8] > r[idx]) + r[idx] * (r[idx] <= r[idx +  8]);
		if (idx + 4 < N) r[idx] = r[idx + 4] > r[idx] ? r[idx + 4] : r[idx];//r[idx +  4] > r[idx] ? r[idx +  4] : r[idx];//r[idx +  4] * (r[idx +  4] > r[idx]) + r[idx] * (r[idx] <= r[idx +  4]);
		if (idx + 2 < N) r[idx] = r[idx + 2] > r[idx] ? r[idx + 2] : r[idx];//r[idx +  2] > r[idx] ? r[idx +  2] : r[idx];//r[idx +  2] * (r[idx +  2] > r[idx]) + r[idx] * (r[idx] <= r[idx +  2]);
		if (idx + 1 < N) r[idx] = r[idx + 1] > r[idx] ? r[idx + 1] : r[idx];//r[idx +  1] > r[idx] ? r[idx +  1] : r[idx];//r[idx +  1] * (r[idx +  1] > r[idx]) + r[idx] * (r[idx] <= r[idx +  1]);
	}
	return r[0];
}

template <unsigned int blockSize>
__global__ void maxMonoBlock6(float* res1, float* resOut1, float* res2, float* resOut2, int const N) {
	int idx = threadIdx.x;
	float max = 0;
	float max2 = 0;
	for (int i = idx; i < N; i += blockSize) {
		float s = res1[i];
		float s2 = res2[i];
		max = s > max ? s : max;// s>max ? s:max;//s * (s > max) + max * (max <= s);
		max2 = s2 > max2 ? s2 : max2;// s>max ? s:max;//s * (s > max) + max * (max <= s);
	}
	
	__shared__ float r[blockSize];
	__shared__ float r2[blockSize];
	r[idx] = max;
	r2[idx] = max2;
	warpReduceMax6(&r[idx & ~(warpSize - 1)], blockSize); // warpReduceMax6(&r[floor(idx/32)*32]);
	warpReduceMax6(&r2[idx & ~(warpSize - 1)], blockSize); 
	__syncthreads();
	if (idx < warpSize) { //first warp only
		r[idx] = idx * warpSize < blockSize ? r[idx * warpSize] : 0;
		r2[idx] = idx * warpSize < blockSize ? r2[idx * warpSize] : 0;
		warpReduceMax6(r, blockSize);
		warpReduceMax6(r2, blockSize);
		if (idx == 0) {
			*resOut1 = r[0];
			*resOut2 = r2[0];
		}	
	}
}

void ADMMGPU6::display() {

	std::cout << _name << std::endl;
}


