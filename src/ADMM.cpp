#include "../head/ADMM.h"

ADMM::ADMM() : Method()
{
#if DEBUG_CONSTRUCTOR
	std::cout << " ADMM Constructor" << std::endl;
#endif // DEBUG_CONSTRUCTOR
	_name = NAME;
}


ADMM::ADMM(float rho) : Method()
{
#if DEBUG_CONSTRUCTOR
	std::cout << "default ADMM Constructor" << std::endl;
#endif // DEBUG_CONSTRUCTOR
	_name = NAME;
	_rho = rho;
}

ADMM::~ADMM()
{
}
void ADMM::setParam(float rho)
{
	_rho = rho;
}

void ADMM::setTau(float tau)
{
	throw std::domain_error("tau is not define for this method");
}



void ADMM::solve(Simparam* result, const Simparam& sim, const StudyCase& cas)
{
#ifdef DEBUG_SOLVE
	cas.display();
	sim.display(1);
#endif // DEBUG_SOLVE
	
	clock_t tall = clock();
	// FB 0
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	float rho = sim.getRho();
	int iterG = sim.getIterG();
	int iterL = sim.getIterL();
	int stepL = sim.getStepL();
	int stepG = sim.getStepG();
	
	float epsG = sim.getEpsG();
	float epsL = sim.getEpsL();
	int nAgent = sim.getNAgent();
	MatrixCPU GAMMA(cas.getG());
	MatrixCPU connect(cas.getC());
	MatrixCPU a(cas.geta());
	MatrixCPU b(cas.getb());
	MatrixCPU Ub(cas.getUb());
	MatrixCPU Lb(cas.getLb());
	MatrixCPU Pmin(cas.getPmin());
	MatrixCPU Pmax(cas.getPmax());
	MatrixCPU nVoisin(cas.getNvoi());

	Pmin.divideT(&nVoisin);
	Pmax.divideT(&nVoisin);

	


	MatrixCPU LAMBDA(sim.getLambda());
	MatrixCPU trade(sim.getTrade());
	MatrixCPU Pn(sim.getPn()); // trades sum for each agent
	
	
	MatrixCPU resF(2, (iterG/stepG)+1);
	float fc = 0;


	MatrixCPU MU(nAgent, 1); //  lambda_l/_rho
	MatrixCPU Tlocal(nAgent, nAgent);
	MatrixCPU Tlocal_pre(trade);
	MatrixCPU Tmoy(nAgent,1);
	Tmoy.Moy(&Tlocal_pre, &nVoisin);
	MatrixCPU P(nAgent, 1); // trades mean for each agent
	
	
	

	float rho_p = _rho * nAgent;
	if (_rho == 0) {
		rho_p = rho;
	}
	

	float at1 = rho; // 2*a in the article
	float at2 = rho_p;

	MatrixCPU Ap2(nAgent, 1);
	MatrixCPU Ap1(nVoisin);
	MatrixCPU Ap12(nAgent, 1); // Ap2+Ap1;
	MatrixCPU Bt1(nAgent, nAgent);
	MatrixCPU Bt2(nAgent, nAgent);
	MatrixCPU Bp1(nAgent, 1);
	MatrixCPU Ct(nAgent, nAgent);
	MatrixCPU Cp(nAgent, 1);
	MatrixCPU matUb(nAgent, nAgent);
	MatrixCPU matLb(nAgent, nAgent);

	MatrixCPU tempN1(nAgent, 1);
	MatrixCPU tempNN(nAgent, nAgent);


	int iterLocal = 0;
	for (int i = 0; i < nAgent; i++) 
	{
		for (int j = 0; j < nAgent;j++) {
			matUb.set(i, j, Ub.get(i, 0));
			matLb.set(i, j, Lb.get(i, 0));
		}
	}
	matUb.multiplyT(&connect);
	matLb.multiplyT(&connect);
	
	Ap2.multiplyT(&a, &nVoisin);
	Ap2.multiplyT(&nVoisin);
	Ap1.multiply(rho_p);
	Ap12.add(&Ap1, &Ap2);
	Ct.multiplyT(&GAMMA, &connect);
	Cp.multiplyT(&b, &nVoisin); 
	updateLAMBDA(&LAMBDA, &trade, rho);
	updateBt1(&Bt1, &trade, rho, &LAMBDA);
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	timePerBlock.increment(0, 0, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
	occurencePerBlock.increment(0, 0, 1);
	
	float resG = 2 * epsG;
	float resL = 2 * epsL;
	int iterGlobal = 0;
	
	while ((iterGlobal < iterG) && (resG>epsG)) {
		resL = 2 * epsL;
		iterLocal = 0;
		
		while (iterLocal< iterL && resL>epsL) {
			// FB 1a	
			t1 = std::chrono::high_resolution_clock::now();
			updateBt2(&Bt2,&Tlocal_pre,&Tmoy,&P,&MU);
			updateTl(&Tlocal, at1, at2, &Bt1, &Bt2, &Ct, &matLb, &matUb);
			t2 = std::chrono::high_resolution_clock::now();
			timePerBlock.increment(0, 1, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());

			// FB 1b
			t1 = std::chrono::high_resolution_clock::now();
			Tmoy.Moy(&Tlocal, &nVoisin); 
			t2 = std::chrono::high_resolution_clock::now();
			timePerBlock.increment(0, 2, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());

			// FB 1c
			t1 = std::chrono::high_resolution_clock::now();
			updateBp1(&Bp1, &MU, &Tmoy);
			updateP(&P, &Ap1, &Ap12, &Bp1, &Cp, &Pmin,&Pmax);
			updateMU(&MU,&Tmoy,&P);
			t2 = std::chrono::high_resolution_clock::now();
			timePerBlock.increment(0, 3, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());


			// FB 2
			t1 = std::chrono::high_resolution_clock::now();
			resL = calcRes(&Tlocal,&Tlocal_pre,&Tmoy,&P); 
			t2 = std::chrono::high_resolution_clock::now();
			timePerBlock.increment(0, 4, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());

			Tlocal_pre.swap(&Tlocal); 
			iterLocal++;
			
		}
		occurencePerBlock.increment(0, 1, iterLocal);
		occurencePerBlock.increment(0, 2, iterLocal);
		occurencePerBlock.increment(0, 3, iterLocal);
		occurencePerBlock.increment(0, 4, iterLocal);
		Tlocal_pre.swap(&Tlocal);
		trade.swap(&Tlocal);

		// FB 3
		t1 = std::chrono::high_resolution_clock::now();
		updateLAMBDA(&LAMBDA, &trade, rho);
		updateBt1(&Bt1, &trade, rho, &LAMBDA);
		t2 = std::chrono::high_resolution_clock::now();
		timePerBlock.set(0, 5, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
		// FB 4
		t1 = std::chrono::high_resolution_clock::now();
		resG = updateRes(&resF, &Tlocal, &trade,(iterGlobal/stepG));
		t2 = std::chrono::high_resolution_clock::now();
		timePerBlock.increment(0, 6, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());

		iterGlobal++;
	}
	occurencePerBlock.increment(0, 5, iterGlobal);
	occurencePerBlock.increment(0, 6, iterGlobal);
	// FB 5
	t1 = std::chrono::high_resolution_clock::now();
	result->setResF(&resF);
	
	result->setLAMBDA(&LAMBDA);
	
	result->setTrade(&trade);
	
	result->setIter(iterGlobal);
	
	updatePn(&Pn, &trade);
	result->setPn(&Pn);
	fc = calcFc(&a, &b, &trade, &Pn, &GAMMA,&tempN1,&tempNN);
	result->setFc(fc);
	t2 = std::chrono::high_resolution_clock::now();
	timePerBlock.increment(0, 7, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
	occurencePerBlock.increment(0, 7, 1);

	result->setTimeBloc(&timePerBlock, &occurencePerBlock);
	tall = clock() - tall;
	result->setTime((float)tall / CLOCKS_PER_SEC);
	
}

void ADMM::updateP0(const StudyCase& cas)
{
	// not used for this method
}

void ADMM::init(const Simparam& sim, const StudyCase& cas)
{
	// not used for this method
}


void ADMM::updateBt1(MatrixCPU* Bt1, MatrixCPU* trade, float rho, MatrixCPU* LAMBDA)
{
	Bt1->set(trade);
	Bt1->subtractTrans(trade);
	Bt1->multiply(0.5*rho); 
	Bt1->subtract(LAMBDA);
	Bt1->divide(rho);

}

void ADMM::updateBt2(MatrixCPU* Bt2, MatrixCPU* Tlocal, MatrixCPU* Tmoy, MatrixCPU* P, MatrixCPU* MU)
{
	Bt2->set(Tlocal);
	Bt2->subtractVector(Tmoy);
	Bt2->addVector(P);
	Bt2->subtractVector(MU);
}

void ADMM::updateBp1(MatrixCPU* Bp1, MatrixCPU* MU, MatrixCPU* Tmoy)
{
	Bp1->add(MU, Tmoy);
}

void ADMM::updateTl(MatrixCPU* Tlocal, float at1, float at2, MatrixCPU* Bt1, MatrixCPU*Bt2, MatrixCPU* Ct, MatrixCPU* matLb, MatrixCPU* matUb)
{

	float ada = at1 / at2; 
	float apa = at1 + at2;

	Tlocal->set(Bt1);
	Tlocal->multiply(ada);
	Tlocal->add(Bt2);
	Tlocal->multiply(at2);

	Tlocal->subtract(Ct);
	Tlocal->divide(apa); 
	Tlocal->project(matLb, matUb);
}

float ADMM::calcRes( MatrixCPU* Tlocal, MatrixCPU* Tlocal_pre, MatrixCPU* Tmoy, MatrixCPU* P)
{
	MatrixCPU temp(*Tlocal);
	temp.subtract(Tlocal_pre);

	MatrixCPU temp2(*Tmoy);
	temp2.subtract(P);
	float d1 = temp.max2();
	float d2 = temp2.max2();

	return d1 * (d1 > d2) + d2 * (d2 >= d1);
}

void ADMM::updateP(MatrixCPU* P, MatrixCPU* Ap1, MatrixCPU* Ap12, MatrixCPU* Bp1, MatrixCPU* Cp, MatrixCPU* Pmin, MatrixCPU* Pmax)
{
	P->multiplyT(Ap1, Bp1);
	P->subtract(Cp);
	
	P->divideT(Ap12);
	P->project(Pmin, Pmax);
}

void ADMM::updateMU(MatrixCPU* MU, MatrixCPU* Tmoy, MatrixCPU* P)
{
	MU->add(Tmoy);
	MU->subtract(P);
}

void ADMM::display() {

	std::cout << _name << std::endl;
}
