#include "../head/ADMMGPU3.cuh"

ADMMGPU3::ADMMGPU3() : Method()
{
#if DEBUG_CONSTRUCTOR
	std::cout << " ADMMGPU3 constructor" << std::endl;
#endif // DEBUG_CONSTRUCTOR
	_name = NAME;
}


ADMMGPU3::ADMMGPU3(float rho) : Method()
{
#if DEBUG_CONSTRUCTOR
	std::cout << "default ADMMGPU3 constructor" << std::endl;
#endif // DEBUG_CONSTRUCTOR
	_name = NAME;
	_rho = rho;
}

ADMMGPU3::~ADMMGPU3()
{
}

void ADMMGPU3::setParam(float rho)
{
	_rho = rho;
}

void ADMMGPU3::setTau(float tau)
{
	if (tau < 1) {
		throw std::invalid_argument("tau must be greater than 1");
	}
	_tau = tau;
}

void ADMMGPU3::solve(Simparam* result, const Simparam& sim, const StudyCase& cas)
{
#ifdef DEBUG_SOLVE
	cas.display();
	sim.display(1);
#endif // DEBUG_SOLVE
	clock_t tall = clock();
	std::chrono::high_resolution_clock::time_point t1;
	std::chrono::high_resolution_clock::time_point t2;
	// FB 0
	if (_id == 0) {
		//cudaDeviceSynchronize();
		t1 = std::chrono::high_resolution_clock::now();
		init(sim, cas);
		t2 = std::chrono::high_resolution_clock::now();
		timePerBlock.set(0, 0, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
		occurencePerBlock.set(0, 0, 1);
	}
	_rhog = sim.getRho();
	_at1 = _rhog; //  2*a
	

	int iterG = sim.getIterG();
	int iterL = sim.getIterL();
	float epsG = sim.getEpsG();
	float epsL = sim.getEpsL();
	int stepL = sim.getStepL();
	int stepG = sim.getStepG();


	float resG = 2 * epsG;
	float resL = 2 * epsL;
	int iterGlobal = 0;
	int iterLocal = 0;

	
	MatrixCPU resF(2, (iterG / stepG) + 1);
	

	while ((iterGlobal < iterG) && (resG>epsG)) {
		resL = 2 * epsL;
		iterLocal = 0;
		// FB 1
		
		while (iterLocal< iterL && resL>epsL) {
			// FB 2
			updateLocalProbGPU(&Tlocal,&P);		
			if (!(iterLocal % stepL)) { // FB 3
				//cudaDeviceSynchronize();
				t1 = std::chrono::high_resolution_clock::now();
				resL = calcRes(&Tlocal,&P, &tempN1, &tempNN); 
				//cudaDeviceSynchronize();
				t2 = std::chrono::high_resolution_clock::now();
				timePerBlock.increment(0, 4, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
			}
			Tlocal.swap(&Tlocal_pre); 
			iterLocal++;
		}
		occurencePerBlock.increment(0, 1, iterLocal);
		occurencePerBlock.increment(0, 2, iterLocal);
		occurencePerBlock.increment(0, 4, iterLocal / stepL);
		Tlocal.swap(&Tlocal_pre); 
		trade.swap(&Tlocal);
		// FB 3
		//cudaDeviceSynchronize();
		t1 = std::chrono::high_resolution_clock::now();
		updateGlobalProbGPU();
		//cudaDeviceSynchronize();
		t2 = std::chrono::high_resolution_clock::now();
		timePerBlock.set(0, 5, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
		if (!(iterGlobal % stepG)) { // FB 4
			//cudaDeviceSynchronize();
			t1 = std::chrono::high_resolution_clock::now();
			resG=updateRes(&resF, &Tlocal, iterGlobal/stepG, &tempNN);
			//cudaDeviceSynchronize();
			t2 = std::chrono::high_resolution_clock::now();
			timePerBlock.increment(0, 6, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
		}
		
		
		iterGlobal++;
	}
	
	occurencePerBlock.increment(0, 5, iterGlobal);
	occurencePerBlock.increment(0, 6, iterGlobal / stepG);

	// FB 5
	//cudaDeviceSynchronize();
	t1 = std::chrono::high_resolution_clock::now();
	updatePnGPU << <_numBlocks, _blockSize >> > (Pn._matrixGPU, Tmoy._matrixGPU, nVoisin._matrixGPU, _n);
	
	float fc = calcFc(&a, &b, &trade, &Pn, &GAMMA,&tempN1,&tempNN);
	
	


	MatrixCPU LAMBDACPU;
	LAMBDA.toMatCPU(LAMBDACPU);
	MatrixCPU tradeCPU;
	trade.toMatCPU(tradeCPU);
	MatrixCPU PnCPU;
	Pn.toMatCPU(PnCPU);


	result->setResF(&resF);
	result->setLAMBDA(&LAMBDACPU);
	result->setTrade(&tradeCPU);
	result->setIter(iterGlobal);
	result->setRho(_rhog);
	result->setPn(&PnCPU);
	
	result->setFc(fc);
	//cudaDeviceSynchronize();
	t2 = std::chrono::high_resolution_clock::now();
	timePerBlock.increment(0, 7, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
	occurencePerBlock.increment(0, 7, 1);

	result->setTimeBloc(&timePerBlock, &occurencePerBlock);
	tall = clock() - tall;
	result->setTime((float)tall / CLOCKS_PER_SEC);
	
}
void ADMMGPU3::updateP0(const StudyCase& cas)
{
	_id = _id + 1;
	
	//cudaDeviceSynchronize();
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	b.transferCPU();
	matLb.transferCPU();
	Pmin.transferCPU();
	Pmax.transferCPU();
	Cp.transferCPU();
	nVoisin.transferCPU();

	b = cas.getb();
	Pmin = MatrixGPU(cas.getPmin());
	Pmax = MatrixGPU(cas.getPmax());


	MatrixGPU Lb(cas.getLb());

	Cp = MatrixGPU(b);

	for (int i = 0; i < _n; i++)
	{
		for (int j = 0; j < _n;j++) {
			matLb.set(i, j, Lb.get(i, 0));
		}
	}

	nVoisin.transferGPU();
	b.transferGPU();
	matLb.transferGPU();
	Pmin.transferGPU();
	Pmax.transferGPU();
	Cp.transferGPU();

	matLb.multiplyT(&connect);
	Pmin.divideT(&nVoisin);
	Pmax.divideT(&nVoisin);
	Cp.multiplyT(&nVoisin);
	//cudaDeviceSynchronize();
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	timePerBlock.increment(0, 8, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
	occurencePerBlock.increment(0, 8, 1);



}

void ADMMGPU3::init(const Simparam& sim, const StudyCase& cas)
{
	clock_t t =clock();
	
	_rhog = sim.getRho();
	int nAgent = sim.getNAgent();
	_n = nAgent;
	float rho_p = _rho * _n;
	if (_rho == 0) {
		rho_p = _rhog;
	}
	_numBlocks = ceil((_n + _blockSize - 1) / _blockSize);
	_numBlocksM = ceil((_n*_n + _blockSize - 1) / _blockSize);
	GAMMA = MatrixGPU(cas.getG());
	connect = cas.getC();
	a = cas.geta();
	b = cas.getb();

	Pmin = MatrixGPU(cas.getPmin());
	Pmax = MatrixGPU(cas.getPmax());
	nVoisin = MatrixGPU(cas.getNvoi());
	MatrixGPU Ub(cas.getUb());
	MatrixGPU Lb(cas.getLb());
	matUb = MatrixGPU(nAgent, nAgent);
	matLb = MatrixGPU(nAgent, nAgent);
	for (int i = 0; i < nAgent; i++)
	{
		for (int j = 0; j < nAgent;j++) {
			matUb.set(i, j, Ub.get(i, 0));
			matLb.set(i, j, Lb.get(i, 0));
		}
	}
	
	LAMBDA = MatrixGPU(sim.getLambda());
	trade = MatrixGPU(sim.getTrade());

	MU = sim.getMU(); //  lambda_l/_rho
	
	Tlocal_pre = MatrixGPU(sim.getTrade());
	Tmoy = sim.getPn();
	

	
	_at1 = _rhog; //  2*a
	_at2 = rho_p;

	Ap2 = MatrixGPU(a);
	Ap1 = MatrixGPU(nVoisin);
	Ap12 = MatrixGPU(nAgent, 1);


	Bt1 = MatrixGPU(nAgent, nAgent);
	Ct = MatrixGPU(nAgent, nAgent);
	Cp = MatrixGPU(b);


	Tlocal = MatrixGPU(_n, _n, 0, 1);
	P = MatrixGPU(_n, 1, 0, 1); // trades mean
	Pn = MatrixGPU(_n, 1, 0, 1); // trades sum

	tempNN = MatrixGPU(_n, _n, 0, 1); 
	tempN1 = MatrixGPU(_n, 1, 0, 1); 
	tempNN.preallocateReduction();
	tempN1.preallocateReduction();

	
	GAMMA.transferGPU();
	connect.transferGPU();
	a.transferGPU();
	b.transferGPU();
	matUb.transferGPU();
	matLb.transferGPU();
	Pmin.transferGPU();
	Pmax.transferGPU();
	nVoisin.transferGPU();

	LAMBDA.transferGPU();
	trade.transferGPU();
	MU.transferGPU();
	Tlocal_pre.transferGPU();
	Tmoy.transferGPU();

	Ap2.transferGPU();
	Ap1.transferGPU();
	Ap12.transferGPU();
	Bt1.transferGPU();
	Ct.transferGPU();
	Cp.transferGPU();

	matUb.multiplyT(&connect);
	matLb.multiplyT(&connect);
	Pmin.divideT(&nVoisin);
	Pmax.divideT(&nVoisin);


	Ap2.multiplyT(&a, &nVoisin);
	Ap2.multiplyT(&nVoisin);
	Ap1.multiply(rho_p);
	Ct.multiplyT(&GAMMA, &connect);
	Cp.multiplyT(&nVoisin);
	Tmoy.divideT(&nVoisin);
	updateGlobalProbGPU();

}



void ADMMGPU3::updateGlobalProbGPU() {
	updateLAMBDAGPU <<<_numBlocksM, _blockSize >>> (LAMBDA._matrixGPU, trade._matrixGPU, _rhog, _n);
	updateBt1GPU <<<_numBlocksM, _blockSize >>> (Bt1._matrixGPU,trade._matrixGPU, _rhog, LAMBDA._matrixGPU, _n);
}

void ADMMGPU3::updateLocalProbGPU(MatrixGPU* Tlocal, MatrixGPU* P) {
	
	//cudaDeviceSynchronize();
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	updateTrade2 << <_numBlocksM, _blockSize >> > (Tlocal->_matrixGPU, Tlocal_pre._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, MU._matrixGPU, _at1, _at2, Bt1._matrixGPU, Ct._matrixGPU, matLb._matrixGPU, matUb._matrixGPU, _n);
	//cudaDeviceSynchronize();
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	timePerBlock.increment(0, 1, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());

	// Fb1b
	//cudaDeviceSynchronize();
	t1 = std::chrono::high_resolution_clock::now();
	updatePGPU2 << <_numBlocks, _blockSize >> > (Tlocal->_matrixGPU, nVoisin._matrixGPU, MU._matrixGPU, Tmoy._matrixGPU, P->_matrixGPU, Ap1._matrixGPU, Ap2._matrixGPU, Cp._matrixGPU, Pmin._matrixGPU, Pmax._matrixGPU, _n);
	//cudaDeviceSynchronize();
	t2 = std::chrono::high_resolution_clock::now();
	timePerBlock.increment(0, 2, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
	
}



 float ADMMGPU3::calcRes( MatrixGPU* Tlocal, MatrixGPU* P, MatrixGPU* tempN1, MatrixGPU* tempNN)
{
	 tempNN->subtract(Tlocal, &Tlocal_pre);
	 tempN1->subtract(&Tmoy, P);

	 float d1 = tempN1->max2();
	 float d2 = tempNN->max2();
	 return d1* (d1 > d2) + d2 * (d2 >= d1);
}

 float ADMMGPU3::updateRes(MatrixCPU* res, MatrixGPU* Tlocal, int iter, MatrixGPU* tempNN)
 {
	 tempNN->subtract(Tlocal, &trade);
	 float resS = tempNN->max2();
	 tempNN->set(Tlocal);
	 tempNN->addTrans(Tlocal);
	 float resR = tempNN->max2();
	 res->set(0, iter, resR);
	 res->set(1, iter, resS);

	 if (resR > _mu * resS) {
		 _rhog = _tau * _rhog;
		 _at1 = _rhog;
		 
	 }
	 else if (resS > _mu * resR) {
		 _rhog = _rhog / _tau;
		 _at1 = _rhog;
	 }

	 return resR * (resR > resS) + resS * (resR <= resS);
 }




void ADMMGPU3::display() {

	std::cout << _name << std::endl;
}


