
#include "../head/KernelFunction.cuh"

__global__ void updateLAMBDAGPU(float* LAMBDA, float* trade, float rho, int const n) { // ne marche que pour matrix carr�e
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;
	for (int l = index; l < (n * n); l += step)
	{
		float m = LAMBDA[l];
		int i = l / n;
		int j = l % n;
		int k = i + j * n;
		LAMBDA[l] = m + 0.5 * rho * (trade[l] + trade[k]);
	}

}
__global__ void updateBt1GPU(float* Bt1, float* trade, float rho, float* LAMBDA, int const n)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;
	for (int l = index; l < (n * n); l += step)
	{
		int i = l / n;
		int j = l % n;
		int k = i + j * n;
		Bt1[l] = 0.5 * (trade[l] - trade[k]) - LAMBDA[l] / rho;
	}
}

__global__ void updateTrade(float* Bt2, float* Tlocal, float* Tlocal_pre, float* Tmoy, float* P, float* MU, float at1, float at2, float* Bt1, float* Ct, float* matLb, float* matUb, int const n) {
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;
	for (int i = index; i < n * n; i += step)
	{
		int k = i / n; // division enti�re
		Bt2[i] = Tlocal_pre[i] - Tmoy[k] + P[k] - MU[k];

		float r = (Bt1[i] * at1 + Bt2[i] * at2 - Ct[i]) / (at1 + at2);
		float ub = matUb[i];
		float lb = matLb[i];
		Tlocal[i] = (ub - r) * (r > ub) + (lb - r) * (r < lb) + r;
	}

}
__global__ void updatePGPU(float* Tlocal, float* nVoisin, float* MU, float* Tmoy, float* P, float* Ap1, float* Ap2, float* Bp1, float* Cp, float* matLb, float* matUb, int const n) {
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;

	for (int i = index; i < n; i += step)
	{
		float s = 0.0;
		for (int j = 0; j < n; j++)
		{
			s += Tlocal[i * n + j];
		}
		float r = s / nVoisin[i];
		Tmoy[i] = r;
		Bp1[i] = r + MU[i];
		float p = (Ap1[i] * Bp1[i] - Cp[i]) / (Ap1[i] + Ap2[i]);
		float ub = matUb[i];
		float lb = matLb[i];
		p = (ub - p) * (p > ub) + (lb - p) * (p < lb) + p;
		P[i] = p;
		MU[i] = MU[i] + r - p;
	}
}

// sans stockage de la valeur interm�diaire
__global__ void updateTrade2(float* Tlocal, float* Tlocal_pre, float* Tmoy, float* P, float* MU, float at1, float at2, float* Bt1, float* Ct, float* matLb, float* matUb, int const n) {
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;
	for (int i = index; i < n * n; i += step)
	{
		int k = i / n;
		float m = Tlocal_pre[i] - Tmoy[k] + P[k] - MU[k];

		float r = (Bt1[i] * at1 + m * at2 - Ct[i]) / (at1 + at2);
		float ub = matUb[i];
		float lb = matLb[i];
		Tlocal[i] = (ub - r) * (r > ub) + (lb - r) * (r < lb) + r;
	}

}
__global__ void updatePGPU2(float* Tlocal, float* nVoisin, float* MU, float* Tmoy, float* P, float* Ap1, float* Ap2, float* Cp, float* matLb, float* matUb, int const n) {
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;

	for (int i = index; i < n; i += step)
	{
		float s = 0.0;
		for (int j = 0; j < n; j++)
		{
			s += Tlocal[i * n + j];
		}
		float r = s / nVoisin[i];
		Tmoy[i] = r;
		float b = r + MU[i];
		float p = (Ap1[i] * b - Cp[i]) / (Ap1[i] + Ap2[i]);
		float ub = matUb[i];
		float lb = matLb[i];
		p = (ub - p) * (p > ub) + (lb - p) * (p < lb) + p;
		P[i] = p;
		MU[i] = MU[i] + r - p;
	}
}

// lin�arisation des calculs
__global__ void updateLAMBDAGPULin(float* LAMBDALin, float* tradeLin, float rho, float* CoresLinAgent, float* CoresLinVoisin, float* CoresMatLin, int const n, int const N)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;
	for (int l = index; l < N; l += step)
	{
		float m = LAMBDALin[l];
		int i = CoresLinAgent[l];
		int j = CoresLinVoisin[l];
		int k = CoresMatLin[j * n + i];
		LAMBDALin[l] = m + 0.5 * rho * (tradeLin[l] + tradeLin[k]);
	}
}
__global__ void updateBt1GPULin(float* Bt1, float* tradeLin, float rho, float* LAMBDA, float* CoresLinAgent, float* CoresLinVoisin, float* CoresMatLin, int const n, int const N)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;
	for (int l = index; l < N; l += step)
	{
		int i = CoresLinAgent[l];
		int j = CoresLinVoisin[l];
		int k = CoresMatLin[j * n + i];
		Bt1[l] = 0.5 * (tradeLin[l] - tradeLin[k]) - LAMBDA[l] / rho;
	}
}
__global__ void updateTradeLin(float* Tlocal, float* Tlocal_pre, float* Tmoy, float* P, float* MU, float at1, float at2, float* Bt1, float* Ct, float* matLb, float* matUb, float* CoresLinAgent, int const N) {
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;
	for (int i = index; i < N; i += step)
	{
		int k = CoresLinAgent[i];
		float m = Tlocal_pre[i] - Tmoy[k] + P[k] - MU[k];
		float r = (Bt1[i] * at1 + m * at2 - Ct[i]) / (at1 + at2);
		float ub = matUb[i];
		float lb = matLb[i];
		Tlocal[i] = (ub - r) * (r > ub) + (lb - r) * (r < lb) + r;
	}

}
__global__ void updatePGPULin(float* Tlocal, float* nVoisin, float* MU, float* Tmoy, float* P, float* Ap1, float* Ap12, float* Cp, float* matLb, float* matUb, float* CoresAgentLin, int const n) {
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;

	for (int i = index; i < n; i += step)
	{
		float s = 0.0;
		for (int j = CoresAgentLin[i]; j < CoresAgentLin[i + 1]; j++)
		{
			s += Tlocal[j];
		}
		float m = s / nVoisin[i];
		Tmoy[i] = m;
		float b = m + MU[i];
		float p = (Ap1[i] * b - Cp[i]) / (Ap12[i]);
		float ub = matUb[i];
		float lb = matLb[i];
		p = (ub - p) * (p > ub) + (lb - p) * (p < lb) + p;

		P[i] = p;
		MU[i] = MU[i] + m - p;
	}
}
__global__ void updateDiffGPU(float* tempNN, float* Tlocal, float* CoresLinAgent, float* CoresLinVoisin, float* CoresMatLin, int const n, int const N)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;
	for (int l = index; l < N; l += step)
	{
		int i = CoresLinAgent[l];
		int j = CoresLinVoisin[l];
		int k = CoresMatLin[j * n + i];
		tempNN[l] = (Tlocal[l] + Tlocal[k]);
	}
}
__global__ void updatePnGPU(float* Pn, float* Tmoy, float* nVoisin, const int nAgent)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;
	for (int n = index; n < nAgent; n += step)
	{
		Pn[n] = Tmoy[n] * nVoisin[n];
	}

}


// ajout de la corespondance indice-> transpos�, et meilleure reduction
__global__ void updateLAMBDAGPULin2(float* LAMBDALin, float* tradeLin, float rho, float* CoresLinTrans, int const N)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;
	float rhoLocal = rho;
	for (int l = index; l < N; l += step)
	{
		float m = LAMBDALin[l];
		int k = CoresLinTrans[l];
		LAMBDALin[l] = m + 0.5 * rhoLocal * (tradeLin[l] + tradeLin[k]);
	}
}
__global__ void updateBt1GPULin2(float* Bt1, float* tradeLin, float rho, float* LAMBDA, float* CoresLinTrans, int const N)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;
	float rhoLocal = rho;
	for (int l = index; l < N; l += step)
	{
		int k = CoresLinTrans[l];
		Bt1[l] = 0.5 * (tradeLin[l] - tradeLin[k]) - LAMBDA[l] / rhoLocal;
	}
}

__global__ void updatePGPULin2(float* MU, float* Tmoy, float* P, float* Ap1, float* Ap12, float* Cp, float* matLb, float* matUb, int const n) {
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;

	for (int i = index; i < n; i += step)
	{
		float MULocal = MU[i];
		float TmoyLocal = Tmoy[i];
		float b = TmoyLocal + MULocal;
		float p = (Ap1[i] * b - Cp[i]) / (Ap12[i]);
		float ub = matUb[i];
		float lb = matLb[i];
		p = (ub - p) * (p > ub) + (lb - p) * (p < lb) + p;
		P[i] = p;
		MU[i] = MULocal + TmoyLocal - p;
	}
}

__global__ void updateDiffGPU2(float* tempNN, float* Tlocal, float* CoresLinTrans, int const N)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;
	for (int l = index; l < N; l += step)
	{
		int k = CoresLinTrans[l];
		tempNN[l] = (Tlocal[l] + Tlocal[k]);
	}
}

__global__ void symetriseGPU(float* out, float* tradeLin, float* CoresLinTrans, int const N) {
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;
	for (int l = index; l < N; l += step)
	{
		int k = CoresLinTrans[l];
		out[l] = (tradeLin[l] - tradeLin[k]) / 2;
	}
}

// regroupement des calculs
__global__ void updateLAMBDABt1GPU(float* Bt1, float* LAMBDALin, float* tradeLin, float rho, float* CoresLinTrans, int const N)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;
	float rhoLocal = rho;
	for (int l = index; l < N; l += step)
	{
		float m = LAMBDALin[l];
		int k = CoresLinTrans[l];
		float tradeT = tradeLin[k];
		LAMBDALin[l] = m + 0.5 * rhoLocal * (tradeLin[l] + tradeT);
		Bt1[l] = -tradeT - m / rhoLocal;
	}
}
