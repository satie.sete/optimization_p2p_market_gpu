#include "../head/Method.h"


Method::Method()
{
#if DEBUG_CONSTRUCTOR
	std::cout << "method constructor" << std::endl;
#endif // DEBUG_CONSTRUCTOR
	
}

Method::~Method()
{
	#if DEBUG_DESTRUCTOR
		std::cout << "method destructor" << std::endl;
	#endif
}

void Method::updateLAMBDA(MatrixCPU* LAMBDA, MatrixCPU* trade, float rho)
{
	MatrixCPU times = MatrixCPU(*trade);
	times.addTrans(trade);
	times.multiply(rho);
	times.multiply(0.5);
	LAMBDA->add(LAMBDA, &times);
}

float Method::updateRes(MatrixCPU* res, MatrixCPU* Tlocal, MatrixCPU* trade, int iter)
{
	MatrixCPU temp(*Tlocal);
	temp.addTrans(Tlocal);

	MatrixCPU temp2(*Tlocal);
	float resR = temp.max2();
	temp2.subtract(trade);
	float resS = temp2.max2();

	res->set(0, iter, resR);
	res->set(1, iter, resS);

	return resR * (resR > resS) + resS * (resR <= resS);

}

float Method::calcFc(MatrixCPU* cost1, MatrixCPU* cost2, MatrixCPU* trade, MatrixCPU* Pn, MatrixCPU* GAMMA, MatrixCPU* tempN1, MatrixCPU* tempNN)
{

	float fc = 0;
	tempN1->set(cost1);
	tempN1->multiply(0.5);
	tempN1->multiplyT(Pn);
	tempN1->add(cost2);
	tempN1->multiplyT(Pn);
	
	fc = fc + tempN1->sum();
	tempNN->set(trade);
	tempNN->multiplyT(GAMMA);
	

	fc = fc + tempNN->sum();

	return fc;

}


void Method::updateLAMBDA(MatrixGPU* LAMBDA, MatrixGPU* trade, float rho, MatrixGPU* tempNN)
{
	tempNN->set(trade);
	tempNN->addTrans(trade);
	tempNN->multiply(rho);
	tempNN->multiply(0.5);
	LAMBDA->add(LAMBDA, tempNN);
}

float Method::updateRes(MatrixCPU* res, MatrixGPU* Tlocal, MatrixGPU* trade, int iter, MatrixGPU* tempNN)
{
	tempNN->subtract(Tlocal, trade);
	
	float resS = tempNN->max2();
	tempNN->set(Tlocal);
	tempNN->addTrans(Tlocal);
	float resR = tempNN->max2();
	

	res->set(0, iter, resR);
	res->set(1, iter, resS);
	

	return resR* (resR > resS) + resS * (resR <= resS);
}

float Method::calcFc(MatrixGPU* cost1, MatrixGPU* cost2, MatrixGPU* trade, MatrixGPU* Pn, MatrixGPU* GAMMA, MatrixGPU* tempN1, MatrixGPU* tempNN)
{
	
	tempN1->set(cost1);
	tempN1->multiply(0.5);
	tempN1->multiplyT(Pn);
	tempN1->add(cost2);
	tempN1->multiplyT(Pn);
	float fc = tempN1->sum();
	

	tempNN->set(trade);
	tempNN->multiplyT(GAMMA);
	fc = fc + tempNN->sum();
	
	return fc;

}

void Method::updatePn(MatrixCPU* Pn, MatrixCPU* trade)
{
	Pn->sum(trade);
}
void Method::updatePn(MatrixGPU* Pn, MatrixGPU* trade)
{
	Pn->sum(trade);
}

void Method::resetId()
{
	_id = 0;
}

















