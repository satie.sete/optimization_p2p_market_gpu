#include "../head/ADMMGPU.cuh"

ADMMGPU::ADMMGPU() : Method()
{
#if DEBUG_CONSTRUCTOR
	std::cout << "ADMMGPU Constructor " << std::endl;
#endif // DEBUG_CONSTRUCTOR
	_name = NAME;
}


ADMMGPU::ADMMGPU(float rho) : Method()
{
#if DEBUG_CONSTRUCTOR
	std::cout << " default ADMMGPU Constructor " << std::endl;
#endif // DEBUG_CONSTRUCTOR
	_name = NAME;
	_rho = rho;
}

ADMMGPU::~ADMMGPU()
{
}

void ADMMGPU::setParam(float rho)
{
	_rho = rho;
}

void ADMMGPU::setTau(float tau)
{
	throw std::domain_error("tau is not define for this method");
}

void ADMMGPU::solve(Simparam* result, const Simparam& sim, const StudyCase& cas)
{
#ifdef DEBUG_SOLVE
	cas.display();
	sim.display(1);
#endif // DEBUG_SOLVE

	clock_t tall = clock();
	
	// initialization FB0
	//cudaDeviceSynchronize();
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

	float rho = sim.getRho();
	int iterG = sim.getIterG();
	int iterL = sim.getIterL();
	float epsG = sim.getEpsG();
	float epsL = sim.getEpsL();
	int nAgent = sim.getNAgent();
	int stepL = sim.getStepL();
	int stepG = sim.getStepG();
	_n = nAgent;
	_numBlocks = ceil((_n + _blockSize - 1) / _blockSize);
	_numBlocksM = ceil((_n*_n + _blockSize - 1) / _blockSize);

	MatrixGPU GAMMA(cas.getG());
	MatrixGPU connect(cas.getC());
	MatrixGPU a(cas.geta());
	MatrixGPU b(cas.getb());
	MatrixGPU Ub(cas.getUb());
	MatrixGPU Lb(cas.getLb());
	MatrixGPU matUb(nAgent, nAgent);
	MatrixGPU matLb(nAgent, nAgent);
	for (int i = 0; i < nAgent; i++)
	{
		for (int j = 0; j < nAgent;j++) {
			matUb.set(i, j, Ub.get(i, 0));
			matLb.set(i, j, Lb.get(i, 0));
		}
	}
	
	MatrixGPU Pmin(cas.getPmin());
	MatrixGPU Pmax(cas.getPmax());
	MatrixGPU nVoisin(cas.getNvoi());

	MatrixGPU LAMBDA(sim.getLambda());
	MatrixGPU trade(sim.getTrade());
	MatrixGPU Pn(sim.getPn()); // sum of trades for each agent
	MatrixCPU resF(2, iterG/stepG+1);

	float fc = 0;


	MatrixGPU MU(sim.getMU()); //  lambda_l/_rho
	MatrixGPU Tlocal(nAgent, nAgent);
	MatrixGPU Tlocal_pre(sim.getTrade());
	MatrixGPU Tmoy(nAgent, 1);
	MatrixGPU P(nAgent, 1); //  trades means
	
	
	// initialisation des paramètres constants
	float rho_p = _rho * nAgent;
	if (_rho == 0) {
		rho_p = rho;
	}

	float at1 = rho; // 2*a in the article
	float at2 = rho_p;

	MatrixGPU Ap2(nAgent, 1);
	MatrixGPU Ap1(nVoisin);
	MatrixGPU Ap12(nAgent, 1); // Ap2+Ap1;
	MatrixGPU Bt1(nAgent, nAgent);
	MatrixGPU Bt2(nAgent, nAgent);
	MatrixGPU Bp1(nAgent, 1);
	MatrixGPU Ct(nAgent, nAgent);
	MatrixGPU Cp(b);

	MatrixGPU tempNN(nAgent, nAgent); // matrix for intermediate calculations
	tempNN.preallocateReduction();
	MatrixGPU tempN1(nAgent, 1); // to prevent from reallocating memory
	tempN1.preallocateReduction();

	GAMMA.transferGPU();
	connect.transferGPU();
	a.transferGPU();
	b.transferGPU();
	matUb.transferGPU();
	matLb.transferGPU();
	Pmin.transferGPU();
	Pmax.transferGPU();
	nVoisin.transferGPU();

	LAMBDA.transferGPU();
	trade.transferGPU();
	MU.transferGPU();
	Tlocal.transferGPU();
	Tlocal_pre.transferGPU();
	Tmoy.transferGPU();
	P.transferGPU();
	Pn.transferGPU();

	Ap2.transferGPU();
	Ap1.transferGPU();
	Ap12.transferGPU();
	Bt1.transferGPU();
	Bt2.transferGPU();
	Bp1.transferGPU();
	Ct.transferGPU();
	Cp.transferGPU();

	tempN1.transferGPU();
	tempNN.transferGPU();
	

	matUb.multiplyT(&connect);
	matLb.multiplyT(&connect);
	Pmin.divideT(&nVoisin);
	Pmax.divideT(&nVoisin);


	Ap2.multiplyT(&a, &nVoisin);
	Ap2.multiplyT(&nVoisin);
	Ap1.multiply(rho_p);
	Ct.multiplyT(&GAMMA, &connect);
	Cp.multiplyT(&nVoisin);
	Tmoy.Moy(&Tlocal, &nVoisin);
	updateGlobalProbGPU(&LAMBDA, &Bt1, &trade, rho);
	//cudaDeviceSynchronize();
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	timePerBlock.increment(0, 0, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
	occurencePerBlock.increment(0, 0, 1);

	float resG = 2 * epsG;
	float resL = 2 * epsL;
	int iterGlobal = 0;
	int iterLocal = 0;
	while ((iterGlobal < iterG) && (resG>epsG)) {
		resL = 2 * epsL;
		iterLocal = 0;
		while (iterLocal< iterL && resL>epsL) {
			// FB 1
			updateLocalProbGPU(&Bt2, &Tlocal, &Tmoy, &Bp1, &P, &MU, at1, at2, &Bt1, &Ct, &matLb, &matUb, &nVoisin, &Ap1, &Ap2, &Ap12, &Cp, &Pmin, &Pmax, &Tlocal_pre);		
			if (!(iterLocal % stepL)) { // FB 2
				//cudaDeviceSynchronize();
				t1 = std::chrono::high_resolution_clock::now();
				resL = calcRes(&Tlocal,&Tlocal_pre,&Tmoy,&P, &tempN1, &tempNN); 
				//cudaDeviceSynchronize();
				t2 = std::chrono::high_resolution_clock::now();
				timePerBlock.increment(0, 4, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
			}
			
			Tlocal.swap(&Tlocal_pre); 
			iterLocal++;
		}
		occurencePerBlock.increment(0, 1, iterLocal);
		occurencePerBlock.increment(0, 2, iterLocal);
		occurencePerBlock.increment(0, 4, iterLocal / stepL);
		Tlocal.swap(&Tlocal_pre); 
		trade.swap(&Tlocal);
		
		// FB 3
		//cudaDeviceSynchronize();
		t1 = std::chrono::high_resolution_clock::now();
		updateGlobalProbGPU(&LAMBDA, &Bt1, &trade, rho); 
		//cudaDeviceSynchronize();
		t2 = std::chrono::high_resolution_clock::now();
		timePerBlock.set(0, 5, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());

		if (!(iterGlobal % stepG)) {// FB 4
			//cudaDeviceSynchronize();
			t1 = std::chrono::high_resolution_clock::now();
			resG=updateRes(&resF, &Tlocal, &trade, iterGlobal/stepG, &tempNN);
			//cudaDeviceSynchronize();
			t2 = std::chrono::high_resolution_clock::now();
			timePerBlock.increment(0, 6, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
		}
			
		iterGlobal++;
	}
	occurencePerBlock.increment(0, 5, iterGlobal);
	occurencePerBlock.increment(0, 6, iterGlobal / stepG);

	// FB 5
	//cudaDeviceSynchronize();
	t1 = std::chrono::high_resolution_clock::now();

	updatePn(&Pn, &trade); 
	fc = calcFc(&a, &b, &trade, &Pn, &GAMMA,&tempN1,&tempNN);
	
	MatrixCPU LAMBDACPU;
	LAMBDA.toMatCPU(LAMBDACPU);
	MatrixCPU tradeCPU;
	trade.toMatCPU(tradeCPU);
	MatrixCPU PnCPU;
	Pn.toMatCPU(PnCPU);


	result->setResF(&resF);
	result->setLAMBDA(&LAMBDACPU);
	result->setTrade(&tradeCPU);
	result->setIter(iterGlobal);
	
	result->setPn(&PnCPU);
	
	result->setFc(fc);
	//cudaDeviceSynchronize();
	t2 = std::chrono::high_resolution_clock::now();
	timePerBlock.increment(0, 7, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
	occurencePerBlock.increment(0, 7, 1);

	result->setTimeBloc(&timePerBlock, &occurencePerBlock);
	tall = clock() - tall;
	result->setTime((float)tall / CLOCKS_PER_SEC);
	
}
void ADMMGPU::updateP0(const StudyCase& cas)
{
	// not used for this method
}

void ADMMGPU::init(const Simparam& sim, const StudyCase& cas)
{
	// not used for this method
}



void ADMMGPU::updateGlobalProbGPU(MatrixGPU* LAMBDA, MatrixGPU* Bt1, MatrixGPU* trade, float rho) {
	updateLAMBDAGPU <<<_numBlocksM, _blockSize >>> (LAMBDA->_matrixGPU, trade->_matrixGPU, rho, _n);
	updateBt1GPU <<<_numBlocksM, _blockSize >>> (Bt1->_matrixGPU,trade->_matrixGPU, rho, LAMBDA->_matrixGPU, _n);
}
void ADMMGPU::updateLocalProbGPU(MatrixGPU* Bt2, MatrixGPU* Tlocal, MatrixGPU* Tmoy, MatrixGPU* Bp1, MatrixGPU* P, MatrixGPU* MU, float at1, float at2, MatrixGPU* Bt1, MatrixGPU* Ct, MatrixGPU* matLb,
	MatrixGPU* matUb, MatrixGPU* nVoisin, MatrixGPU* Ap1, MatrixGPU* Ap2, MatrixGPU* Ap12, MatrixGPU* Cp, MatrixGPU* Pmin, MatrixGPU* Pmax, MatrixGPU* Tlocal_pre) {
	
	// Fb1a
	//cudaDeviceSynchronize();
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	updateTrade <<<_numBlocksM, _blockSize>>> (Bt2->_matrixGPU, Tlocal->_matrixGPU, Tlocal_pre->_matrixGPU, Tmoy->_matrixGPU, P->_matrixGPU, MU->_matrixGPU, at1, at2, Bt1->_matrixGPU, Ct->_matrixGPU, matLb->_matrixGPU, matUb->_matrixGPU, _n);
	//cudaDeviceSynchronize();
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();

	timePerBlock.increment(0, 1, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());

	// Fb1b
	//cudaDeviceSynchronize();
	t1 = std::chrono::high_resolution_clock::now();
	updatePGPU <<<_numBlocks, _blockSize >>> (Tlocal->_matrixGPU, nVoisin->_matrixGPU, MU->_matrixGPU, Tmoy->_matrixGPU, P->_matrixGPU, Ap1->_matrixGPU, Ap2->_matrixGPU, Bp1->_matrixGPU, Cp->_matrixGPU, Pmin->_matrixGPU, Pmax->_matrixGPU, _n); 
	//cudaDeviceSynchronize();
	t2 = std::chrono::high_resolution_clock::now();
	timePerBlock.increment(0, 2, std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
}



 float ADMMGPU::calcRes( MatrixGPU* Tlocal, MatrixGPU* Tlocal_pre, MatrixGPU* Tmoy, MatrixGPU* P, MatrixGPU* tempN1, MatrixGPU* tempNN)
{
	 tempNN->subtract(Tlocal, Tlocal_pre);
	 tempN1->subtract(Tmoy, P);

	 float d1 = tempN1->max2();
	 float d2 = tempNN->max2();
	

	 return d1* (d1 > d2) + d2 * (d2 >= d1);
}





void ADMMGPU::display() {

	std::cout << _name << std::endl;
}


