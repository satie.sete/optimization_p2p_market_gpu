#include "../head/StudyCase.h"


void StudyCase::setMatFromFile(const std::string& path, const std::string& date, MatrixCPU* Pgen, MatrixCPU* P0, MatrixCPU* costGen)
{
	std::string namePgen = "/PowerMaxCountry.csv";
	std::string nameP0 = "/load/Country_" + date + ".txt";
	std::string namecostGen = "/CoefPoly.csv";
	Pgen->setFromFile(path + namePgen);
	P0->setFromFile(path + nameP0,1);
	costGen->setFromFile(path + namecostGen);

}

void StudyCase::setGenFromFile(const std::string& path, MatrixCPU* Pgen, MatrixCPU* costGen)
{
	std::string namePgen = "/PowerMaxCountry.csv";
	std::string namecostGen = "/CoefPoly.csv";
	Pgen->setFromFile(path + namePgen);
	costGen->setFromFile(path + namecostGen);

}

float StudyCase::rand1() const
{
	float a = (float)(rand()) / ((float)(RAND_MAX));
	return a;
}


void StudyCase::genConnec(MatrixCPU* connec, int nCons,int nGen, int nPro)
{
	
	int nAgent = nPro + nGen + nCons;
#ifdef DEBUG_CONSTRUCTOR
	std::cout << " nPro =" << nPro << " nGen =" << nGen << " nCons =" << nCons << " nAgent =" << nAgent << std::endl;
	std::cout << " row =" << connec->getNLin() << " column =" << connec->getNCol() << std::endl;
#endif
	
	for (int i = 0; i < nCons;i++) {
		for (int j = nCons; j < nAgent;j++) {
			connec->set(i, j, 1);
		}
	}
	for (int i = nCons; i < nCons+nGen; i++) {
		for (int j = 0; j < nCons;j++) {
			connec->set(i, j, 1);
		}
		for (int j = nCons + nGen; j < nAgent;j++) {
			connec->set(i, j, 1);
		}
	}
	for (int i = nCons + nGen; i < nAgent;i++) {
		for (int j = 0; j < nCons; j++) {
			connec->set(i, j, 1);
		}
		for (int j = nCons; j < nCons + nGen;j++) {
			connec->set(i, j, 1);
		}
	}
}

StudyCase::StudyCase()
{
	 _nAgent=0;
	 _nPro=0;
	 _nGen=0;
	 _nCons=0;
	 
}

void StudyCase::Set29node()
{
	_nAgent = 29;
	_nPro = 2;
	_nGen = 8;
	_nCons = 19;
	DELETEA(_agents);
	_agents = new Agent[_nAgent];
	float Plim1[29] = { -146.4, -483, -750, -350.7, -783, -9.8, -12.8, -480, -493.5, -237, -1020, -411, -371.3, -462.9, -336, -208.5, -421.5, -309, -425.3, 0, 0, 0, 0, 0, 0, 0, 0, -13.8, -1656 }; 
	float Plim2[29] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1040, 725, 652, 508, 687, 580, 564, 865, 646, 1100};
	float Cost1[29] = { 67, 47, 47, 53, 82, 52, 87, 57, 50, 52, 71, 64, 57, 82, 69, 69, 86, 54, 78, 89, 55, 82, 88, 76, 84, 77, 51, 74, 73 };
	float Cost2[29] = { 64, 79, 71, 62, 65, 73, 63, 81, 73, 69, 62, 79, 60, 80, 78, 70, 62, 70, 66, 18, 37, 25, 17, 38, 28, 36, 38, 40, 40 };
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;
	_connect = MatrixCPU(_nAgent, _nAgent);
	_a = MatrixCPU(_nAgent, 1);
	_b = MatrixCPU(_nAgent, 1);
	_Ub = MatrixCPU(_nAgent, 1);
	_Lb = MatrixCPU(_nAgent, 1);
	_Pmin = MatrixCPU(_nAgent, 1);
	_Pmax = MatrixCPU(_nAgent, 1);
	_nVoisin = MatrixCPU(_nAgent, 1);
	_GAMMA = MatrixCPU(_nAgent, _nAgent, 1);
	genConnec(&_connect, _nCons, _nGen, _nPro);
	genGAMMA(&_GAMMA, _nCons, _nGen, _nPro);
	int nVoisin;
	
	for (int id = 0; id < _nAgent; id++)
	{
		if (id < _nCons) { // consumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id] /1000;
			cost2 = Cost2[id] ;
			nVoisin = _nGen + _nPro;
			
			(_agents[id]).setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 1);
	
			_Ub.set(id, 0, 0);
			_Lb.set(id, 0, pLim1);
			
		}
		else if (id < (_nCons + _nGen)) { // generator
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id] / 1000;
			cost2 = Cost2[id] ;
			nVoisin = _nCons + _nPro;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 2);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, 0);
		}
		else { // prosumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id] / 1000;
			cost2 = Cost2[id];
			nVoisin = _nGen + _nCons;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 3);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, pLim1);
		}
		_a.set(id, 0, cost1);
		_b.set(id, 0, cost2);

		_Pmin.set(id, 0, pLim1);
		_Pmax.set(id, 0, pLim2);
		_nVoisin.set(id, 0, nVoisin);
	}
}

void StudyCase::Set4node()
{

	_nAgent = 4;
	_nPro = 1;
	_nGen = 1;
	_nCons = 2;
	DELETEA(_agents);
	_agents = new Agent[_nAgent];
	float Plim1[4] = {-30, -30, 0, -20 };
	float Plim2[4] = { -1, -2, 60, 30 };
	float Cost1[4] = { 1, 1, 0.7, 0.7 };
	float Cost2[4] = { 70, 70, 40, 60 };
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;
	_connect = MatrixCPU(_nAgent, _nAgent);
	_a = MatrixCPU(_nAgent, 1);
	_b = MatrixCPU(_nAgent, 1);
	_Ub = MatrixCPU(_nAgent, 1);
	_Lb = MatrixCPU(_nAgent, 1);
	_Pmin = MatrixCPU(_nAgent, 1);
	_Pmax = MatrixCPU(_nAgent, 1);
	_nVoisin = MatrixCPU(_nAgent, 1);
	_GAMMA = MatrixCPU(_nAgent, _nAgent, 1);
	genConnec(&_connect, _nCons, _nGen, _nPro);
	genGAMMA(&_GAMMA, _nCons, _nGen, _nPro);
	int nVoisin;

	for (int id = 0; id < _nAgent; id++)
	{
		if (id < _nCons) { // consumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id];
			cost2 = Cost2[id];
			nVoisin = _nGen + _nPro;

			(_agents[id]).setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 1);

			_Ub.set(id, 0, 0);
			_Lb.set(id, 0, pLim1);

		}
		else if (id < (_nCons + _nGen)) { // generator
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id];
			cost2 = Cost2[id];
			nVoisin = _nCons + _nPro;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 2);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, 0);
		}
		else { // prosumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id];
			cost2 = Cost2[id];
			nVoisin = _nGen + _nCons;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 3);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, pLim1);
		}
		_a.set(id, 0, cost1);
		_b.set(id, 0, cost2);

		_Pmin.set(id, 0, pLim1);
		_Pmax.set(id, 0, pLim2);
		_nVoisin.set(id, 0, nVoisin);
	}



}

void StudyCase::Set2node() {
	_nAgent = 2;
	_nPro = 0;
	_nGen = 1;
	_nCons = 1;
	DELETEA(_agents);
	_agents = new Agent[_nAgent];
	float Plim1[2] = { -30, 0 };
	float Plim2[2] = { 0, 60 };
	float Cost1[2] = { 1, 1 };
	float Cost2[2] = { 8, 4 };
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;
	_connect = MatrixCPU(_nAgent, _nAgent);
	_a = MatrixCPU(_nAgent, 1);
	_b = MatrixCPU(_nAgent, 1);
	_Ub = MatrixCPU(_nAgent, 1);
	_Lb = MatrixCPU(_nAgent, 1);
	_Pmin = MatrixCPU(_nAgent, 1);
	_Pmax = MatrixCPU(_nAgent, 1);
	_nVoisin = MatrixCPU(_nAgent, 1);
	_GAMMA = MatrixCPU(_nAgent, _nAgent, 1);
	genConnec(&_connect, _nCons, _nGen, _nPro);
	genGAMMA(&_GAMMA, _nCons, _nGen, _nPro);
	int nVoisin;

	for (int id = 0; id < _nAgent; id++)
	{
		if (id < _nCons) { // consumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id];
			cost2 = Cost2[id];
			nVoisin = _nGen + _nPro;

			(_agents[id]).setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 1);

			_Ub.set(id, 0, 0);
			_Lb.set(id, 0, pLim1);

		}
		else if (id < (_nCons + _nGen)) { // generator
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id];
			cost2 = Cost2[id];
			nVoisin = _nCons + _nPro;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 2);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, 0);
		}
		else { // prosumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id];
			cost2 = Cost2[id];
			nVoisin = _nGen + _nCons;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 3);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, pLim1);
		}
		_a.set(id, 0, cost1);
		_b.set(id, 0, cost2);

		_Pmin.set(id, 0, pLim1);
		_Pmax.set(id, 0, pLim2);
		_nVoisin.set(id, 0, nVoisin);
	}
}

void StudyCase::SetEuropeCountry(const std::string& path, const std::string& date)
{
	int nCountry = 25;
	_nAgent = nCountry*2; // 25 countries, 1 for productors, 1 for consumers, no prosumers
	_nPro = 0;
	_nGen = nCountry;
	_nCons = nCountry;

	float dP = 0.1; // P = P0 +/- dP * P0 for the consumers
	DELETEA(_agents);
	_agents = new Agent[_nAgent];
	_connect = MatrixCPU(_nAgent, _nAgent);
	_a = MatrixCPU(_nAgent, 1);
	_b = MatrixCPU(_nAgent, 1);
	_Ub = MatrixCPU(_nAgent, 1);
	_Lb = MatrixCPU(_nAgent, 1);
	_Pmin = MatrixCPU(_nAgent, 1);
	_Pmax = MatrixCPU(_nAgent, 1);
	_nVoisin = MatrixCPU(_nAgent, 1);
	_GAMMA = MatrixCPU(_nAgent, _nAgent, 1);

	genConnec(&_connect, _nCons, _nGen, _nPro);
	genGAMMA(&_GAMMA, _nCons, _nGen, _nPro);


	int nVoisin;
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;
	MatrixCPU Pgen(nCountry, 1);
	MatrixCPU P0(nCountry, 1);
	MatrixCPU costGen(nCountry, 2);

	

	setMatFromFile(path, date, &Pgen, &P0, &costGen);


	for (int id = 0; id < _nAgent; id++)
	{
		if (id < _nCons) { // consumer
			
			pLim1 =  - (1 + dP) * P0.get(id,0);
			pLim2 =  - (1 - dP) * P0.get(id, 0);
			cost1 = 1;
			cost2 =	P0.get(id, 0) * cost1;

			nVoisin = _nGen + _nPro;
			(_agents[id]).setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 1);
			_Ub.set(id, 0, 0);
			_Lb.set(id, 0, pLim1);

		}
		else  { // generator
			
			pLim1 = 0;
			pLim2 = Pgen.get(id - nCountry, 0);
			cost1 = costGen.get(id - nCountry, 0);
			cost2 = costGen.get(id - nCountry, 1);

			nVoisin = _nCons + _nPro;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 2);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, 0);
		}
		_Pmin.set(id, 0, pLim1);
		_Pmax.set(id, 0, pLim2);
		_a.set(id, 0, cost1);
		_b.set(id, 0, cost2);
		_nVoisin.set(id, 0, nVoisin);

	}
}

void StudyCase::SetEuropeCountryP0(const std::string& path, MatrixCPU* P0)
{
	int nCountry = 25;
	_nAgent = nCountry * 2; // 25 countries, 1 for productors, 1 for consumers, no prosumers
	_nPro = 0;
	_nGen = nCountry;
	_nCons = nCountry;

	float dP = 0.1; // P = P0 +/- dP * P0 for the consumers
	DELETEA(_agents);
	_agents = new Agent[_nAgent];
	_connect = MatrixCPU(_nAgent, _nAgent);
	_a = MatrixCPU(_nAgent, 1);
	_b = MatrixCPU(_nAgent, 1);
	_Ub = MatrixCPU(_nAgent, 1);
	_Lb = MatrixCPU(_nAgent, 1);
	_Pmin = MatrixCPU(_nAgent, 1);
	_Pmax = MatrixCPU(_nAgent, 1);
	_nVoisin = MatrixCPU(_nAgent, 1);
	_GAMMA = MatrixCPU(_nAgent, _nAgent, 1);

	genConnec(&_connect, _nCons, _nGen, _nPro);
	genGAMMA(&_GAMMA, _nCons, _nGen, _nPro);


	int nVoisin;
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;
	MatrixCPU Pgen(nCountry, 1);
	MatrixCPU costGen(nCountry, 2);

	setGenFromFile(path, &Pgen, &costGen);


	for (int id = 0; id < _nAgent; id++)
	{
		if (id < _nCons) { // consumer

			pLim1 = -(1 + dP) * P0->get(id, 0);
			pLim2 = -(1 - dP) * P0->get(id, 0);
			cost1 = 1;
			cost2 = P0->get(id, 0) * cost1;

			nVoisin = _nGen + _nPro;
			(_agents[id]).setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 1);
			_Ub.set(id, 0, 0);
			_Lb.set(id, 0, pLim1);

		}
		else { // generator

			pLim1 = 0;
			pLim2 = Pgen.get(id - nCountry, 0);
			cost1 = costGen.get(id - nCountry, 0);
			cost2 = costGen.get(id - nCountry, 1);

			nVoisin = _nCons + _nPro;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 2);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, 0);
		}
		_Pmin.set(id, 0, pLim1);
		_Pmax.set(id, 0, pLim2);
		_a.set(id, 0, cost1);
		_b.set(id, 0, cost2);
		_nVoisin.set(id, 0, nVoisin);

	}
}


void StudyCase::SetEurope(const std::string& path, const std::string& date)
{
	
	_nGen = 969;
	_nCons = 1494;
	_nAgent = _nGen+_nCons; 
	_nPro = 0;
	
	
	float dP = 0.1; // P = P0 +/- dP * P0 for the consumers
	DELETEA(_agents);
	_agents = new Agent[_nAgent];
	_connect = MatrixCPU(_nAgent, _nAgent);
	_a = MatrixCPU(_nAgent, 1);
	_b = MatrixCPU(_nAgent, 1);
	_Ub = MatrixCPU(_nAgent, 1);
	_Lb = MatrixCPU(_nAgent, 1);
	_Pmin = MatrixCPU(_nAgent, 1);
	_Pmax = MatrixCPU(_nAgent, 1);
	_nVoisin = MatrixCPU(_nAgent, 1);
	_GAMMA = MatrixCPU(_nAgent, _nAgent, 1);

	genConnec(&_connect, _nCons, _nGen, _nPro);
	genGAMMA(&_GAMMA, _nCons, _nGen, _nPro);


	int nVoisin;
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;
	MatrixCPU Pgen(_nGen, 1);
	MatrixCPU P0(_nCons, 1);
	MatrixCPU Cost(_nGen, 1);
	
	std::string pathGen = path + "PowerMaxGen.txt";
	std::string pathGenCost = path + "costGen.txt";
	std::string pathCons = path + "/load/all/" + date + ".txt";

	Pgen.setFromFile(pathGen,1);
	P0.setFromFile(pathCons, 1);
	Cost.setFromFile(pathGenCost, 1);


	for (int id = 0; id < _nAgent; id++)
	{
		if (id < _nCons) { // consumer

			pLim1 = -(1 + dP) * P0.get(id, 0);
			pLim2 = -(1 - dP) * P0.get(id, 0);
			cost1 = 1;
			cost2 = P0.get(id, 0) * cost1;

			nVoisin = _nGen + _nPro;
			(_agents[id]).setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 1);
			_Ub.set(id, 0, 0);
			_Lb.set(id, 0, pLim1);

		}
		else { // generator

			pLim1 = 0;
			pLim2 = Pgen.get(id - _nCons, 0);
			cost1 = 0.1;
			cost2 = Cost.get(id-_nCons,0);

			nVoisin = _nCons + _nPro;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 2);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, 0);
		}
		_Pmin.set(id, 0, pLim1);
		_Pmax.set(id, 0, pLim2);
		_a.set(id, 0, cost1);
		_b.set(id, 0, cost2);
		_nVoisin.set(id, 0, nVoisin);

	}
}

void StudyCase::SetEuropeP0(const std::string& path, MatrixCPU* P0)
{
	_nGen = 969;
	_nCons = 1494;
	_nAgent = _nGen + _nCons;
	_nPro = 0;

	float dP = 0.1; // P = P0 +/- dP * P0 for the consumers
	DELETEA(_agents);
	_agents = new Agent[_nAgent];
	_connect = MatrixCPU(_nAgent, _nAgent);
	_a = MatrixCPU(_nAgent, 1);
	_b = MatrixCPU(_nAgent, 1);
	_Ub = MatrixCPU(_nAgent, 1);
	_Lb = MatrixCPU(_nAgent, 1);
	_Pmin = MatrixCPU(_nAgent, 1);
	_Pmax = MatrixCPU(_nAgent, 1);
	_nVoisin = MatrixCPU(_nAgent, 1);
	_GAMMA = MatrixCPU(_nAgent, _nAgent, 0);

	genConnec(&_connect, _nCons, _nGen, _nPro);
	genGAMMA(&_GAMMA, _nCons, _nGen, _nPro);


	int nVoisin;
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;
	MatrixCPU Pgen(_nGen, 1);
	MatrixCPU Cost(_nGen, 1);
	std::string pathGen = path + "PowerMaxGen.txt";
	std::string pathGenCost = path + "costGen.txt";
	Pgen.setFromFile(pathGen, 1);
	Cost.setFromFile(pathGenCost, 1);


	for (int id = 0; id < _nAgent; id++)
	{
		if (id < _nCons) { // consumer

			pLim1 = -(1 + dP) * P0->get(id, 0);
			pLim2 = -(1 - dP) * P0->get(id, 0);
			cost1 = 1;
			cost2 = P0->get(id, 0) * cost1;

			nVoisin = _nGen + _nPro;
			(_agents[id]).setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 1);
			_Ub.set(id, 0, 0);
			_Lb.set(id, 0, pLim1);

		}
		else { // generator

			pLim1 = 0;
			pLim2 = Pgen.get(id - _nCons, 0);
			cost1 = 0.1;
			cost2 = Cost.get(id - _nCons, 0);

			nVoisin = _nCons + _nPro;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 2);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, 0);
		}
		_Pmin.set(id, 0, pLim1);
		_Pmax.set(id, 0, pLim2);
		_a.set(id, 0, cost1);
		_b.set(id, 0, cost2);
		_nVoisin.set(id, 0, nVoisin);

	}
}

void StudyCase::genGAMMA(MatrixCPU* Gamma, int nCons, int nGen, int nPro)
{
	int nAgent = nCons + nGen + nPro;
	for (int i = 0;i < nCons; i++) {
		for (int j = nCons;j < nAgent;j++) {
			Gamma->set(i, j, -Gamma->get(i,j));
		}
	}
	for (int i = nCons + nGen;i < nAgent; i++) {
		for (int j = nCons;j < nCons + nGen;j++) {
			Gamma->set(i, j, -Gamma->get(i, j));
		}
	}

}


StudyCase::StudyCase(int nAgent, float P, float dP, float a, float da, float b, float db, float propCons, float propPro)
{
	srand(time(nullptr));
	if (propCons < 0 || propPro < 0 || (1 - propCons - propPro) < 0) {
		throw std::invalid_argument("propCons and propPro are proportion <1 and >0");
	}
	_nAgent = nAgent;
	_nCons = nAgent *propCons;
	_nPro = nAgent*propPro;
	_nGen = nAgent - _nCons - _nPro;
	
	_agents = new Agent[nAgent];
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;

	_a = MatrixCPU(nAgent, 1);
	_b = MatrixCPU(nAgent, 1);
	_Ub = MatrixCPU(nAgent, 1);
	_connect = MatrixCPU(nAgent,nAgent);
	_Lb = MatrixCPU(nAgent, 1);
	_Pmin = MatrixCPU(nAgent, 1);
	_Pmax = MatrixCPU(nAgent, 1);
	_nVoisin = MatrixCPU(nAgent, 1);
	_GAMMA = MatrixCPU(_nAgent, _nAgent, 0);

	genConnec(&_connect, _nCons, _nGen, _nPro);
	genGAMMA(&_GAMMA, _nCons, _nGen, _nPro);

	int nVoisin;

	bool impossible = true;

	while (impossible)
	{
		for (int id = 0; id < nAgent; id++)
		{
			if (id < _nCons) { // consumer
				pLim1 = -P + dP * 2 * (rand1() - 0.5);
				pLim2 = -dP * rand1();
				cost1 = a + da * 2 * (rand1() - 0.5);
				cost2 = b + db * 2 * (rand1() - 0.5);
				nVoisin = _nGen + _nPro;
				_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, nAgent, 1);
				_Ub.set(id, 0, 0);
				_Lb.set(id, 0, pLim1);
			}
			else if (id < (_nCons + _nGen)) { // generator
				pLim1 = dP * rand1();
				pLim2 = P + dP * 2 * (rand1() - 0.5);
				cost1 = a + da * 2 * (rand1() - 0.5);
				cost2 = b + db * 2 * (rand1() - 0.5);
				nVoisin = _nCons + _nPro;
				_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, nAgent, 2);
				_Ub.set(id, 0, pLim2);
				_Lb.set(id, 0, 0);
			}
			else { // prosumer
				pLim1 = -P + dP * 2 * (rand1() - 0.5);
				pLim2 = P + dP * 2 * (rand1() - 0.5);
				cost1 = a + da * 2 * (rand1() - 0.5);
				cost2 = b + db * 2 * (rand1() - 0.5);
				nVoisin = _nGen + _nCons;
				_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, nAgent, 3);
				_Ub.set(id, 0, pLim2);
				_Lb.set(id, 0, pLim1);
			}
			_a.set(id, 0, cost1);
			_b.set(id, 0, cost2);

			_Pmin.set(id, 0, pLim1);
			_Pmax.set(id, 0, pLim2);
			_nVoisin.set(id, 0, nVoisin);
		}
		impossible = false;
	}
}

StudyCase::StudyCase(int nAgent, float P0, float dP, float b, float db, float propCons)
{
	srand(time(nullptr));
	if (propCons < 0  || (1 - propCons) < 0) {
		throw std::invalid_argument("propCons is a proportion <1 and >0");
	}
	_nAgent = nAgent;
	_nCons = nAgent * propCons;
	_nPro = 0;
	_nGen = nAgent - _nCons - _nPro;

	_agents = new Agent[nAgent];
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;

	_a = MatrixCPU(nAgent, 1);
	_b = MatrixCPU(nAgent, 1);
	_Ub = MatrixCPU(nAgent, 1);
	_connect = MatrixCPU(nAgent, nAgent);
	_Lb = MatrixCPU(nAgent, 1);
	_Pmin = MatrixCPU(nAgent, 1);
	_Pmax = MatrixCPU(nAgent, 1);
	_nVoisin = MatrixCPU(nAgent, 1);
	_GAMMA = MatrixCPU(_nAgent, _nAgent, 0);

	genConnec(&_connect, _nCons, _nGen, _nPro);
	genGAMMA(&_GAMMA, _nCons, _nGen, _nPro);

	int nVoisin;

	bool impossible = true;

	while (impossible)
	{
		for (int id = 0; id < nAgent; id++)
		{
			if (id < _nCons) { // consumer
				pLim1 = -P0 - dP * (rand1() + 0.01);
				pLim2 = -P0 + dP * (rand1() + 0.01);
				cost1 = 1;
				cost2 = P0;
				nVoisin = _nGen + _nPro;
				_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, nAgent, 1);
				_Ub.set(id, 0, 0);
				_Lb.set(id, 0, pLim1);
			}
			else { // generator
				pLim1 = 0;
				pLim2 = P0 + dP * 2 * (rand1() - 0.5);
				cost1 = 0.1;
				cost2 = b + db * 2 * (rand1() - 0.5);
				nVoisin = _nCons + _nPro;
				_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, nAgent, 2);
				_Ub.set(id, 0, pLim2);
				_Lb.set(id, 0, 0);
			}
			_a.set(id, 0, cost1);
			_b.set(id, 0, cost2);

			_Pmin.set(id, 0, pLim1);
			_Pmax.set(id, 0, pLim2);
			_nVoisin.set(id, 0, nVoisin);
		}
		impossible = false;
	}
}

StudyCase::StudyCase(const StudyCase& s)
{
	_nAgent = s._nAgent;
	_nPro = s._nPro;
	_nGen = s._nGen;
	_nCons = s._nCons;

	_agents = new Agent[_nAgent];
	for (int i = 0;i < _nAgent;i++) {
		_agents[i] = s._agents[i];
	}

	_a = MatrixCPU(s._a);
	_b = MatrixCPU(s._b);
	_Ub = MatrixCPU(s._Ub);
	_connect = MatrixCPU(s._connect);
	_Lb = MatrixCPU(s._Lb);
	_Pmin = MatrixCPU(s._Pmin);
	_Pmax = MatrixCPU(s._Pmax);
	_nVoisin = MatrixCPU(s._nVoisin);
	_GAMMA = MatrixCPU(s._GAMMA);
}

StudyCase::StudyCase(std::string fileName)
{
	std::ifstream myFile(fileName, std::ios::in);
	std::cout << fileName << std::endl;
	if (myFile)
	{
		myFile >> _nAgent >> _nCons >> _nGen >> _nPro;
		int nbLineToRead = 7 + 2 * _nAgent;
		_agents = new Agent[_nAgent];
		_a = MatrixCPU(_nAgent, 1);
		_b = MatrixCPU(_nAgent, 1);
		_Lb = MatrixCPU(_nAgent, 1);
		_Ub = MatrixCPU(_nAgent, 1);
		_Pmin = MatrixCPU(_nAgent, 1);
		_Pmax = MatrixCPU(_nAgent, 1);
		_nVoisin = MatrixCPU(_nAgent, 1);
		_GAMMA = MatrixCPU(_nAgent, _nAgent);
		_connect = MatrixCPU(_nAgent, _nAgent);
		float c = 0;

		for (int i = 0; i < nbLineToRead; i++) 
		{
			switch (i)
			{
			case 0:
				for (int j = 0; j < _nAgent;j++) {
					myFile >> c;
					_a.set(j, 0, c);
				}
				break;
			case 1:
				for (int j = 0; j < _nAgent;j++) {
					myFile >> c;
					_b.set(j, 0, c);
				}
				break;
			case 2:
				for (int j = 0; j < _nAgent;j++) {
					myFile >> c;
					_Lb.set(j, 0, c);
				}
				break;
			case 3:
				for (int j = 0; j < _nAgent;j++) {
					myFile >> c;
					_Ub.set(j, 0, c);
				}
				break;
			case 4:
				for (int j = 0; j < _nAgent;j++) {
					myFile >> c;
					_Pmin.set(j, 0, c);
				}
				break;
			case 5:
				for (int j = 0; j < _nAgent;j++) {
					myFile >> c;
					_Pmax.set(j, 0, c);
				}
				break;
			case 6:
				for (int j = 0; j < _nAgent;j++) {
					myFile >> c;
					_nVoisin.set(j, 0, c);
				}
				break;
			default:
				if (i < (6 + _nAgent)) {
					for (int j = 0; j < _nAgent;j++) {
						myFile >> c;
						_GAMMA.set(i, j, c);
					}
				}
				else {
					for (int j = 0; j < _nAgent;j++) {
						myFile >> c;
						_connect.set(i, j, c);
					}
				}
				break;
			}
			
		}
		myFile.close();

		for (int id = 0; id < _nAgent; id++)
		{
			if (id < _nCons) { // consumer
				_agents[id].setAgent(id, _Pmin.get(id, 0), _Pmax.get(id, 0), _a.get(id, 0), _b.get(id, 0), _nVoisin.get(id, 0), &_connect, _nAgent, 1);
	
			}
			else if (id < (_nCons + _nGen)) { // generator
				_agents[id].setAgent(id, _Pmin.get(id, 0), _Pmax.get(id, 0), _a.get(id, 0), _b.get(id, 0), _nVoisin.get(id, 0), &_connect, _nAgent, 2);
			}
			else { // prosumer
				_agents[id].setAgent(id, _Pmin.get(id, 0), _Pmax.get(id, 0), _a.get(id, 0), _b.get(id, 0), _nVoisin.get(id, 0), &_connect, _nAgent, 3);
			}
		}
	}
	else {
		throw std::invalid_argument("can't open this file");
	}


}

StudyCase& StudyCase::operator= (const StudyCase& s) {
	
	_nAgent = s._nAgent;
	_nPro = s._nPro;
	_nGen = s._nGen;
	_nCons = s._nCons;

	DELETEA(_agents);
	_agents = new Agent[_nAgent];
	for (int i = 0;i < _nAgent;i++) {
		_agents[i] = s._agents[i];
	}

	_a = MatrixCPU(s._a);
	_b = MatrixCPU(s._b);
	_Ub = MatrixCPU(s._Ub);
	_connect = MatrixCPU(s._connect);
	_Lb = MatrixCPU(s._Lb);
	_Pmin = MatrixCPU(s._Pmin);
	_Pmax = MatrixCPU(s._Pmax);
	_nVoisin = MatrixCPU(s._nVoisin);
	_GAMMA = MatrixCPU(s._GAMMA);
	
	
	return *this;
}

void StudyCase::UpdateP0(MatrixCPU* P0) 
{

	float dP = 0.1; // P = P0 +/- dP * P0 for the consumers
	
	if (P0->getNLin() != _nCons) {
		throw std::invalid_argument("P0 hasn't the good number of column");
	}
	for (int id = 0; id < _nCons; id++)
	{
		_agents[id].updateP0(P0->get(id, 0), dP);
		_Pmin.set(id, 0, _agents[id].getPmin());
		_Pmax.set(id, 0, _agents[id].getPmax());
		_a.set(id, 0, _agents[id].getA());
		_b.set(id, 0, _agents[id].getB());
		_Ub.set(id, 0, 0);
		_Lb.set(id, 0, _agents[id].getLb());
	}
	
}

MatrixCPU StudyCase::getG() const
{
	return _GAMMA;
}

MatrixCPU StudyCase::getC() const
{
	return _connect;
}

MatrixCPU StudyCase::geta() const
{
	return _a;
}

MatrixCPU StudyCase::getb() const
{
	return _b;
}

MatrixCPU StudyCase::getUb() const
{
	return _Ub;
}

MatrixCPU StudyCase::getLb() const
{
	return _Lb;
}

MatrixCPU StudyCase::getPmin() const
{
	return _Pmin;
}

MatrixCPU StudyCase::getPmax() const
{
	return _Pmax;
}

MatrixCPU StudyCase::getNvoi() const
{
	return _nVoisin;
}

int StudyCase::getNagent() const
{
	return _nAgent;
}

MatrixCPU StudyCase::getVoisin(int agent) const
{
	if (agent > _nAgent) {
		throw std::invalid_argument("this agent doesn't exists");
	}
	return _agents[agent].getVoisin();
}
Agent StudyCase::getAgent(int agent) const
{
	return _agents[agent];
}

void StudyCase::removeLink(int i, int j)
{
	
	if (i>=_nAgent && j>=_nAgent) {
		throw std::invalid_argument("indice out of range");
	}
	if (_connect.get(i, j) == 0) {
		
		throw std::invalid_argument("agent already not linked"); 
	}
	_connect.set(i, j, 0);
	_connect.set(j, i, 0);


	_nVoisin.set(i, 0, _nVoisin.get(i, 0) - 1);
	_nVoisin.set(j, 0, _nVoisin.get(j, 0) - 1);

	int type = 3;

	if (i < _nCons) {
		type = 1;
	} 
	else if (i >= (_nCons + _nGen)) {
		type = 2;
	}
	_agents[i].setAgent(i, _Pmin.get(i, 0), _Pmax.get(i, 0), _a.get(i, 0), _b.get(i, 0), _nVoisin.get(i, 0), &_connect, _nAgent, type);
	type = 3;
	
	if (j < _nCons) {
		type = 1;
	}
	else if (j >= (_nCons + _nGen)) {
		type = 2;
	}
	_agents[j].setAgent(j, _Pmin.get(j, 0), _Pmax.get(j, 0), _a.get(j, 0), _b.get(j, 0), _nVoisin.get(j, 0), &_connect, _nAgent, type);

}


void StudyCase::addLink(int i, int j)
{
	//std::cout << "link " << i << " " << j << std::endl;
	if (i >= _nAgent && j >= _nAgent) {
		throw std::invalid_argument("indice out of range");
	}
	if (i == j) {
		throw std::invalid_argument("agent must not be link to itself");
	}
	if (_connect.get(i, j) == 1) {
		throw std::invalid_argument("agent already linked");
	}
	int type[2] = { 3 , 3 };
	if (i < _nCons) {
		type[0] = 1;
	}
	else if (i >= (_nCons + _nGen)) {
		type[0] = 2;
	}
	if (j < _nCons) {
		type[1] = 1;
	}
	else if (j >= (_nCons + _nGen)) {
		type[1] = 2;
	}
	if (type[0] == type[1]) {
		throw std::invalid_argument("agent must not be the same type");
	}

	_connect.set(i, j, 1);
	_connect.set(j, i, 1);


	_nVoisin.set(i, 0, _nVoisin.get(i, 0) + 1);
	_nVoisin.set(j, 0, _nVoisin.get(j, 0) + 1);

	_agents[i].setAgent(i, _Pmin.get(i, 0), _Pmax.get(i, 0), _a.get(i, 0), _b.get(i, 0), _nVoisin.get(i, 0), &_connect, _nAgent, type[0]);
	_agents[j].setAgent(j, _Pmin.get(j, 0), _Pmax.get(j, 0), _a.get(j, 0), _b.get(j, 0), _nVoisin.get(j, 0), &_connect, _nAgent, type[1]);

}

Agent StudyCase::removeAgent(int agent)
{
	if (agent > _nAgent || agent<0) {
		throw std::invalid_argument("this agent doesn't exist");
	}
	Agent agentRemove(_agents[agent]);
	MatrixCPU omega(getVoisin(agent));
	int Nvoisinmax = _nVoisin.get(agent, 0);
	for (int voisin = 0; voisin < Nvoisinmax; voisin++) {
		int idVoisin = omega.get(voisin, 0);
		removeLink(agent, idVoisin);
	}

	_Pmin.set(agent, 0, 0);
	_Pmax.set(agent, 0, 0);
	int type =  3;
	if (agent < _nCons) {
		type = 1;
	}
	else if (agent >= (_nCons + _nGen)) {
		type = 2;
	}
	_agents[agent].setAgent(agent, _Pmin.get(agent, 0), _Pmax.get(agent, 0), _a.get(agent, 0), _b.get(agent, 0), _nVoisin.get(agent, 0), &_connect, _nAgent, type);

	return agentRemove;

}

void StudyCase::restoreAgent(Agent& agent, bool all) {


	int id = agent.getId();
	float Pmin = agent.getPmin();
	float Pmax = agent.getPmax();
	int type = agent.getType();

	_Pmin.set(id, 0, Pmin);
	_Pmax.set(id, 0, Pmax);
	if (!all) {
		std::cout << "Beware restore agent without giving it neighbor can lead to a non solvable problem" << std::endl;
	}
	else {
		MatrixCPU omega(agent.getVoisin());
		int nVoisin = agent.getNVoisin();
		for (int i = 0;i < nVoisin;i++) {
			int idVoisin = omega.get(i, 0);
			addLink(id, idVoisin);
		}
	}
	_agents[id].setAgent(id, _Pmin.get(id, 0), _Pmax.get(id, 0), _a.get(id, 0), _b.get(id, 0), _nVoisin.get(id, 0), &_connect, _nAgent, type);
}

void StudyCase::saveCSV(const std::string& fileName, bool all)
{

	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	MatrixCPU nombre(1, 4);
	nombre.set(0, 0, _nAgent);
	nombre.set(0, 1, _nCons);
	nombre.set(0, 2, _nGen);
	nombre.set(0, 3, _nPro);
	nombre.saveCSV(fileName, mode);


	MatrixCPU temp(1, _nAgent);
	MatrixCPU zero(1, _nAgent);
	temp.addTrans(&_a);
	temp.saveCSV(fileName, mode);
	temp.set(&zero);
	temp.addTrans(&_b);
	temp.saveCSV(fileName, mode);
	temp.set(&zero);
	temp.addTrans(&_Lb);
	temp.saveCSV(fileName, mode);
	temp.set(&zero);
	temp.addTrans(&_Ub);
	temp.saveCSV(fileName, mode);
	temp.set(&zero);
	temp.addTrans(&_Pmin);
	temp.saveCSV(fileName, mode);
	temp.set(&zero);
	temp.addTrans(&_Pmax);
	temp.saveCSV(fileName, mode);
	temp.set(&zero);
	temp.addTrans(&_nVoisin);
	temp.saveCSV(fileName, mode);
	temp.set(&zero);

	if (all) {
		_GAMMA.saveCSV(fileName, mode);
		_connect.saveCSV(fileName, mode);
	}


}

void StudyCase::display() const
{
	std::cout<< "Study Case : "<< _nAgent << " agents " <<std::endl; 
	_connect.display();
	std::cout << " a :" << std::endl;
	_a.display();
	std::cout << " b :" << std::endl;
	_b.display();
	std::cout << " lower bound: " << std::endl;
	_Lb.display();
	std::cout << " uper bound: " << std::endl;
	_Ub.display();
	std::cout << " Pmin :" << std::endl;
	_Pmin.display();
	std::cout << " Pmax :" << std::endl;
	_Pmax.display();
	std::cout << " Peers count :" << std::endl;
	_nVoisin.display();

	std::cout << "end case " << std::endl;
}

StudyCase::~StudyCase()
{
#ifdef DEBUG_DESTRUCTOR
	std::cout << "case destructor" << std::endl;
#endif

	DELETEA(_agents);

}


